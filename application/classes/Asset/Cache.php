<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Beautiful Asset Cache
 *
 * @package     Beautiful
 * @subpackage  Beautiful Asset
 * @category    Asset Cache
 * @author      Luke Morton
 * @copyright   Luke Morton, 2011
 * @license     MIT
 */
class Asset_Cache extends Beautiful_Asset_Cache {
	
	// Get asset locations as array
	protected function _asset_locations()
	{
		$locations = array();
		
		foreach ($this->_group->as_array() as $_asset)
		{
			$locations[] = $_asset->location();
		}
		
		return $locations;
	}
}