<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller contains all logic for admn controllers
 *
 * @version 02 - Hezron Obuchele 2013-01-03
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Controller_Admin
 * @author     Hezron Obuchele
 * @package    Mawaidha
 * @category   Controllers
 * @copyright  (c) 2012 BeeBuy Investments Ltd. - http://www.beebuy.biz
 */
class Controller_Admin extends Controller_App {
	
	/**
	 * System component name
	 * @var string
	 */
	protected $_component_name = 'admin';
	
    // Require login to access system
    //public $auth_required = 'admin';
    protected $_template_layout = 'admin-layout';
	
	
	protected $permission_actions = array(
		'ADMIN_LOGIN' => array('before')
	);
	
    public function before() {
        parent::before();
		
			
		// Set template blocks TODO: Make this dynamic depending on tpl folder contents
		// $this->_template->template()->partial('header', $this->_template_path . 'header');
		// $this->_template->template()->partial('menu', $this->_template_path . 'blocks/menu');
		$this->_template->set('menuname', 'admin');
		// $this->_template->template()->partial('message', $this->_template_path . 'blocks/message');
		// $this->_template->template()->partial('footer', $this->_template_path . 'footer');
		// $this->_template->template()->partial('pagination', $this->_template_path . 'blocks/pagination');
		
		// Setup language switcher
		$lang_files = Kohana::list_files('i18n', array(APPPATH));
		$lang_codes = array();
		// remove current language from list to avoid unnecessary requests of setting the same language again TODO: Move to template
		//unset($lang_files['i18n' . DIRECTORY_SEPARATOR . I18n::lang() . '.php']);
		foreach ($lang_files as $file_name => $full_path) {
			$code = str_ireplace('i18n' . DIRECTORY_SEPARATOR, '', $file_name);
			$code = str_ireplace('.php', '', $code);
			//echo $code;
			$lang_codes[] = $code;
		}
		$this->_template->set('languages', $lang_codes);
		
		// setup theme switcher
		if (Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.enable_dynamic_themes")){
			$themes = array();
			foreach (Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.themes") as $name => $file) {
				$themes[] = array('name' => $name, 'label' => ucfirst($name));
			}
			
			$this->_template->set('themes', $themes);
		}
		
		// setup breadcrumbs
        $this->_breadcrumbs('Admin');
    }

    /**
     * Get the widgets from the config/dB
     * 
     */
    protected function _widgets($position = '') {
        // load from dB
        //$widgets = ARM::factory('widget')->getWidgets($position);
        //return $widgets;
        // load from config
        $widgets = Kohana::$config->load('widget');
        if ($widgets) {
            // Remove widgets that have been disabled
            foreach ($widgets as $name => $options) {
                if (!Kohana::$config->load('widget.' . $name . '.enabled')) {
                    unset($widgets[$name]);
                }
            }

            // Remove widgets that require a role above the current one
            foreach ($widgets as $name => $options) {
                // if access_level not defined for menu item, move to next item
                if (!array_key_exists('access_level', Kohana::$config->load('widget.' . $name))) {
                    continue;
                }
                $access_level = Kohana::$config->load('widget.' . $name . '.access_level');
                if (!Auth::instance()->logged_in($access_level)) {
                    unset($widgets[$name]);
                }
                $widgets[$name] = (object) $options;
            }
        }
        return $widgets;
    }
	
	/**
	 * Function to generate a report based on the parameters set in the URL
	 */
	public function action_report() {
		$this->response->headers('Content-Type', 'application/pdf');
        $this->response->body(file_get_contents(DOCROOT . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR . 'location_stats.pdf'));
		return;
        $data = $this->request->query();
        if (empty($data) || !isset($data['report_type'])) {
            die('Sorry, invalid request!');// TODO: Add 400 error page here
        }
        $report_type = $data['report_type'];
        // get report data
        unset($data['report_type']);
        $params = $data;
        
        foreach ($params as $k => $p){
            if (is_numeric($p))
                $params[$k] = (int) $p;
        }
        // Display the report on browser
        //die(var_dump($params));
        $jasper = new JasperReport($this->response);
        $jasper->generate_report($report_type , $params, 'I');
    }
	
	/**
     * Run any functionality that needs to be performed after the action
     */
    public function after() {
        /*if (!Request::$current->is_ajax()) {
            // Clean up messages & cookies
            $this->_clean_up_msg();
			Cookie::delete('active_record');
			if (strtolower(Request::$current->action()) !== 'report')
            // Run anything that needs to run after this.
            parent::after();
        }*/
        parent::after();
		if(Auth::instance()->logged_in()){
        	if(Auth::instance()->logged_in('admin')||Auth::instance()->logged_in('school_admin')){
				//$this->_set_msg('You are now logged in as an administrator','success');
			}else{
				$this->_set_msg('You need to login as an administrator to view that page','error');
				$this->redirect('home');
			}   
        }
    }
	/*
	 * Function to toggle publish state of resource to true
	 * @param string Model Name
	 * @param array() Post array with ids
	 */
	protected function publish($model_name,$post) {
		if ($post) {
			if (isset($post['ids']) and is_array($post['ids'])) {
				foreach ($post['ids'] as $key => $value) {
					$model = ORM::factory($model_name, $value);
					if ($model -> loaded()) {
						try {
							$model -> published = 1;
							$model -> save();
						} catch(Exception $e) {
							$this -> _set_msg(' oops something went wrong, try again later', 'error', TRUE);
						}
					}
				}
				$this -> _set_msg('Toggled publsihed state', 'success', TRUE);
			}
		}
	}
	/*
	 * Function to toggle unpublish state of resource to true
	 * @param string Model Name
	 * @param array() Post array with ids
	 */
	protected function unpublish($model_name,$post) {
		if ($post) {
			if (isset($post['ids']) and is_array($post['ids'])) {
				foreach ($post['ids'] as $key => $value) {
					$model = ORM::factory($model_name, $value);
					if ($model -> loaded()) {
						try {
							$model -> published = 0;
							$model -> save();
						} catch(Exception $e) {
							$this -> _set_msg(' oops something went wrong, try again later', 'error', TRUE);
						}
					}
				}
				$this -> _set_msg('Toggled publsihed state', 'success', TRUE);
			}
		}
	}
	

}