<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_Announcements extends Controller_Admin {

	public function action_index() {
		$announcements = ORM::factory('Announcement');
		// perform a model function that filters a datset
		$search_field = array('subject');
		//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this -> _search_context;
		//Arr::get($this->_request_params, 'search');
		$list_columns = $announcements -> get_announcements($search_field, $search_value);
		// Set up pagination params
		if($this->request->post('fk_school_id')){
			$announcements->where('fk_school_id','=',$this->request->post('fk_school_id'));
			$this->_template->set('active_school_id', $this->request->post('fk_school_id'));
		}
		if($this->request->post('fk_country_id')){
			$announcements->where('fk_country_id','=',$this->request->post('fk_country_id'));
			$this->_template->set('active_country_id', $this->request->post('fk_country_id'));
		}
		if($this->request->post('fk_education_level_id')){
			$announcements->where('fk_education_level_id','=',$this->request->post('fk_education_level_id'));
			$this->_template->set('active_education_level_id', $this->request->post('fk_education_level_id'));
		}
		$pagination = $this -> _setup_pagination($announcements, $list_columns);
		//$this->template->pagination = $paging->render();
		$this -> _template -> set('pagination_data', $pagination);
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		$announcements -> order_by('date', 'DESC');
		$content_array = $announcements -> find_all();
		$countries = ORM::factory('Country')->find_all();
		$educational_levels = ORM::factory('EducationLevel')->find_all();
		$schools = ORM::factory('School')->find_all();
		$this->_template->set('schools', $schools);
		$this->_template->set('countries', $countries);
		$this->_template->set('educational_levels', $educational_levels);
		$this -> _template -> set('content_data', $content_array);
		if(count($content_array)== 0){
			$this -> _template -> set('no_data', 'There are currently no announcements saved in the database.');
		}
		if($this->request->is_ajax())			
			$this->_set_content('announcement-list-items');
		else
			$this -> _set_content('announcements');		

	}

	/**
	 * Edit an Announcement's Details
	 * if GET request returns details for a particular Announcement
	 * if POST request saves or updates a Announcement
	 */
	public function action_edit() {
		$schools = ORM::factory('School') -> find_all();
		$levels = ORM::factory('EducationLevel') -> find_all();
		$countries = ORM::factory('Country') -> find_all();
		$this -> _template -> set('schools', $schools);
		$this -> _template -> set('levels', $levels);
		$this -> _template -> set('countries', $countries);
		if ($this -> request -> post()) {
			$post = $this -> request -> post();
			//STEP1: Save Announcement Details
			$announcement = ORM::factory('Announcement', $post['announcement_id']);
			$announcement -> values($post);
			$announcement -> date = date('Y-m-d H-i-s');
			$announcement -> save();
			//STEP2: Return message response
			$this -> _set_msg("Successfully saved Announcement", "success", $announcement -> as_array());
		}
		//Retrieve existing Announcement
		$role = Auth::instance() -> logged_in('school_admin');
		$this -> _template -> set('role', $role);
		if ($this -> request -> query('announcement_id')) {
			//Get the announcement
			$model = ORM::factory('Announcement', $this -> request -> query('announcement_id'));
			$this -> _template -> set('announcement', $model -> as_array());
			$this -> _set_content('announcement-form');

		}
		//Send back empty form for new announcement
		else {
			$this -> _set_content('announcement-form');
		}
	}

	public function action_delete() {
		$post = $this -> request -> post();
		if ($post) {
			if (isset($post['ids']) and is_array($post['ids'])) {
				foreach ($post['ids'] as $key => $value) {
					$announcement = ORM::factory('Announcement', $value);
					if ($announcement -> loaded()) {
						try {
							$announcement -> delete();
							$this -> _set_msg('Deleted announcement(s)', 'success', TRUE);
						} catch(Exception $e) {
							$this -> _set_msg(' oops something went wrong, try again later', 'error', TRUE);
						}
					}
				}
			} elseif ($post['id']) {
				$announcement = ORM::factory('Announcement', $post['id']);
				try {
					$announcement -> delete();
					$this -> _set_msg('Deleted announcement(s)', 'success', TRUE);
				} catch(Exception $e) {
					$this -> _set_msg(' oops something went wrong, try again later', 'error', TRUE);
				}
			}
		}
	}

} // End Default
