<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Admin_Categories extends Controller_Admin
{ 
	/**
	 * Function to test messaging
	 */
	public function action_index() {
		$categories = ORM::factory('Category');
		// perform a model function that filters a datset
		$search_field = array('title', 'short_name');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $categories->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($categories, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		foreach ($categories->find_all() as $key => $model) {
			$content_array[] = array_merge(
					$model->as_array(),
					array(
						'num' => $this->_numbering,
						'active' => ($model->category_id == $active_record_id),
						'parent' => $model->parent_category->short_name
					)
				);// $this->model_to_api($model);
			$this->_numbering++;
		}
		$this->_template->set('content_data', $content_array);
		
		$this->_set_search_context(I18n::get("nav.admin.search.category"));
		$this->_set_content('category_list');
		// Manually set current menu
		$this->_set_current_page('admin/dashboard');
	}
	
	/**
	 * Function to handle category editing
	 */
	public function action_edit() {
		$id = $this->request->param('id');
        // Handle POST
        if (array_key_exists('save', $this->request->post())) {
            $category = ORM::factory('Category', $id);
            // bind values to table columns
            $fields_to_update = array('category_id', 'title', 'short_name','fk_parent_id');
            $category->values($this->request->post(), $fields_to_update);
			
			try{
				$category->save();
                // Update user message
                if ($id) {
                    $this->_set_msg('Successfully updated changes to the category!', 'success');
                    $redirect = Cookie::get('ref');
                    Cookie::delete('ref');
                    Cookie::set('active_record', $category->category_id);
                } else {
                    $this->_set_msg('Successfully added a new category!', 'success');
                    $redirect = 'admin/categories';
                }
                // redirect
                $this->redirect($redirect);
			} catch (ORM_Validation_Exception $e){
				$this->_set_msg('Please correct the errors below and try again!', 'error');
				$this->_template->set('errors', $e->errors('models'));
			}
        }elseif(array_key_exists('cancel', $this->request->post())){
        	$this->_set_msg('No changes were made!', 'info');
        	if ($id) {
                $redirect = Cookie::get('ref');
                Cookie::delete('ref');
                Cookie::set('active_record', $id);
            } else {
                $redirect = 'admin/categories';
            }
            // redirect
            $this->redirect($redirect);
        } else {
            // set referrer page
            if ($id && $this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/categories');
        }
		
		$other_categories = array();
        $categories = ORM::factory('Category');
		$category = null;
		foreach ($categories->find_all() as $key => $model) {
			if ($model->category_id == $id){
				$category = $model->as_array();
			}else{
				$other_categories[] = $model->as_array();
			}
		}
		$this->_template->set('content_data', $category);
		$this->_template->set('other_categories', $other_categories);
		
		if ($category) 
			$this->_set_breadcrumbs_context($category['short_name'], true);
		else
			$this->_set_breadcrumbs_context('Add Category');
		
        $this->_set_content('category_edit');
		// Manually set current menu
		//$this->_set_current_page('admin/categories');
    }
	
	/**
	 * Function to add a new category
	 */
	public function action_add() {
		$this->action_edit();
	}
	
	/**
	 * Function to delete a category
	 */
	public function action_delete() {
        $id = $this->request->param('id');
		$category = ORM::factory('Category', $id);
		if ($this->request->post()){
			if (array_key_exists('delete', $this->request->post())) {
				try{
					if ($category->loaded()) {
						$category->delete();
						$this->_set_msg('Successfully deleted the category!', 'success');
					}
				}catch (Exception $e){
					$this->_set_msg('The category could not be deleted!', 'error');
				}
			}elseif(array_key_exists('cancel', $this->request->post())){
	        	$this->_set_msg('Record delete cancelled!', 'info');
	        }
			// redirect back
            $redirect = Cookie::get('ref');
            Cookie::delete('ref');
            Cookie::set('active_record', $id);
            $this->redirect($redirect);
		} else {
            // set referrer page
            if ($this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/categories');
        }
		$this->_set_breadcrumbs_context($category->title, true);
		$this->_template->set('content_data', $category);
		$this->_set_content('category_delete');
    }
 }