<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_Countries extends Controller_Admin {

	public function action_index()
	{
		$countries = ORM::factory('Country');
		// perform a model function that filters a datset
		$search_field = array('country_name');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $countries->get_countries($search_field, $search_value);
		// Set up pagination params
		$pagination = $this->_setup_pagination($countries, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		$content_array = $countries->order_by('country_name')->find_all();
		$this->_template->set('content_data', $content_array);
		$this->_set_content('countries');
		
	}
	public function action_edit()
	{
		If($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Country',$post['country_id']);
			$model->values($post);
			try{
				$model->save();
				$this->_set_msg("Updated Country List", "success",$model->as_array());
			}catch(ORM_Validation_Exception $e){
				$model->save();
				$this->_set_msg("Opps something went wrong", "error",$model->as_array());
			}
			
		}
		if($this->request->query()){
			$model = ORM::factory('Country',$this->request->query('country_id'));
			$this->_set_msg("Updated Subject List", "success",$model->as_array());
		}
	}

} // End Default