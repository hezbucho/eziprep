<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Admin_Dashboard extends Controller_Admin
 { 
	/**
	 * Function to display index page
	 */
	public function action_index() {
		$content = array();
		/*foreach ($this->_widgets() as $name => $widget) {
			$params = '';
			$config_path = APPPATH . 'widgets/' . $name . '/config.php';
			//include widget config if exists (config file optional)
			if (file_exists($config_path)){
				include($config_path);
				// create url from config
				$config = (isset($config)) ? $config : array('params'=> '');
				$paramkeys = explode(',',$config['params']);
				foreach ($paramkeys as $pk => $pv) {
					if (array_key_exists($pv,$config)){
						$paramkeys[$pk] = $config[$pv];
					}
				}
				$params = implode(',', $paramkeys);
			}
			//echo $name . ' params: ' . $params . '<br />';
			$url = 'widget/' . $name . '/' . $params;
			$content['widget_' . $widget->position][$widget->order] = Request::factory($url)->execute();
		}*/
		
		$this->_set_content('dashboard');
		
		// Generate categories widget info
		$categories = ORM::factory('Category');
		$categories_array = array();
		foreach ($categories->find_all() as $key => $model) {
			$categories_array[] = array_merge(
					$model->as_array(),
					array(
						'datasets_count' => $model->datasets->count_all()
					)
				);
		}
		$this->_template->set('categories', $categories_array);
		$this->_template->set('categories_count', count($categories_array));
		
		// Generate datasets widget info
		$datasets = ORM::factory('Dataset');
		$datasets_array = array();
		foreach ($datasets->find_all() as $key => $model) {
			$datasets_array[] = $model->as_array();
		}
		$this->_template->set('datasets', $datasets_array);
		$this->_template->set('datasets_count', count($datasets_array));
		
		// generate latest news widget info
		$latest_feeds = array();
		try {
			$feed_urls = array(
				'Aid Info (Development Initiatives devinit.org)' => 'http://feeds.feedburner.com/aidinfo?format=xml',
				'HumanIPO' => 'http://www.humanipo.com/rss',
				'News from Africa' => 'http://africa.peacelink.org/feeds/newsfromafrica.rss',
				'Techweez' => 'http://feeds.feedburner.com/techweez?format=xml',
			);
			$search_phrase = 'open data';
			// regex for html to strip out from our feed descriptions
			$search_regex = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
	               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
	               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
	               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
			);
			
			foreach ($feed_urls as $name => $url) {
				$feeds = Feed::parse($url);
				foreach ($feeds as $feed) {
					if ( (Arr::get($feed, 'category') && ( (is_array($feed['category']) && in_array($search_phrase, $feed['category'])) || (is_string($feed['category']) && stripos($feed['category'], $search_phrase) !== false) )) || stripos(Arr::get($feed, 'title', ''), $search_phrase) !== false || stripos(Arr::get($feed, 'description', ''), $search_phrase) !== false ){
						$feed['day'] = date('d', strtotime($feed['pubDate']));
						$feed['month'] = date('M', strtotime($feed['pubDate']));
						$feed['description'] = $this->trim_text($feed['description'], 150);//$this->tokenTruncate(preg_replace($search_regex, '', $feed['description']), 150);
						$latest_feeds[strtotime($feed['pubDate'])] = $feed;
					}
				} 
			}
			// sort feeds by timestamp in descending order (latest first)
			krsort($latest_feeds);
		} catch (Exception $e) {
			
		}
		$this->_template->set('latest_feeds', $latest_feeds);
		
		// generate location data
		$location_data = array();
		// pull dB stats
		$query = DB::query(Database::SELECT, 'SELECT COUNT(DISTINCT counties.county_id) AS db_counties, COUNT(DISTINCT constituencies.constituency_id) AS db_constituencies, COUNT(DISTINCT wards.ward_id) AS db_wards, COUNT(DISTINCT wardlocations.wardlocation_id) AS db_wardlocations FROM counties LEFT JOIN constituencies ON (counties.county_id = constituencies.fk_county_id) LEFT JOIN wards ON (constituencies.constituency_id = wards.fk_constituency_id) LEFT JOIN wardlocations ON (wards.ward_id = wardlocations.fk_ward_id)');
		$result = $query->execute();
		$db_stats = $result[0];
		// get actual stats
		$actual_location_stats = array('actual_counties' => 47, 'actual_constituencies' => 290, 'actual_wards' => 1418, 'actual_wardlocations' => 6854);
		$this->_template->set('location_data', array_merge($db_stats, $actual_location_stats));
	}

	/**
	 * trims text to a space then adds ellipses if desired
	 * @param string $input text to trim
	 * @param int $length in characters to trim to
	 * @param bool $ellipses if ellipses (...) are to be added
	 * @param bool $strip_html if html tags are to be stripped
	 * @return string
	 */
	private function trim_text($input, $length, $ellipses = true, $strip_html = true) {
		 if(empty($input)) return $input ;
	    //strip tags, if desired
	    if ($strip_html) {
	    	$search_regex = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
	               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
	               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
	               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
			);
	        $input = preg_replace($search_regex, '', $input);//strip_tags($input);
	    }
	  
	    //no need to trim, already shorter than trim length
	    if (strlen($input) <= $length) {
	        return $input;
	    }
	  
	    //find last space within length
	    $last_space = strrpos(substr($input, 0, $length), ' ');
	    $trimmed_text = substr($input, 0, $last_space);
		
		// The commented block proposes a change that handles multiple spaces between words and doesn't restrict a space or blank to a "space" 
		/*$output = substr($input,0,$length);
            
        //normals words are seldom more than 30 chars
        $pos = 0 ;
        $found = false ;
        
        for($i = $length ; $i >= 0 ; $i--) {
             if(ctype_space($output[$i])) {
                $found = true ;
                break ;
             }
             $pos++ ;
        }
        
        if($found && ($pos > 0)) {
            $output = substr($output,0,($length-$pos));
            $output = rtrim($output) ;
        }*/
	  
	    //add ellipses (...)
	    if ($ellipses) {
	        $trimmed_text .= '...';
	    }
	  
	    return $trimmed_text;
	}

	private function tokenTruncate($string, $your_desired_width) {
	  $parts = preg_split('/([\s\n\r]+)/', $string, null, PREG_SPLIT_DELIM_CAPTURE);
	  $parts_count = count($parts);
	
	  $length = 0;
	  $last_part = 0;
	  for (; $last_part < $parts_count; ++$last_part) {
	    $length += strlen($parts[$last_part]);
	    if ($length > $your_desired_width) { break; }
	  }
	
	  return implode(array_slice($parts, 0, $last_part));
	}

	/**
	 * Function to test messaging
	 */
	public function action_x() {
		$health_facilities = ORM::factory('HealthFacility');
		// perform a model function that filters a datset
		$search_field = Arr::get($this->_request_params, 'search_field' );
		$search_value = Arr::get($this->_request_params, 'search');
		$list_columns = $health_facilities->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($health_facilities, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Send back the list
		$content_array = array();
		foreach ($health_facilities->find_all() as $key => $model) {
			$content_array[] = $model->as_array();// $this->model_to_api($model);
		}
		$this->_template->set('content_data', $content_array);
		
		$this->_set_content('dashboard');
		// Manually set current menu
		$this->_set_current_page('admin/dashboard');
	}
	
	/**
	 * Function to sort widgets
	 */
	public function action_sort() {
		if (isset($_POST) && count($_POST)) {
			$position = substr($_POST['pos'],8);
			$order_array = array_flip($_POST['widget']);
			ARM::factory('widget')->updatePositions($position, $order_array);
		}
	}
 }