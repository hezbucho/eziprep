<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Admin_Datasets extends Controller_Admin
 { 
	/**
	 * Function to list datasets
	 */
	public function action_index() {
		$datasets = ORM::factory('Dataset');
		// perform a model function that filters a datset
		$search_field = 'title';//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $datasets->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($datasets, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		foreach ($datasets->find_all() as $key => $model) {
			$content_array[] = array_merge(
				$model->as_array(),// $this->model_to_api($model);
				array(
					'num' => $this->_numbering,
					'active' => ($model->dataset_id == $active_record_id),
					'parent' => $model->category->short_name
				)
			);
			$this->_numbering++;
		}
		$this->_template->set('content_data', $content_array);
		
		$this->_set_search_context(I18n::get("nav.admin.search.dataset"));
		
		$this->_set_content('dataset_list');
		// Manually set current menu
		$this->_set_current_page('admin/datasets');
	}
	
	/**
	 * Function to browse a dataset
	 */
	public function action_browse() {
		$id = intval($this->request->param('id'));
		$dataset = ORM::factory('Dataset', $id);
		$table = $dataset->table_name;
		$context_title = $dataset->title;
		$this->_set_search_context("Search " . ucwords(Inflector::humanize($table)));
		$this->_set_breadcrumbs_context($context_title, true);
		$model = str_ireplace(' ', '', ucwords(Inflector::singular(Inflector::humanize($table))));
		$view = $table . '_list'; // view file names take after table names for the dataset with _list/_edit suffixes
		
		$list = ORM::factory($model);
		// perform a model function that filters a datset
		$search_field = Arr::get($this->_request_params, 'search_field' );
		$search_value = Arr::get($this->_request_params, 'search');
		$list_columns = $list->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($list, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Setup table headings
		$headings = array('#');
		$useless_ids = array('geometry');
		foreach ($list_columns as $name => $attribs) {
			if ($name != $list->primary_key()){
				if (strpos($name, 'fk_') !== false){ // foreign key
					// pull related field
					$table = str_ireplace('fk_', '', $name);
					$table = str_ireplace('_id', '', $table);
					if (!in_array($table, $useless_ids))
						$headings[] = ucwords(Inflector::humanize($table));
				}else{
					$headings[] = Inflector::humanize(Inflector::underscore(ucwords($name)));
				}
			}
		}
		
		$this->_template->set('content_headings', $headings);
		
		// Send back the list
		$content_array = array();
		foreach ($list->find_all() as $key => $model) {
			$model_array = $model->as_array();
			// update foreign key fields with actual text
			foreach ($model_array as $field => &$value) {
				if ($field != $model->primary_key()){
					if (strpos($field, 'fk_') !== false){ // foreign key
						// pull related field
						$table = str_ireplace('fk_', '', $field);
						$table = str_ireplace('_id', '', $table);
						$relationship = $model->{$table};
						//$table = str_ireplace(' ', '', ucwords(Inflector::humanize($table))););
						
						if (isset($relationship->{$table . '_name'})){
							$value = $relationship->{$table . '_name'};
						}elseif(isset($relationship->name)){
							$value = $relationship->name;
						}elseif(isset($relationship->title)){
							$value = $relationship->title;
						}else{
							unset($model_array[$field]);
						}
						//if (isset($model_array[$field]))
							//echo $table . " | " . $field . " | $relationship | " . $value . '<br />';
					}
				}
			}
			$content_array[] = array_merge(array('num' => $this->_numbering), $model_array);// $this->model_to_api($model);
			$this->_numbering++;
		}
		$this->_template->set('content_data', $content_array);
		
		$this->_set_content($view);
	}
	
	/**
	 * Function to handle dataset editing
	 */
	public function action_edit() {
		$id = $this->request->param('id');
		$dataset = ORM::factory('Dataset', $id);
        // Handle POST
        if (array_key_exists('save', $this->request->post())) {
            // bind values to table columns
            $fields_to_update = array('dataset_id', 'title', 'table_name','published', 'fk_category_id');
            $dataset->values($this->request->post(), $fields_to_update);
			
			try{
				$dataset->save();
                // Update user message
                if ($id) {
                    $this->_set_msg('Successfully updated changes to the dataset!', 'success');
                    $redirect = Cookie::get('ref');
                    Cookie::delete('ref');
                    Cookie::set('active_record', $dataset->dataset_id);
                } else {
                    $this->_set_msg('Successfully added a new dataset!', 'success');
                    $redirect = 'admin/datasets';
                }
                // redirect
                $this->redirect($redirect);
			} catch (ORM_Validation_Exception $e){
				$this->_set_msg('Please correct the errors below and try again!', 'error');
				$this->_template->set('errors', $e->errors('models'));
				//$this->redirect(''); // TODO: Make it possible for errors to be displayed without a redirect
			}
        }elseif(array_key_exists('cancel', $this->request->post())){
        	$this->_set_msg('No changes were made!', 'info');
        	if ($id) {
                $redirect = Cookie::get('ref');
                Cookie::delete('ref');
                Cookie::set('active_record', $id);
            } else {
                $redirect = 'admin/datasets';
            }
            // redirect
            $this->redirect($redirect);
        } else {
            // set referrer page
            if ($id && $this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/datasets');
        }
		
		$categories = array();
        $categories_list = ORM::factory('Category')->find_all();
		foreach ($categories_list as $key => $model) {
			$categories[] = $model->as_array();
		}
		$this->_template->set('content_data', $dataset);
		$this->_template->set('categories', $categories);
		
		if ($dataset->loaded()) 
			$this->_set_breadcrumbs_context($dataset->title, true);
		else
			$this->_set_breadcrumbs_context('Add Dataset');
		
        $this->_set_content('dataset_edit');
		// Manually set current menu
		//$this->_set_current_page('admin/datasets');
    }
	
	/**
	 * Function to add a new dataset
	 */
	public function action_add() {
		$this->action_edit();
	}
	
	/**
	 * Function to delete a dataset
	 */
	public function action_delete() {
		$id = $this->request->param('id');
		$dataset = ORM::factory('Dataset', $id);
		if ($this->request->post()){
			if (array_key_exists('delete', $this->request->post())) {
				try{
					if ($dataset->loaded()) {
						$dataset->delete();
						$this->_set_msg('Successfully deleted the dataset!', 'success');
					}
				}catch (Exception $e){
					$this->_set_msg('The dataset could not be deleted!', 'error');
				}
			}elseif(array_key_exists('cancel', $this->request->post())){
	        	$this->_set_msg('Record delete cancelled!', 'info');
	        }
			// redirect back
            $redirect = Cookie::get('ref');
            Cookie::delete('ref');
            Cookie::set('active_record', $id);
            $this->redirect($redirect);
		} else {
            // set referrer page
            if ($this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/datasets');
        }
		$this->_set_breadcrumbs_context($dataset->title, true);
		$this->_template->set('content_data', $dataset);
		$this->_set_content('dataset_delete');
        /*if (isset($_POST['cancel'])) {
            $this->_set_msg('Record delete cancelled!');
            $this->request->redirect(Route::url('default', array('action' => ''), true));
        } elseif ((isset($_GET['ajax']) && is_numeric($_GET['ajax'])) || (isset($_POST['delete']) && is_numeric($id))) {
            $member = ARM::factory('member')->get_member($_GET['ajax']);
            $member->delete();
            $this->_set_msg('Successfully deleted record!');
            // redirect back to current page
            $this->request->redirect($this->request->referrer());
        } elseif (is_numeric($id)) {
            $view = $this->_loadView('admin/member/delete');
            $member = ARM::factory('member')->get_member($id);
            $view->id = $id;
            $view->name = $member->member_name;
            $this->_setCurrent('members');
            $this->template->content = $view;
        }*/
    }
 }