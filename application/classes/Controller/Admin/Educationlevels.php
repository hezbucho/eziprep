<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_EducationLevels extends Controller_Admin {

	public function action_index()
	{
		$levels = ORM::factory('EducationLevel');
		// perform a model function that filters a datset
		$search_field = array('level_name');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $levels->getList($search_field, $search_value);
		// Set up pagination params
		$pagination = $this->_setup_pagination($levels, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
				// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		$levels->order_by('level_name');
		foreach ($levels->find_all() as $key => $model) {
			$content_array[] = array_merge(
					$model->as_array(),
					array(
						'num' => $this->_numbering,
						'active' => ($model->education_level_id == $active_record_id),
						
					)
				);
			$this->_numbering++;
		}
		
		$this->_template->set('content_data', $content_array);
			
		$this->_set_content('education-levels');
		
	}
	public function action_edit()
	{
		If($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('EducationLevel',$post['education_level_id']);
			$model->values($post);
			$model->save();
			$this->_set_msg("Updated Education Level List", "success",$model->as_array());
		}
		
		
		if($this->request->query()){
			$model = ORM::factory('EducationLevel',$this->request->query('education_level_id'));
			$this->_set_msg("Updated Education Level List", "success",$model->as_array());
		}
	}

} // End Default