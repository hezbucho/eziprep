<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Controller for all admin actions relating to managing exam sessions
 * @author Joseph Bosire 2013-06-14
 * @version 01  *
 * PHP version 5
 */
class Controller_Admin_ExamSessions extends Controller_Admin {

	public function action_index()
	{
		$sessions = ORM::factory('ExamSession');
		// perform a model function that filters a datset
		$search_field = array('exam_session_title');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $sessions->get_exam_sessions($search_field, $search_value);
		// Set up pagination params
		$pagination = $this->_setup_pagination($sessions, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		// Send back the list
		$this->_template->set('content_data', $sessions->find_all());		
		$this->_set_content('exam-sessions');		
	}
	/**
	
	/**
	 * Edit a Exam session's Details
	 * if GET request returns details for a particular exam session
	 * if POST request saves or updates a exam session
	 */
	public function action_edit(){
		if($this->request->post()){
			$post = $this->request->post();
			//STEP1: Save Exam Session Details 	
			$session = ORM::factory('ExamSession',$post['exam_session_id']);
			$session->values($post);
			$session->save();
			//STEP2: Return message response	
			$this->_set_msg("Successfully saved exam session", "success",$session->as_array());		
		}
		//Retrieve existing exam session
		if($this->request->query('exam_session_id')){
			//Get the exam session
			$model = ORM::factory('ExamSession',$this->request->query('exam_session_id'));
			$this->_template->set('session_data', $model->as_array());
			$this->_set_content('session-form');
			
		}
		//Send back empty form for new exam session
		else{
			$this->_set_content('session-form');
		}
	}

} // End Exam Session Controller