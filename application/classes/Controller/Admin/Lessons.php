<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_Lessons extends Controller_Admin {
		
	/**
	 * Return a list of lessons 
	 *
	 */
	public function action_index()
	{
		$lessons = ORM::factory('Lesson');
		// perform a model function that filters a datset
		$search_field = array('topic_title','level_name','subject_title','country_name');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $lessons->getList($search_field, $search_value);
		if($this->request->post('fk_subject_id')){
			$lessons->where('lesson.fk_subject_id','=',$this->request->post('fk_subject_id'));
		}
		if($this->request->post('fk_country_id')){
			$lessons->where('lesson.fk_country_id','=',$this->request->post('fk_country_id'));
		}
		if($this->request->post('fk_education_level_id')){
			$lessons->where('lesson.fk_education_level_id','=',$this->request->post('fk_education_level_id'));
		}
		// Set up pagination params
		$pagination = $this->_setup_pagination($lessons, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
				// Send back the list
		$content_array = array();
		
		$content_array =$lessons->find_all();
		
		$this->_template->set('content_data', $content_array);
		$countries = ORM::factory('Country')->find_all();
		$educational_levels = ORM::factory('EducationLevel')->find_all();
		$subjects = ORM::factory('Subject')->find_all();
		$this->_template->set('subjects', $subjects);
		$this->_template->set('countries', $countries);
		$this->_template->set('educational_levels', $educational_levels);
		//$this->_set_search_context(I18n::get("nav.admin.search.category"));
		if($this->request->is_ajax())
			$this->_set_content('topic-list-items');
		else
			$this->_set_content('lessons');
		
	}
	/**
	 * Edit a lesson/topic information 
	 *
	 */
	public function action_edit()
	{
		if($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Lesson',$post['lesson_id']);
			$model->values($post);
			$model->save();			
			$this->redirect('admin/lessons');
			
		}
		if($this->request->param('id')){
			$model = ORM::factory('Lesson',$this->request->param('id'));
			$this->_template->set('lesson_data', $model);
					
		}
		$subjects = ORM::factory('Subject')->find_all();		
		$this->_template->set('subject_data', $subjects);
		$levels = ORM::factory('EducationLevel')->find_all();		
		$this->_template->set('education_level_data', $levels);
		$countries = ORM::factory('Country')->find_all();
		$this->_template->set('countries', $countries);
		
		$this->_set_content('edit-lesson');
	}
	/**
	 * Returns a list of subtopics based on a get request with the following params
	 * @param lesson_id
	 *
	 */
	public function action_get_subtopics(){
		if($this->request->query()){
			$lesson_id = $this->request->query('lesson_id');
			$subtopics = ORM::factory('Subtopic')->where('fk_lesson_id','=',$lesson_id)->find_all();
			$this->_template->set('subtopics_data', $subtopics);
			$this->_set_content('subtopics-list-items');
		}
		
	}
	/**
	 * Called to edit or add a new subtopic	 * 
	 *
	 */
	public function action_edit_subtopic(){
		If($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Subtopic',$post['subtopic_id']);
			$model->values($post);
			$model->save();
			$this->_set_msg("Updated Subt opic List", "success",$model->as_array());
		}
		if($this->request->query('subtopic_id')){
			$model = ORM::factory('Subtopic',$this->request->query('subtopic_id'));
			$this->_template->set('subtopic_data', $model);
			$this->_set_content('subtopic-form');
		}else{
			$data = array('fk_lesson_id'=>$this->request->query('lesson_id'));
			$this->_template->set('subtopic_data', $data);
			$this->_set_content('subtopic-form');
		}
	}
	/**
	 * Returns a list of trial questions
	 *
	 */
	public function action_trial_questions(){
		if($this->request->query()){
			$model = ORM::factory('TrialQuestion')
			->select('questions.*','subtopics.subtopic_title')
			->join('subtopics','LEFT')->on('fk_subtopic_id','=','subtopics.subtopic_id')
			->join('questions','LEFT')->on('questions.question_id','=','fk_question_id')
			->where('fk_subtopic_id','=',$this->request->query('subtopic_id'))
			->find_all();
			//var_dump($model);exit;
			$this->_template->set('trial_question_data', $model);
			$this->_set_content('trial-question-list-items');
		}
		
	}
	/**
	 * Called to edit or add a new trial question	 * 
	 *
	 */
	public function action_edit_trial_question(){		
		if($this->request->post()){
			$post = $this->request->post();	
			//var_dump($post['answers']);exit;		
			//save question first
			$question = ORM::factory('Question',$post['fk_question_id']);
			$question->values($post);
			$question->save();
			//Save answers to question
			if(!empty($post['answers'])){
				$is_assoc = Arr::is_assoc($post['answers']);			
				foreach ($post['answers'] as $key => $value) {
					//check if its a fresh bash of answers or an edit to existing answers
					if($is_assoc)
						$answer = ORM::factory('Answer',$key);
					else
						$answer = ORM::factory('Answer');
					$answer->answer = $value;
					$answer->fk_question_id = $question->question_id;
					//if a new batch of answers assume the first indexed value as the correct answer 
					if($key == 0)
						$answer->status = 1;
					$answer->save();
				}
			}
			
			//Save the trial question record
			$model = ORM::factory('TrialQuestion',$post['subtopic_trial_question_id']);
			$model->values($post);
			$model->fk_question_id = $question->question_id;
			$model->save();	
			$this->_set_msg("Successfully saved trial question", "success",$model->as_array());					
		}
		$this->_template->set('question_type', 'trial');
		if($this->request->query('subtopic_trial_question_id')){
			$model = ORM::factory('TrialQuestion',$this->request->query('subtopic_trial_question_id'));
			//var_dump($model);exit;
			$answers = ORM::factory('Answer')->where('fk_question_id','=',$model->fk_question_id)->find_all()->as_array();
			$this->_template->set('question_data', $model->as_array());
			$subtopics = ORM::factory('Subtopic')->where('fk_lesson_id','=',$this->request->query('lesson_id'))->find_all();
			$this->_template->set('subtopic_id', $this->request->query('subtopic_id'));
			$this->_template->set('answers', $answers);
			$this->_set_content('question-form');
			
		}else{
			//var_dump('here');exit;
			//dummy array to determine number of optional answers for a question
			$answers = array(
				array('answer_id'=>'','answer'=>'','status'=>'1'),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
			);
			// $subtopics = ORM::factory('Subtopic')->where('fk_lesson_id','=',$this->request->query('lesson_id'))->find_all();
			//$this->_template->set('subtopic_data', $subtopics->as_array());
			$this->_template->set('answers', $answers);
			$this->_template->set('subtopic_id', $this->request->query('subtopic_id'));
			$this->_set_content('question-form');
		}
		
	}
	/**
	 * Deletes a subtopic 
	 *
	 */
	public function action_delete_subtopic(){
		$model = ORM::factory('Subtopic',$this->request->query('subtopic_id'));
		try{
			$model->delete();
			$this->_set_msg('Successfully deleted subtopic','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}
		
	}
	/**
	 * Deletes a trial question 
	 *
	 */
	public function action_delete_trial_question(){
		$model = ORM::factory('TrialQuestion',$this->request->query('subtopic_trial_question_id'));
		try{
			$model->delete();
			$this->_set_msg('Successfully deleted question','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}
	}

} // End Default