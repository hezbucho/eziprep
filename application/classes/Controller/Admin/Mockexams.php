<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_MockExams extends Controller_Admin {

	public function action_index()
	{
		$mock_exams = ORM::factory('MockExam');
		// perform a model function that filters a datset
		$search_field = array('mock_exam_title','year');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $mock_exams->get_mock_exams($search_field, $search_value);
		//TODO:: find out why this search isnt working teh first time
		if(isset($search_value)){
			$mock_exams->or_where_open();
			$mock_exams->where('fk_subject_id','=',Model_Subject::get_subject_by_name($search_value));
			$mock_exams->or_where('mockexam.fk_country_id','=',Model_Country::get_country_by_name($search_value));
			$mock_exams->or_where('fk_education_level_id','=',Model_EducationLevel::get_level_by_name($search_value));
			$mock_exams->or_where_close();
		}
		
						
		if($this->request->post('fk_subject_id')){
			$mock_exams->where('fk_subject_id','=',$this->request->post('fk_subject_id'));
			$this->_template->set('active_subject_id', $this->request->post('fk_subject_id'));
		}
		if($this->request->post('fk_country_id')){
			$mock_exams->where('mockexam.fk_country_id','=',$this->request->post('fk_country_id'));
			$this->_template->set('active_country_id', $this->request->post('fk_country_id'));
		}
		if($this->request->post('fk_education_level_id')){
			$mock_exams->where('fk_education_level_id','=',$this->request->post('fk_education_level_id'));
			$this->_template->set('active_education_level_id', $this->request->post('fk_education_level_id'));
		}
		
		// Set up pagination params
		$pagination = $this->_setup_pagination($mock_exams, $list_columns);
		//$this->template->pagination = $paging->render();
		
        $this->_template->set('pagination_data', $pagination);
				// Send back the list
		$countries = ORM::factory('Country')->find_all();
		$educational_levels = ORM::factory('EducationLevel')->find_all();
		$subjects = ORM::factory('Subject')->find_all();
		$this->_template->set('subjects', $subjects);
		$this->_template->set('countries', $countries);
		$this->_template->set('educational_levels', $educational_levels);		
		//$this->_set_search_context(I18n::get("nav.admin.search.pastquestions"));	
		$content_array = $mock_exams->find_all();		
		$this->_template->set('content_data', $content_array);
		if($this->request->is_ajax())	
			$this->_set_content('mock-exam-list-items');
		else
			$this->_set_content('mock-exams');	
		
		//$this->_set_search_context(I18n::get("nav.admin.search.category"));
		
		
	}
	public function action_edit()
	{
		if($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('MockExam',$post['mock_exam_id']);
			$model->values($post);
			$model->save();			
			$this->redirect('admin/mockexams');
			
		}
		if($this->request->param('id')){
			$model = ORM::factory('MockExam',$this->request->param('id'));
			$sections = $model->sections->find_all();
			$this->_template->set('sections', $sections);
			$this->_template->set('mock_exam_data', $model);
					
		}
		$subjects = ORM::factory('Subject')->find_all();		
		$this->_template->set('subject_data', $subjects);
		$levels = ORM::factory('EducationLevel')->find_all();
		$countries = ORM::factory('Country')->find_all();
		$this->_template->set('countries', $countries);		
		$this->_template->set('education_level_data', $levels);		
		$this->_set_content('edit-mock-exam');
		
	}
	/**
	 * Returns a list of sections based on a get request with the following params
	 * @param lesson_id
	 *
	 */
	public function action_get_sections(){
		if($this->request->query()){
			$mock_exam_id = $this->request->query('mock_exam_id');
			//TODO: Move function to model
			$sections = ORM::factory('Section')->where('fk_mock_exam_id','=',$mock_exam_id)->find_all();
			$this->_template->set('sections_data', $sections);
			$this->_set_content('sections-list-items');
		}
		
	}
	/**
	 * Called to edit or add a new section	 * 
	 *
	 */
	public function action_edit_section(){
		If($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Section',$post['section_id']);
			$model->values($post);
			$model->save();
			$this->_set_msg("Updated Section List", "success",$model->as_array());
		}
		if($this->request->query('section_id')){
			$model = ORM::factory('Section',$this->request->query('section_id'));
			$this->_template->set('section_data', $model);
			$this->_set_content('section-form');
		}else{
			$data = array('fk_mock_exam_id'=>$this->request->query('mock_exam_id'));
			$this->_template->set('section_data', $data);
			$this->_set_content('section-form');
		}
	}
	/**
	 * Returns a list of mock questions
	 *
	 */
	public function action_mock_exam_questions(){
		if($this->request->query()){
			$section_id = $this->request->query('section_id');
			$model = ORM::factory('MockExamQuestion')
			->select('questions.*','section_title')
			->join('sections','LEFT')->on('fk_section_id','=','sections.section_id')
			->join('questions','LEFT')->on('questions.question_id','=','fk_question_id')
			->where('mockexamquestion.fk_mock_exam_id','=',$this->request->query('mock_exam_id'));
			if($section_id){
				$model->where('fk_section_id','=',$section_id);
			}else{
				$model->order_by('mockexamquestion.order','ASC');
			}
			$questions = $model->find_all();
			//var_dump($model);exit;
			$this->_template->set('mock_exam_question_data', $questions);
			$this->_set_content('mock-question-list-items');
		}
		
	}
		public function action_get_section_content(){
		$this->_template_layout = 'admin-basic';
		$this->before();
		if($this->request->query()){
			$section_id = $this->request->query('section_id');
			$model = ORM::factory('MockExamQuestion')
			->select('questions.*','section_title')
			->join('sections','LEFT')->on('fk_section_id','=','sections.section_id')
			->join('questions','LEFT')->on('questions.question_id','=','fk_question_id')
			->where('mockexamquestion.fk_mock_exam_id','=',$this->request->query('mock_exam_id'));
			if($section_id){
				$model->where('fk_section_id','=',$section_id);
			}else{
				$model->order_by('mockexamquestion.order','ASC');
			}
			$questions = $model->find_all();
			//var_dump($model);exit;
			$this->_template->set('mock_exam_question_data', $questions);
			$this -> _template -> set('mock_exam_id', $this -> request -> query('mock_exam_id'));
			$sections = ORM::factory('Section')->where('fk_mock_exam_id','=',$this->request->query('mock_exam_id'))->find_all();
			$this->_template->set('sections', $sections->as_array());
			$this->_set_content('mock-question-list');
		}
		
	}
	/**
	 * Called to edit or add a new trial question	 * 
	 *
	 */
	public function action_edit_mock_question(){		
		if($this->request->post()){
			$post = $this->request->post();
				
			//TODO: Move this routine to question model for reuse
			$question = ORM::factory('Question',$post['fk_question_id']);
			$question->values($post);
			$question->save();
			//Save answers to question
			if(!empty($post['answers'])){
				$is_assoc = Arr::is_assoc($post['answers']);			
				foreach ($post['answers'] as $key => $value) {
					//check if its a fresh bash of answers or an edit to existing answers
					if($is_assoc)
						$answer = ORM::factory('Answer',$key);
					else
						$answer = ORM::factory('Answer');
					$answer->answer = $value;
					$answer->fk_question_id = $question->question_id;
					//if a new batch of answers assume the first indexed value as the correct answer 
					if($key == 0)
						$answer->status = 1;
					$answer->save();
				}
			}
			
			//Save the mock exam question record
			$model = ORM::factory('MockExamQuestion',$post['mock_exam_question_id']);
			$model->values($post);
			$model->fk_question_id = $question->question_id;
			$model->save();	
			$this->_set_msg("Successfully saved mock exam question", "success",$model->as_array());					
		}
		$this->_template->set('question_type', 'mock');
		if($this->request->query('mock_exam_question_id')){
			$model = ORM::factory('MockExamQuestion',$this->request->query('mock_exam_question_id'));
			$answers = ORM::factory('Answer')->where('fk_question_id','=',$model->fk_question_id)->find_all()->as_array();
			$this->_template->set('question_data', $model->as_array());
			$sections = ORM::factory('Section')->where('fk_mock_exam_id','=',$this->request->query('mock_exam_id'))->find_all();
			$this->_template->set('section_data', $sections->as_array());
			$this->_template->set('answers', $answers);
			$this->_set_content('question-form');
			
		}else{
			
			$answers = array(
				array('answer_id'=>'','answer'=>'','status'=>'1'),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
			);
			$sections = ORM::factory('Section')->where('fk_mock_exam_id','=',$this->request->query('mock_exam_id'))->find_all();
			$this->_template->set('section_data', $sections->as_array());
			$this->_template->set('answers', $answers);
			$this->_set_content('question-form');
		}
		
	}
	/**
	 * Deletes a subtopic 
	 *
	 */
	public function action_delete_subtopic(){
		$model = ORM::factory('Subtopic',$this->request->query('subtopic_id'));
		try{
			$model->delete();
			$this->_set_msg('Successfully deleted subtopic','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}
		
	}
	/**
	 * Deletes a trial question 
	 *
	 */
	public function action_delete_trial_question(){
		$model = ORM::factory('TrialQuestion',$this->request->query('subtopic_trial_question_id'));
		try{
			$model->delete();
			$this->_set_msg('Successfully deleted question','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}
	}
	public function action_order_questions(){
		if($this->request->post('order')){
			$order_array = $this->request->post('order');			
			foreach ($order_array as $key => $value) {
				 $id = (int)substr($value, 9);				
				 $model = ORM::factory('MockExamQuestion',$id);
				 //Added plus one so that order starts at number 1 instead of 0
				 $model->order = $key+1;
				 $model->save();
			}
			$this->_set_msg('Reorder was completed','success',TRUE);
			
		}
	}
	public function action_publish(){
		if($this->request->post()){
			$this->publish('MockExam', $this->request->post());
		}
	}
	public function action_unpublish(){
		if($this->request->post()){
			$this->unpublish('MockExam', $this->request->post());
		}
	}
} // End Default