<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_PastQuestions extends Controller_Admin {

	public function action_index() {
		$pastquestions = ORM::factory('PastQuestion');
		// perform a model function that filters a datset
		$search_field = array('past_question_title', 'year');
		//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this -> _search_context;
		//Arr::get($this->_request_params, 'search');
		$list_columns = $pastquestions -> get_past_questions($search_field, $search_value);
		if(isset($search_value)){
			$pastquestions->or_where_open();
			$pastquestions->where('fk_subject_id','=',Model_Subject::get_subject_by_name($search_value));
			$pastquestions->or_where('pastquestion.fk_country_id','=',Model_Country::get_country_by_name($search_value));
			$pastquestions->or_where('fk_education_level_id','=',Model_EducationLevel::get_level_by_name($search_value));
			$pastquestions->or_where_close();
		}
		if ($this -> request -> post('fk_subject_id')) {
			$pastquestions -> where('fk_subject_id', '=', $this -> request -> post('fk_subject_id'));
			$this -> _template -> set('active_subject_id', $this -> request -> post('fk_subject_id'));
		}
		if ($this -> request -> post('fk_country_id')) {
			$pastquestions -> where('pastquestion.fk_country_id', '=', $this -> request -> post('fk_country_id'));
			$this -> _template -> set('active_country_id', $this -> request -> post('fk_country_id'));
		}
		if ($this -> request -> post('fk_education_level_id')) {
			$pastquestions -> where('fk_education_level_id', '=', $this -> request -> post('fk_education_level_id'));
			$this -> _template -> set('active_education_level_id', $this -> request -> post('fk_education_level_id'));
		}
		// Set up pagination params
		$pagination = $this -> _setup_pagination($pastquestions, $list_columns);
		//$this->template->pagination = $paging->render();
		$this -> _template -> set('pagination_data', $pagination);
		// Send back the list
		$this -> _template -> set('content_data', $pastquestions -> find_all());
		$countries = ORM::factory('Country') -> find_all();
		$educational_levels = ORM::factory('EducationLevel') -> find_all();
		$subjects = ORM::factory('Subject') -> find_all();
		$this -> _template -> set('subjects', $subjects);
		$this -> _template -> set('countries', $countries);
		$this -> _template -> set('educational_levels', $educational_levels);
		//$this->_set_search_context(I18n::get("nav.admin.search.pastquestions"));
		if ($this -> request -> is_ajax())
			$this -> _set_content('past-question-list-items');
		else
			$this -> _set_content('past-questions');

	}

	/**
	 * Called to edit a particular test
	 */
	public function action_edit() {
		if ($this -> request -> post()) {
			$post = $this -> request -> post();
			$model = ORM::factory('PastQuestion', $post['past_question_id']);
			$model -> values($post);
			$model -> save();
			$this -> redirect('admin/pastquestions');
		}
		if ($this -> request -> param('id')) {
			$model = ORM::factory('PastQuestion', $this -> request -> param('id'));
			$this -> _template -> set('pastquestion_data', $model);
		}
		$subjects = ORM::factory('Subject') -> find_all();
		$this -> _template -> set('subjects', $subjects);
		$levels = ORM::factory('EducationLevel') -> find_all();
		$this -> _template -> set('education_level_data', $levels);
		$countries = ORM::factory('Country') -> find_all();
		$this -> _template -> set('countries', $countries);
		$exam_sessions = ORM::factory('ExamSession') -> find_all();
		$this -> _template -> set('exam_session_data', $exam_sessions);
		$this -> _set_content('edit-past-question');
	}

	/**
	 * Returns a list of sections based on a get request with the following params
	 * @param lesson_id
	 *
	 */
	public function action_get_sections() {
		if ($this -> request -> query()) {
			$past_question_id = $this -> request -> query('past_question_id');
			$sections = ORM::factory('Section') -> where('fk_past_question_id', '=', $past_question_id) -> find_all();
			$this -> _template -> set('sections_data', $sections);
			$this -> _set_content('sections-list-items');
		}

	}

	/**
	 * Called to edit or add a new section	 *
	 *
	 */
	public function action_edit_section() {
		If ($this -> request -> post()) {
			$post = $this -> request -> post();
			$model = ORM::factory('Section', $post['section_id']);
			$model -> values($post);
			$model -> save();
			$this -> _set_msg("Updated Section List", "success", $model -> as_array());
		}
		if ($this -> request -> query('section_id')) {
			$model = ORM::factory('Section', $this -> request -> query('section_id'));
			$this -> _template -> set('section_data', $model);
			$this -> _set_content('section-form');
		} else {
			$data = array('fk_past_question_id' => $this -> request -> query('past_question_id'));
			$this -> _template -> set('section_data', $data);
			$this -> _set_content('section-form');
		}
	}

	/*
	 * GET request to get a list of questions for a particular set of past questions
	 * Expects query parameter "test_id" to be passed
	 */
	public function action_get_past_questions() {
		if ($this -> request -> query('past_question_id')) {
			$questions = ORM::factory('PastQuestionQuestion') -> get_past_questions($this -> request -> query('past_question_id'));
			$this -> _template -> set('question_data', $questions);
			$this -> _template -> set('defaultContext', 'past');
			//var_dump($questions);exit;
			$this -> _set_content('past-questions-list-items');
		}

	}
	public function action_get_section_content() {
		$this->_template_layout = 'admin-basic';
		$this->before();
		if ($this -> request -> query('past_question_id')) {
			$questions = ORM::factory('PastQuestionQuestion') -> get_past_questions($this -> request -> query('past_question_id'));
			$this -> _template -> set('question_data', $questions);
			$this -> _template -> set('defaultContext', 'past');
			$this -> _template -> set('past_question_id', $this -> request -> query('past_question_id'));
			//var_dump($questions);exit;
			$this -> _set_content('past-questions-list');
		}

	}

	/**
	 * Edit a Test Question's Details
	 * if GET request returns details for a particular test question
	 * if POST request saves or updates a test question
	 */
	public function action_edit_past_question() {
		if ($this -> request -> post()) {
			$post = $this -> request -> post();
			//STEP1: Save Question Details
			$question = ORM::factory('Question', $post['fk_question_id']);
			$question -> values($post);
			$question -> save();
			//STEP2: Save answers to question
			$answers = ORM::factory('Answer') -> save_answers($post, $question -> question_id);
			//STEP3: Save the trial question record
			$model = ORM::factory('PastQuestionQuestion', $post['past_question_questions_id']);
			$model -> values($post);
			$model -> fk_question_id = $question -> question_id;
			$model -> save();
			//STEP4: Return message response
			$this -> _set_msg("Successfully saved past question", "success", $model -> as_array());
		}
		//Used by shared modal to distinguish type of qestion in modal window
		$this -> _template -> set('question_type', 'past');

		//Retrieve existing trial question
		if ($this -> request -> query('past_question_questions_id')) {
			//Get the test question
			$model = ORM::factory('PastQuestionQuestion', $this -> request -> query('past_question_questions_id'));
			//Get answers for particular question
			$answers = ORM::factory('Answer') -> get_question_answers($model -> fk_question_id) -> as_array();
			$this -> _template -> set('question_data', $model -> as_array());
			$this -> _template -> set('answers', $answers);
			$sections = ORM::factory('Section') -> where('fk_past_question_id', '=', $this -> request -> query('past_question_id')) -> find_all();
			$this -> _template -> set('section_data', $sections -> as_array());
			$this -> _set_content('question-form');

		}
		//Send back empty form for new trial question
		else {
			//dummy array to determine number of optional answers for a question
			$answers = Model_Answer::$default_answers;
			$this -> _template -> set('answers', $answers);
			$sections = ORM::factory('Section') -> where('fk_past_question_id', '=', $this -> request -> query('past_question_id')) -> find_all();
			$this -> _template -> set('section_data', $sections -> as_array());
			$this -> _set_content('question-form');
		}
	}

	public function action_publish() {
		if ($this -> request -> post()) {
			$this -> publish('PastQuestion', $this -> request -> post());
		}
	}

	public function action_unpublish() {
		if ($this -> request -> post()) {
			$this -> unpublish('PastQuestion', $this -> request -> post());
		}
	}

} // End Default
