<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_Subjects extends Controller_Admin {

	public function action_index()
	{
		$subjects = ORM::factory('Subject');
		// perform a model function that filters a datset
		$search_field = array('subject_title');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $subjects->getList($search_field, $search_value);
		// Set up pagination params
		$pagination = $this->_setup_pagination($subjects, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
				// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		$subjects->order_by('subject_title');
		foreach ($subjects->find_all() as $key => $model) {
			$content_array[] = array_merge(
					$model->as_array(),
					array(
						'num' => $this->_numbering,
						'active' => ($model->subject_id == $active_record_id),
						
					)
				);
			$this->_numbering++;
		}
		
		$this->_template->set('content_data', $content_array);
		
		//$this->_set_search_context(I18n::get("nav.admin.search.category"));
		$this->_set_content('subjects');
		
	}
	public function action_edit()
	{
		If($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Subject',$post['subject_id']);
			$model->values($post);
			$model->save();
			$this->_set_msg("Updated Subject List", "success",$model->as_array());
		}
		if($this->request->query()){
			$model = ORM::factory('Subject',$this->request->query('subject_id'));
			$this->_set_msg("Updated Subject List", "success",$model->as_array());
		}
	}

} // End Default