<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Admin_Subtopics extends Controller_Admin {

	/**
	 * Returns a list of trial questions
	 *
	 */
	protected static $_template_layout = 'admin-basic';
	public function action_trial_questions() {
		//if($this->request->query()){
		$model = ORM::factory('TrialQuestion') -> select('questions.*', 'subtopics.subtopic_title') -> join('subtopics', 'LEFT') -> on('fk_subtopic_id', '=', 'subtopics.subtopic_id') -> join('questions', 'LEFT') -> on('questions.question_id', '=', 'fk_question_id') -> where('fk_subtopic_id', '=', $this -> request -> query('fk_subtopic_id')) -> find_all();
		//var_dump($model);exit;
		$this -> _template -> set('trial_question_data', $model -> as_array());
		$this -> _template -> set('subtopic', ORM::factory('Subtopic',$this -> request -> query('fk_subtopic_id')));
		//}
		$this -> _set_content('edit-subtopic-test');
	}

	public function action_past_questions() {
		$subtopic_past_questions = ORM::factory('PastQuestionQuestion') -> get_subtopic_past_questions();
		$this -> _template -> set('question_data', $subtopic_past_questions);
		$this -> _template -> set('addContext', 'subtopic');
		$this -> _template -> set('subtopic_id', $this -> request -> query('fk_subtopic_id'));
		$this -> _set_content('past-questions-list-items');
	}

	public function action_subtopic_past_questions() {
		$subtopic = $this -> request -> query('fk_subtopic_id');
		$subtopic_past_questions = ORM::factory('SubtopicPastQuestion') -> get_past_questions($subtopic);
		$this -> _template -> set('question_data', $subtopic_past_questions);
		$this -> _template -> set('listContext', 'subtopic');
		$this -> _template -> set('subtopic_id', $this -> request -> query('fk_subtopic_id'));
		$this -> _set_content('past-questions-list-items');
	}

	public function action_add_past_question() {
		$model = ORM::factory('SubtopicPastQuestion');
		$model -> values($this -> request -> post());
		$model -> save();
		$this -> _set_msg('Added Past Question Successfully', 'success', $model -> as_array());
	}

	public function action_delete_past_question() {
		$model = ORM::factory('SubtopicPastQuestion', $this -> request -> post('past_question_id'));
		$model -> delete();
		$this -> _set_msg('Successfully Deleted Past Question', 'success', TRUE);
	}

	public function action_publish() {
		if($this->request->post()){
			$this->publish('Subtopic', $this->request->post());
		}
	}

	public function action_unpublish() {
		if($this->request->post()){
			$this->unpublish('Subtopic', $this->request->post());
		}
	}
	public function action_save_notes(){
		if($this->request->post()){
			$subtopic = ORM::factory('Subtopic',$this->request->post('subtopic_id'));
			$subtopic->values($this->request->post());
			try{
				$subtopic->save();
				$this -> _set_msg('Updated subtopic notes', 'success');
			}catch(Exception $e){
				$this -> _set_msg('Something went wrong. Try again later!', 'error');
			}
			$this->redirect('admin/subtopics/trial_questions?fk_subtopic_id='.$this->request->post('subtopic_id'));
		}
	}

} // End Default
