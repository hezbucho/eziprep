<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Controller for all admin actions relating to managing tests
 * @author Joseph Bosire 2013-06-06
 * @version 01  *
 * PHP version 5
 */
class Controller_Admin_Tests extends Controller_Admin {

	public function action_index()
	{
		$tests = ORM::factory('Test');	
		$search_field = array('test_title');
		$search_value = $this->_search_context;
		$list_columns = $tests->get_tests($search_field, $search_value);
		//SEARCH OTHER JOIN COLUMNS USING SEARCHFIELD
		$tests->or_where_open();
		$tests->where('lesson.topic_title','LIKE','%'.$search_value.'%');
		$tests->or_where('lesson.fk_country_id','=',Model_Country::get_country_by_name($search_value));
		$tests->or_where('lesson.fk_education_level_id','=',Model_EducationLevel::get_level_by_name($search_value));
		$tests->or_where_close();				
		//PERFORM FILTERING
		if($this->request->post('fk_subject_id')){
			$tests->where('lesson.fk_subject_id','=',$this->request->post('fk_subject_id'));			
			$this->_template->set('active_subject_id', $this->request->post('fk_subject_id'));
		}
		if($this->request->post('fk_country_id')){
			$tests->where('lesson.fk_country_id','=',$this->request->post('fk_country_id'));
			$this->_template->set('active_country_id', $this->request->post('fk_country_id'));
		}
		if($this->request->post('fk_education_level_id')){
			$tests->where('lesson.fk_education_level_id','=',$this->request->post('fk_education_level_id'));
			$this->_template->set('active_education_level_id', $this->request->post('fk_education_level_id'));
		}
		// Set up pagination params
		$pagination = $this->_setup_pagination($tests, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		// Send back the list
		$this->_template->set('content_data', $tests->find_all());		
		//$this->_set_search_context(I18n::get("nav.admin.search.tests"));	
		$countries = ORM::factory('Country')->find_all();
		$educational_levels = ORM::factory('EducationLevel')->find_all();
		$subjects = ORM::factory('Subject')->find_all();
		$this->_template->set('subjects', $subjects);
		$this->_template->set('countries', $countries);
		$this->_template->set('educational_levels', $educational_levels);
		if($this->request->is_ajax())	
			$this->_set_content('tests-list-items');
		else
			$this->_set_content('tests');		
	}
	/**
	 * Called to edit a particular test
	 */
	public function action_edit(){
		if($this->request->post()){
			$post = $this->request->post();
			$model = ORM::factory('Test',$post['test_id']);
			$model->values($post);
			try{
				$model->save();
				$this->_set_msg('The test information was saved','success');
			}catch(ORM_Validation_Exception $e){
				$errors = array();
				if ($e instanceof ORM_Validation_Exception)
					$errors = $e->errors('models');
				$this->_template->set('errors',$errors);
				$this->_set_msg('The test was not saved!', 'error');
			}						
			$this->redirect('admin/tests/edit/'.$model->test_id);			
		}
		if($this->request->param('id')){
			$model = ORM::factory('Test',$this->request->param('id'));
			$this->_template->set('test_data', $model);					
		}
		//Get List of Lessons
		$subjects = ORM::factory('Subject')->find_all();		
		$this->_template->set('subject_data', $subjects);
		$levels = ORM::factory('EducationLevel')->find_all();		
		$this->_template->set('education_level_data', $levels);
		$Lessons = ORM::factory('Lesson')->topics_list();		
		$this->_template->set('lesson_data', $Lessons);
		$countries = ORM::factory('Country')->find_all();
		$this->_template->set('countries', $countries);
		$this->_set_content('edit-test');		
	}
	/*
	 * GET request to get a list of test questions for a particular test
	 * Expects query parameter "test_id" to be passed
	 */
	public function action_get_test_questions(){
		if($this->request->query('test_id')){
			$questions = ORM::factory('TestQuestion')->get_test_questions($this->request->query('test_id'));
			$this->_template->set('question_data', $questions);
			$this->_set_content('test-question-list-items');
		}
		
	}
	public function action_get_test_content(){
		$this->_template_layout = 'admin-basic';
		$this->before();		
		if($this->request->query('test_id')){
			$questions = ORM::factory('TestQuestion')->get_test_questions($this->request->query('test_id'));
			$this->_template->set('question_data', $questions);
			$this->_template->set('test_id', $this->request->query('test_id'));
			$this->_set_content('test-questions-list');
			
		}
		
	}
	/**
	 * Edit a Test Question's Details
	 * if GET request returns details for a particular test question
	 * if POST request saves or updates a test question
	 */
	public function action_edit_test_question(){
		if($this->request->post()){
			$post = $this->request->post();
			//STEP1: Save Question Details 	
			$question = ORM::factory('Question',$post['fk_question_id']);
			$question->values($post);
			$question->save();
			//STEP2: Save answers to question
			$answers = ORM::factory('Answer')->save_answers($post,$question->question_id);
			//STEP3: Save the trial question record
			$model = ORM::factory('TestQuestion',$post['test_question_id']);
			$model->values($post);
			$model->fk_question_id = $question->question_id;
			$model->save();
			//STEP4: Return message response	
			$this->_set_msg("Successfully saved test question", "success",$model->as_array());		
		}
		//Used by shared modal to distinguish type of qestion in modal window
		$this->_template->set('question_type', 'test');
		//Retrieve existing trial question
		if($this->request->query('test_question_id')){
			//Get the test question
			$model = ORM::factory('TestQuestion',$this->request->query('test_question_id'));
			//Get answers for particular question
			$answers = ORM::factory('Answer')->get_question_answers($model->fk_question_id)->as_array();
			$this->_template->set('question_data', $model->as_array());
			$this->_template->set('answers', $answers);
			$this->_set_content('question-form');
			
		}
		//Send back empty form for new trial question
		else{
			//dummy array to determine number of optional answers for a question
			$answers = Model_Answer::$default_answers;			
			$this->_template->set('answers', $answers);
			$this->_set_content('question-form');
		}
	}
	public function action_get_topics(){
		$subject=$this->request->query('subject');
		$level=$this->request->query('level');
		$country=$this->request->query('country');
		
		$topics = ORM::factory('Lesson')->topics($subject,$level,$country);
		$topics_array = array();
		foreach ($topics as $topic) {
			$topics_array[]=array('id'=>$topic->lesson_id,'title'=>$topic->topic_title);
		}		
		$this->_set_msg('Fetched Topics:'.$subject.' level:'.$level,'success',$topics_array);
	}
	public function action_get_levels(){
			$country=$this->request->query('country');			
			$levels = ORM::factory('EducationLevel')->levels($country);
			$levels_array = array();
			foreach ($levels as $level) {
				$levels_array[]=array('id'=>$level->education_level_id,'title'=>$level->level_name);
			}		
		$this->_set_msg('Fetched Levels:'.$country,'success',$levels_array);
	}
	public function action_publish() {
		if ($this -> request -> post()) {
			$this -> publish('Test', $this -> request -> post());
		}
	}

	public function action_unpublish() {
		if ($this -> request -> post()) {
			$this -> unpublish('Test', $this -> request -> post());
		}
	}

} // End Test Controller