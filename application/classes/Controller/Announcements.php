<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Announcements extends Controller_Site
 {
 	protected $permission_actions = array(
		'STUDENT_LOGIN' => 'index'//array('')
	);
	 
	/**
	 * Function to display index page
	 */
	public function action_index() {
		$announcements = ORM::factory('Announcement');
		// perform a model function that filters a datset
		$search_field = array('subject', 'message');
		$search_value = $this->_search_context;;
		$list_columns = $announcements->get_announcements($search_field, $search_value,TRUE);
		// Set up pagination params
		$pagination = $this->_setup_pagination($announcements, $list_columns);
		$this->_template->set('pagination_data', $pagination);		
		// Send back the list
		$content_array = array();
		$content_array = $announcements->find_all();		
		$this->_template->set('content_data', $content_array);				
		$this->_set_search_context(I18n::get("nav.site.search.announcements"));
		$this->_set_content('announcements');
		
	}

	
	public function action_invite(){
		if($this->request->param('id') and ($this->request->param('id')!= Auth::instance()->get_user()->id)){
			$friend = $this->request->param('id');
			$user_id = Auth::instance()->get_user()->id;
			$is_friend = ORM::factory('Friend')
			->or_where_open()
				->where('fk_user_one','=',$user_id)
				->and_where('fk_user_two','=',$friend)
			->or_where_close()
			->or_where_open()
				->where('fk_user_one', '=', $friend)
				->and_where('fk_user_two', '=' ,$user_id)
			->or_where_close()
			->find();
			if($is_friend->loaded()){
				//TODO: Dislay message is already a friend
				if($is_friend->status==1)
					$this->_set_msg("Your are already friends", "success");
				else {
					$this->_set_msg("Friend request is already pending", "success");
				}
				$this->redirect('friends');
			}else{
				$newfriend = new Model_Friend();
				$newfriend->fk_user_one = $user_id;
				$newfriend->fk_user_two = $friend;
				$newfriend->save();
				$this->_set_msg("Your invitation was successfully sent", "success");
				$this->redirect('friends');
			}
		}else{
			$this->redirect('friends');
		}
	}
	/**
	 * This sction changes the status of friendship invite to accepted
	 */
	public function action_accept(){
		$id = $this->request->query('user_id');
		if($id and $this->request->query('user_id')!= Auth::instance()->get_user()->id){
			$load_invite = ORM::factory('Friend')->where('fk_user_one','=',$id)
			->where('fk_user_two','=',Auth::instance()->get_user()->id)
			->find();
			//var_dump($friend->status);exit;
			$friend = ORM::factory('friend',$load_invite->friendship_id);
			$friend_user_info = ORM::factory('User',$id);
			try{
				$friend->status = '1';
				$friend->save();
				$this->_set_msg('Friendship Accepted','success',$friend_user_info->as_array());
			}catch(Exception $e){
				$this->_set_msg('An error occured','error',TRUE);
			}
			
			
		}
	}
	/**
	 * This action rejects a friend invite and deletes the record
	 */		
	public function action_deny(){
		$id = $this->request->query('user_id');		
		$load_invite = ORM::factory('Friend')->where('fk_user_two','=',$id)
		->where('fk_user_one','=',Auth::instance()->get_user()->id)
		->find();
		//var_dump($friend->status);exit;
		$friend = ORM::factory('friend',$load_invite->friendship_id);
		try{
			$friend->delete();
			$this->_set_msg('Friendship Rejected','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}
			
			
		
	}

 }