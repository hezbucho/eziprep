<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller contains all system-wide logic for user controllers
 *
 * @version 02 - Hezron Obuchele 2013-01-03
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Controller_App
 * @author     Hezron Obuchele
 * @package    Core
 * @category   Controllers
 * @copyright  (c) 2012 BeeBuy Investments Ltd. - http://www.beebuy.biz
 */
 
abstract class Controller_App extends Controller
{
    // Pagination-related members
    protected $_items_per_page;

    protected $_page;
	
	/**
	 * Temporary cache for instantly set messages that don't require a redirect hence don't need cookies
	 * @var array
	 */
	protected $_user_message = null;
	
	/**
	 * Template path relative to 'views' folder
	 * @var string
	 */
	protected $_template_path = '../../../templates/';
	
	/**
	 * Template name
	 * @var string
	 */
	protected $_template_name = 'default';
	
	/**
	 * Template layout
	 * @var string
	 */
	protected $_template_layout = 'layout';
	
	/**
	 * Page search context (filter param)
	 * @var string
	 */
	protected $_search_context = null;
	
	/**
	 * Holds request params for use in child controllers
	 * @var array
	 */
	protected $_request_params = array();
	/**
	 * Template object
	 * @var BeatifulView
	 */
	protected $_template = null;
	
	/**
	 * Holds any component-specific varibales to be passed to the template
	 * @var array
	 */
	protected $_template_component_vars = array();
	
	/**
     * Controls access for the whole controller, if not set to FALSE we will only allow user roles specified
     *
     * Can be set to a string or an array, for example array('login', 'admin') or 'login'
     */
    protected $auth_required = false;
    /** Controls access for separate actions
     *
     *  Examples:
     * 'adminpanel' => 'admin' will only allow users with the role admin to access action_adminpanel
     * 'moderatorpanel' => array('login', 'moderator') will only allow users with the roles login and moderator to access action_moderatorpanel
     */
    protected $secure_actions = false;
	
	
	/** Controls access for permission based actions
     *
     *  Examples:
     * 'edit_users' => array('save_user', 'delete_user') will only allow users with the roles with permission edit_users to access action_save_user & action_delete_user
     */
    protected $permission_actions = false;
	
	/**
     * Function to log application errors to logs/component_name directory
	 * 
	 *  @param	string	Message to log
	 *  @param	string	Kohana Log Level
	 *  @return	void
     */
    protected function _log($message, $level)
    {
        Log::instance()->add($level, $message)->write();return;
        // Set log level
        /* Log levels
			LOG_EMERG   => 'EMERGENCY',
			LOG_CRIT    => 'CRITICAL',
			LOG_ERR     => 'ERROR',
			LOG_WARNING => 'WARNING',
			LOG_NOTICE  => 'NOTICE',
			LOG_INFO    => 'INFO',
			LOG_DEBUG   => 'DEBUG'
        */
        // Define defaults
        $caller_function = '';
        $caller_class =  '';
        $caller_type = '';
        $debug_info = 'Null';
		$folder = isset($this->_component_name) ? $this->_component_name : 'core';
        // Attach debug info. to Errors and above only
        if ($level <= LOG_ERR) {
            // Add application version
            include_once(DOCROOT.'version.php');
            if (class_exists('App_Version')) {
                $debug_info = 'Ver. ' . App_Version::APP_VERSION . ', ';
            }
            // Add calling function and class
            $trace = debug_backtrace(false);
            $caller_function = $trace[1]['function'];
            $caller_type = $trace[1]['type'];
            if (isset($trace[1]['class']))
                $caller_class = $trace[1]['class'];

            $debug_info .= $caller_class . $caller_type . $caller_function . '()';
        }
		
		// Generate log entry
		 //TODO: Save message log in json format to allow for flexibility
        if (!is_array($message)) {
            $messages[] = array
                    (
                    'time'  => date(Date::$timestamp_format),// Date::formatted_time(),
                    'level' => $level,
                    'body'  => $message . ' {DebugInfo: ' . $debug_info . '}'
            );
        } else {
            $time = date(Date::$timestamp_format);//Date::formatted_time();
            foreach ($message as $msg) {
                $messages[] = array
                        (
                        'time'  => $time,
                        'level' => $level,
                        'body'  => $msg . ' {DebugInfo: ' . $debug_info . '}',
                );
            }
        }

        $file  = new Log_File(APPPATH.'logs/'.$folder);
        $file->write($messages);
    }

	/**
     * Function to set up controller & model pagination using an ORM model
     *
	 * @return	void
     */
    protected function _setup_pagination(ORM &$model, array $list_columns = array())
    {
		$table_columns = !empty($list_columns) ? $list_columns : $model->list_columns();
		$count_model = clone $model;
		$count = $count_model->count_all();
		$pagination = Pagination::factory(array(
			'total_items' => $count,
			'items_per_page' => Arr::get($this->_request_params, 'limit')
		));
		$order_by = Arr::get($this->_request_params, 'order_by');
		if ($order_by && array_key_exists($order_by, $table_columns))
			$model->order_by($order_by, Arr::get($this->_request_params, 'sort'));
		$model->limit(Arr::get($this->_request_params, 'limit'));
		$model->offset($pagination->offset);
		
		// Generate list numbering
		$this->_numbering = ($pagination->items_per_page * ($this->_request_params['page'] - 1)) + 1;
		//TODO: create helper that increments this value between calls
        $this->_template->set('pagination_numbering', $this->_numbering);
		
		return $pagination;
    }
	
	/**
	 * Function to load the view for the content area
	 * 
	 *  @param	string	content file name
	 *  @return	void
	 */
	protected function _set_content($view_name) {
		$content_partial_dir = Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.content_blocks_folder");
		$content_partial_dir = ($content_partial_dir) ? $content_partial_dir . '/' : '';
		$this->_template_content = $content_partial_dir . $view_name;//$this->_template_path . $content_partial_dir . $view_name;
		$this->_template->template()->partial('content', $this->_template_content);
	}
	
	/**
	 * Function to generate a breadcrumb for the current url
	 * 
	 *  @param	array	extra entries to add to the breadcrumb path
	 *  @param	string	text to be displayed for the home page segment
	 *  @return	void
	 */
	protected function _breadcrumbs($root_title = 'Home', $pages = array())
    {
        $pages_passed = $pages;
		$uri_segments = array();
		foreach($segments = explode('/', Request::detect_uri()) as $key => $segment) {
			$url = URL::site(join('/', array_slice($segments, 0, ($key + 1))), true);
		    // make sure urls ending with / are treated same as those without the last / as well as those ending with /index
		    if (strlen($segment) && !in_array($url, $uri_segments) && substr($url,-5) != 'index') {
		        $pages[] = array(
                        'title' => ucfirst($segment),
                        'url'   => $url
                );
		        $uri_segments[] = $url;
		    }
		}
        // Set first item text to home
        $pages[count($pages_passed)]['title'] = $root_title;
		// Set last item url to none
		$pages[count($pages) - 1]['url'] = ''; 
		
		if (intval($pages[count($pages) - 1]['title']) > 0){
			// remove number
			$pages[count($pages) - 1]['title'] = 'List';// TODO: Find a way to pass the page context from controller to here
		}

        $this->_template->template()->partial('breadcrumb', 'blocks/breadcrumb');
		$this->_template->set('breadcrumb_pages', $pages);
    }
	
	/**
	 * Function to set the search context of the current page
	 */
	protected function _set_search_context($context_title){
		$this->_template->set('search_context', $context_title);
	}
	
	/**
	 * Function to update the last item of the breadcrumb to match a specified context
	 */
	protected function _set_breadcrumbs_context($context_title, $merge_second_last = false, $overwrite_second_last = false){
		$pages = $this->_template->breadcrumb_pages;
		if ($merge_second_last){
			$context_title = ($overwrite_second_last) ? $context_title :$pages[count($pages) - 2]['title'] . " $context_title";
			$pages[count($pages) - 2]['url'] = $pages[count($pages) - 1]['url'];
			unset($pages[count($pages) - 1]);
		}
		$pages[count($pages) - 1]['title'] = $context_title;
		$this->_template->set('breadcrumb_pages', $pages);
	}
	
	/**
	 * Function to delete the text for message cookie to avoid repeated messages between pages
	 */
	protected function _clean_up_msg() {
		$types = array('error', 'info', 'success', 'attention');
		foreach ($types as $type) {
			Cookie::delete('message' . $type);
		}
	}
	
	/**
	 * Function to get the text for the message/notifications area
	 */
	protected function _get_msg() {
		$return = $this->_user_message;
		if (!$return){
			$types = array('error', 'info', 'success', 'attention');
			
			foreach ($types as $type) {
				//echo Cookie::get('message' . $type) . ' message <br />';
				if (Cookie::get('message' . $type, false)) {
					//$return[$type] = array(__(Cookie::get('message' . $type)));
					$return = array('type' => $type, 'message_body' => I18n::get(Cookie::get('message' . $type)));
				}
			}
		}
		$this->_user_message = null;
		return $return;
	}

	/**
	 * Function to set the text for message/notifications area
	 * 
	 * @param $msg string the message to display to the user
	 * @param $type string message type (can be used as a css class to style the message box color) 
	 * @param $temp array optional data you want to pass for json repsonses | boolean indicating whether this message will be used in the same request without redirect  
	 * 
	 * @return void
	 */
	protected function _set_msg($msg, $type = 'info', $temp = false) {
		if (!$this->_is_json_request){
			$this->_user_message = null;
			if ($temp)
				$this->_user_message = array('type' => $type, 'message_body' => $msg);
			else
				Cookie::set('message' . $type, $msg);
			if ($type == 'error' && is_array($temp)){ // automatically set errors variable
				$this->_template->set('errors', $temp);
			}
		}else{//TODO: send the response immediately and return without further execution of the app
			$response = array(
				'msg' => array(
					'type' => $type,
					'message_body' => $msg
				)
			);
			if (is_array($temp) && $type != 'error')
				$response['data'] = $temp;
			elseif (is_array($temp) && $type == 'error')
				$response['error'] = $temp;
			$this->_template->set('content_data', $response);
		}
	}

	/**
	 * Function to setup common template variables such as page metadata and base urls for links
	 * 
	 * @return  void
	 */
	protected function _setup_page_metadata()
	{
		$this->_template->set('template_name', $this->_template_name);
		$this->_template->set('template_path', URL::base(). 'templates/' . $this->_template_name . '/' . $this->_template_folder . '/');
		$this->_template->set('charset', Kohana::$charset);
		$this->_template->set('base_url', URL::site(NULL, TRUE));
		$this->_template->set('meta_keywords', '');
		$this->_template->set('meta_description', '');
		$this->_template->set('meta_copyright', '');
		$this->_template->set('meta_language', I18n::lang());
		// Change site title based on config position
		$seo_config = Kohana::$config->load('settings.seo');
		$site_name = Kohana::$config->load('settings.site_name');
		$title_prefix = $seo_config['title']['site-name-pos'] == 'beginning' ?  $site_name . Kohana::$config->load('settings.seo.title.prefix-separator') : Kohana::$config->load('settings.seo.title.prefix-separator');
		$title_suffix = $seo_config['title']['site-name-pos'] == 'end' ?  Kohana::$config->load('settings.seo.title.suffix-separator') . $site_name : Kohana::$config->load('settings.seo.title.prefix-separator');
		$this->_template->set('site_name', $site_name);
		$this->_template->set('title_prefix', $title_prefix);
		$this->_template->set('title_suffix', $title_suffix);
		$this->_template->set('site_tagline', Kohana::$config->load('settings.site_tagline'));
		// Set a base url for component level urls i.e. controllers you want to reference within the same component as the current controller
		$current_route_params = array('controller' => false, 'action' => false, 'directory' => strtolower($this->request->directory()));
		$component_base_url = rtrim(Route::url(Route::name(Request::$current->route()), $current_route_params), '/') .  '/';
		$this->_template->set('component_base_url', $component_base_url);
		// Add a base url for system level urls i.e actions set in App controller hence available in current controller
		$current_route_params['controller'] = strtolower($this->request->controller());
		$system_functions_url = rtrim(Route::url(Route::name(Request::$current->route()), $current_route_params), '/') .  '/';
		$this->_template->set('controller_base_url', $system_functions_url);
		$this->_template->set('system_functions_url', $system_functions_url);
		$this->_template->set('current_url', $this->request->url());
	}
	
	/**
	 * Function to initialize the template
	 * 
	 * @return  void
	 */
	protected function _initialize_template()
	{
		// Get template name and load template based on config
		$this->_template_name = Kohana::$config->load('settings.template') ? Kohana::$config->load('settings.template') : $this->_template_name;
		
		// Update template path to point to location of template files
		$share_template = Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.share_template");
		$this->_template_folder = $share_template ? $share_template : $this->_component_name;
		//$this->_template_path = $this->_template_path . $this->_template_name . '/' . $this->_template_folder . '/tpl/';
		$this->_template_path = DOCROOT . '/templates/' . $this->_template_name . '/';// . $this->_template_folder . '/tpl/';
		
		// Update session with template info for use elsewhere e.g. config files
		$this->session->set('sys_template_component', $this->_template_folder);//$this->_component_name);
		$this->session->set('sys_template_name', $this->_template_name);
		
		// Get theme for template if multiple themes are supported
		$theme = Kohana::$config->load("theme-config.component_configs.{$this->_template_folder}.theme"); 
		$themes = Kohana::$config->load("theme-config.component_configs.{$this->_template_folder}.themes");
		if ($theme && is_string($theme) && is_array($themes) && array_key_exists('default', $themes)){ // default theme is compulsory
			// pull theme from url
			if ($this->request->query('theme'))
				$this->session->set('theme', $this->request->query('theme'));
			$this->_template_theme_name = $this->session->get('theme', $theme);
			if (!array_key_exists($this->_template_theme_name, $themes))
				$this->_template_theme_name = $theme;
			
			$this->_template_theme_path = $themes[$this->_template_theme_name];
		}
		
		// Create template
		$view_class = $this->_template_layout;//(Auth::instance()->get_user() ? 'layout' : 'login_layout');
		$view_model_class = 'View_' . ucfirst($this->_component_name) . 'Layout';
		Template_Handlebars::$dir = $this->_template_folder . DIRECTORY_SEPARATOR . 'tpl';
		$handlebars_template = new Template_Handlebars($view_class);
		//$handlebars_template::$dir = $this->_template_folder . DIRECTORY_SEPARATOR . 'tpl';
		$this->_template = new View($handlebars_template, new $view_model_class);
		$this->_template->set('template_path', URL::site() . 'templates/' . $this->_template_name  . '/' . $this->_template_folder . '/');
		if ($this->_template_theme_name)// Kohana::$config->load("theme-config.component_configs.{$this->_template_folder}.theme"))
			$this->_template->set('theme_name', $this->_template_theme_name);
		
		$this->_template->set('logged_in', !!(Auth::instance()->get_user()));
		$this->_template->set('username', (Auth::instance()->logged_in() ? Auth::instance()->get_user()->username : ''));
		$this->_template->set('user_id', (Auth::instance()->logged_in() ? Auth::instance()->get_user()->id : ''));
		$this->_template->set('user_info', Auth::instance()->get_user());
		
	}

	/**
	 * Function to setup page resources i.e. css and js assets
	 * 
	 * @return  void
	 */
	protected function _setup_page_assets()
	{
		//var_dump($this->_template->logged_in);exit;
		// Set relative path to the theme css
		if (isset($this->_template_theme_path) && !Kohana::$config->load("theme-config.component_configs.{$this->_template_folder}.theme_in_main_css")){
			$theme_css = '';
			if (is_array($this->_template_theme_path) && count($this->_template_theme_path)){
				foreach ($this->_template_theme_path as $path) {
					$theme_css .= HTML::style('templates/' . $this->_template_name . '/' . $this->_template_folder . '/' . $path);
				}
			} else if (is_string($this->_template_theme_path) && $this->_template_theme_path){
				$theme_css = HTML::style('templates/' . $this->_template_name . '/' . $this->_template_folder . '/' . $this->_template_theme_path);
			}			
			$this->_template->set('theme_css', $theme_css);
		}
		//$this->_template->set('theme_css', HTML::style('templates/' . $this->_template_name . '/' . $this->_component_name . '/' . $this->_template_theme_path));
		
		//Set global component resources
		$css_asset_path = 'theme-config.assets.' . $this->_component_name . '.' . $this->_component_name;
		$css_asset_key = $css_asset_path . '.groups.globalcss';
		if(Kohana::$config->load($css_asset_key)){
			$css_group = Kohana::$config->load($css_asset_path);
			$temp_template_path = 'templates/'  . $this->_template_name . '/';
			$temp_template_path .= $this->_template_folder ? $this->_template_folder : $this->_component_name;
			$temp_template_path .= '/';
			if (isset($this->_template_theme_path) && Kohana::$config->load("theme-config.component_configs.{$this->_template_folder}.theme_in_main_css")){
				// prepend theme css to global css array
				array_unshift($css_group['groups']['globalcss'],
								array('CSS', $temp_template_path . $this->_template_theme_path, array(
										'media' => 'screen',
									)
								)
							);
			}
			$global_css = new Asset_Cache(new Asset_Group('globalcss', $css_group));
			$this->_template->set('globalstyles', $global_css);
		}
		$js_asset_path = 'theme-config.assets.' . $this->_component_name . '.' . $this->_component_name;
		$js_asset_key = $js_asset_path . '.groups.globaljs';
		if(Kohana::$config->load($js_asset_key)){
			$global_js = new Asset_Cache(new Asset_Group('globaljs', Kohana::$config->load($js_asset_path)));
			$this->_template->set('globalscripts', $global_js);
		}
		
		//Set page-specific resources
		$css_asset_path = 'theme-config.assets.' . $this->_component_name . '.' . strtolower($this->request->controller());
		$css_asset_key = $css_asset_path . '.groups.' . strtolower($this->request->action()) . 'css';
		if(Kohana::$config->load($css_asset_key)){
			$page_css = new Asset_Cache(new Asset_Group(strtolower($this->request->action()) . 'css', Kohana::$config->load($css_asset_path)));
			$this->_template->set('pagestyles', $page_css);
		}
		$js_asset_path = 'theme-config.assets.' . $this->_component_name . '.' . strtolower($this->request->controller());
		$js_asset_key = $js_asset_path . '.groups.' . strtolower($this->request->action()) . 'js';
		if(Kohana::$config->load($js_asset_key)){
			$page_js = new Asset_Cache(new Asset_Group(strtolower($this->request->action()) . 'js', Kohana::$config->load($js_asset_path)));
			$this->_template->set('pagescripts', $page_js);
		}
	}
	
	/**
	 * Function to setup helpers based on template engine
	 * 
	 * @return  void
	 */
	protected function _setup_template_helpers()
	{
		// Setup handlebars helpers 
		if ($this->_template->template() instanceof Template_Handlebars){
			$template_path = '../../../../' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . $this->_template_name. DIRECTORY_SEPARATOR . $this->_component_name . DIRECTORY_SEPARATOR . 'tpl'. DIRECTORY_SEPARATOR;
			//$this->_template_folder . DIRECTORY_SEPARATOR . 'tpl';
			Template_Handlebars::$helpers = array(
				'i18n' => function ($template, $context, $args, $source) {
					$tmp = $context->get($args);
					if (!$tmp && strpos($args, '"') === 0 && strpos($args, '"', 1) === strlen($args) - 1) //get raw string
						$tmp = trim($args, '"');
					return I18n::get($tmp);
				},
				'php_json' => function ($template, $context, $args, $source) {
					$tmp = $context->get($args);
										 
					$school =$tmp;//'[{school_id:"1",school_name:"Nairobi"},{school_id:"2",school_name:"Lenana"}]';
					
					return $school;
				},
				'percentage' => function ($template, $context, $args, $source) {
					$buffer = '';
					$args = explode(' ', $args);
					$args[0] = $context->get($args[0]);
					$args[1] = $context->get($args[1]);
					if (count($args) == 2 && $args[1])
						$buffer = ($args[0]/$args[1])*100;
					return $buffer;
				},
				'wrap_handlebars_var' => function ($template, $context, $args, $source) {
					return '{{'.$args.'}}';
				},
				'timestamp_to_date' => function ($template, $context, $args, $source) use($template_path) { // usage: {{#timestamp_to_date var format}} {{#timestamp_to_date created "d M"}}
					$args = explode(' ', $args);
					$date = $context->get($args[0]);
					unset($args[0]);
					if (is_numeric($date)){
						$format = 'Y-m-d';
						if (count($args) == 2){
							$format = trim(implode(' ', $args), '"');
						}
						return date($format, $date);
					}
				},
				'menu' => function ($template, $context, $args, $source) use($template_path) { // usage: {{#menu name current}} {{#menu main dataset/details}}
					$args = explode(' ', $args);
					//Menu::$views_dir = $template_path;
					$menu = Menu::factory($context->get($args[0]), $template_path . 'menu');
					if (count($args) > 1)
						$menu->set_current($context->get($args[1]));
					return (string) $menu;
				},
				'render_script' => function ($template, $context, $args, $source) { // usage: {{#render_script var_containing_link_to_script_with_handlebars_vars}}
					$asset_paths = $context->get($args);
					$asset_paths = str_ireplace('<script type="text/javascript" src="' . URL::site(), '', $asset_paths);
					$asset_paths = str_ireplace('"></script>', '\n', $asset_paths);
					$asset_paths = explode('\n', $asset_paths);
					// there's always a blank string returned as the last element, remove it TODO: fix it
					unset($asset_paths[count($asset_paths) - 1]);
					$buffer = "";
					//var_dump($asset_paths);
					if (count($asset_paths)){
						foreach ($asset_paths as $path) {
							$asset_path = DOCROOT . trim(implode(DIRECTORY_SEPARATOR, explode('/', $path)));
							if (file_exists($asset_path) && !is_dir($asset_path)){
								$asset_content = file_get_contents($asset_path);
								$blacklist = array();
								$force_lower = false;
								// generate inline script tag
								$buffer .= '<script type="text/javascript">';
								$buffer .= preg_replace_callback('/\\{\\{([^{}]+)\}\\}/',
						            function($matches) use ($context, $force_lower, $blacklist)
						            {
						                $key = $force_lower ? strtolower($matches[1]) : $matches[1];
										$val = $context->get($key);
						                return ($val && !in_array($key, $blacklist))
						                    ? $val
						                    : '';
						            }
						            , $asset_content);
								$buffer .= '</script>';
							}
						}
					}
					return $buffer;
				},
				'greater_than' => function ($template, $context, $args, $source) {
					$args = explode(' ', $args);
					$buffer = '';
					$param1 = $context->get($args[0]);
					if (!$param1 && is_numeric($args[0]))
						$param1 = intval($args[0]);
					$param2 = $context->get($args[1]);
					if (!$param2 && is_numeric($args[1]))
						$param2 = intval($args[1]);
					if ($param1 && $param2 && count($args) == 2 ){
						if ($param1 > $param2) {
		                    $template->setStopToken('else');
		                    $buffer = $template->render($context);
		                    $template->setStopToken(false);
		                    $template->discard($context);
		                } else {
		                    $template->setStopToken('else');
		                    $template->discard($context);
		                    $template->setStopToken(false);
		                    $buffer = $template->render($context);
		                }
					}
					return $buffer;
				},
				'equals_to' => function ($template, $context, $args, $source) {
					$args = explode(' ', $args);
					$buffer = '';
					$param1 = $context->get($args[0]);
					if (!$param1 && is_numeric($args[0]))
						$param1 = intval($args[0]);
					$param2 = $context->get($args[1]);
					if (!$param2 && is_numeric($args[1]))
						$param2 = intval($args[1]);
					if ($param1 && $param2 && count($args) == 2 ){
						if ($param1 == $param2) {
		                    $template->setStopToken('else');
		                    $buffer = $template->render($context);
		                    $template->setStopToken(false);
		                    $template->discard($context);
		                } else {
		                    $template->setStopToken('else');
		                    $template->discard($context);
		                    $template->setStopToken(false);
		                    $buffer = $template->render($context);
		                }
					}
					return $buffer;
				},
				'get_first_error' => function ($template, $context, $args, $source) { //usage {{get_first_error errors_array field_name}}
					$args = explode(' ', $args);
					$buffer = '';
					$param1 = $context->get($args[0]); // errors array has to be a var in the context
					// "field" can be either a string or a variable
					if (count($args) == 2 && substr($args[1], 0, 1 ) == '"' && substr($args[1], -1, 1 ) == '"')
						$param2 = substr($args[1], 1, strlen($args[1]) - 2);
					elseif (count($args) == 2 && $context->get($args[1]))
						$param2 = $context->get($args[1]);
					
					if (count($args) == 2 && $param1 && $param2 && is_array($param1) && array_key_exists($param2, $param1)){
						if (is_array($param1[$param2]))
							$buffer = array_shift($param1[$param2]);
						else
							 $buffer = $param1[$param2];
					}
					return $buffer;
				},
				'key_in_array' =>  function ($template, $context, $args, $source) {
					$args = explode(' ', $args);
					$buffer = '';
					$param1 = $context->get($args[0]); // errors array has to be a var in the context
					// "field" can be either a string or a variable
					if (count($args) == 2 && substr($args[1], 0, 1 ) == '"' && substr($args[1], -1, 1 ) == '"')
						$param2 = substr($args[1], 1, strlen($args[1]) - 2);
					elseif (count($args) == 2 && $context->get($args[1]))
						$param2 = $context->get($args[1]);
					
					if (count($args) == 2 && $param1 && $param2 && is_array($param1)){
						if (array_key_exists($param2, $param1)) {
			                    $template->setStopToken('else');
			                    $buffer = $template->render($context);
			                    $template->setStopToken(false);
			                    $template->discard($context);
		                } else {
		                    $template->setStopToken('else');
		                    $template->discard($context);
		                    $template->setStopToken(false);
		                    $buffer = $template->render($context);
		                }
					}
					return $buffer;
				},
				'iter' => function ($template, $context, $args, $source) {
					$args = explode(' ', $args);
	                $tmp = $context->get($args[0]);
	                $buffer = '';
	                if (is_array($tmp) || $tmp instanceof Traversable) {
						$ch = count($args) > 1 ? $args[1] : '1';
						$next_ch = $ch;
	                    foreach ($tmp as $var) {
	                    	if (is_array($var))
								$var['@index'] = $next_ch;
							else if(is_object($var))
								$var->{'@index'} = $next_ch;
	                        $context->push($var);
	                        $buffer .= $template->render($context);
	                        $context->pop();
							
							$next_ch = ++$ch;
							if (strlen($next_ch) > 1) { // if you go beyond z or Z reset to a or A
								$next_ch = $next_ch[0];
							}
	                    }
	                }            
	                return $buffer;
	            },
				'increment' => function ($template, $context, $args, $source) { // usage: {{#menu name current}} {{#menu main dataset/details}}
					$tmp = $context->get($args);
					$tmp++;
					//var_dump($this->_template);exit;
					$context->set($args, $tmp); 
					return $tmp;
				},
			);
		}
		
	}

	/**
	 * Function to setup request variables
	 * 
	 * @return  void
	 */
	protected function _setup_request_params()
	{
		$this->_request_params['search'] = $this->request->query('search');
		$this->_request_params['search_field'] = $this->request->query('field');
		$this->_request_params['order_by'] = $this->request->query('orderby');
		$this->_request_params['limit'] = $this->request->query('limit');
		$this->_request_params['offset'] = $this->request->query('offset');
		$this->_request_params['sort'] = $this->request->query('sort');
		$this->_request_params['page'] = intval($this->request->query('page'));
		// Set defaults for necessary params if not passed
		if (!$this->_request_params['limit'])
			$this->_request_params['limit'] = Kohana::$config->load('pagination.default.items_per_page');
		if (!$this->_request_params['offset'])
			$this->_request_params['offset'] = 0;
		if (strtolower($this->_request_params['sort']) == 'desc')
			$this->_request_params['sort'] = 'desc';
		else // Value was passed but wasn't desc/wasn't passed at all
			$this->_request_params['sort'] = 'asc';
		if ($this->_request_params['page'] == 0)
			$this->_request_params['page'] = 1;
		
		// Add other request params
		$request_data = array();
		if ($this->request->method() == HTTP_Request::GET)
			$request_data = $this->request->query();
		// Add PUT/POST type params
		if ($this->request->method() == HTTP_Request::POST || ($this->request->method() == HTTP_Request::PUT && $this->request->param('id')) )
			parse_str($this->request->body(), $request_data);
		
		$this->_request_params = array_merge($request_data, $this->_request_params);
	}
	
	/**
	 Function to get the permissions required to access the current action
	 */
	private function _user_authorized_to_access_action(){
		$action_name = Request::current()->action();
		if (is_array($this->permission_actions)){
			foreach ($this->permission_actions as $permission => $actions) {
				if ( (is_array($actions) && in_array($action_name, $actions)) || (is_string($actions) && $actions == $action_name)){
					//${'permission_' . $$permission} = ORM::factory('Permission')->where('name', '=', $permission)->find();
					$permission_model = ORM::factory('Permission')->where('name', '=', $permission)->find();
					//var_dump($permission_model);exit;
					if ($permission_model->loaded() && Auth::instance()->get_user()){
						if (Auth::instance()->get_user()->can($permission_model))
							return true;
					}
					else // permission doesn't exist
						$this->_log('Required permission does not exist: ' . $permission, Log::DEBUG);
				}
			}
		}
		return false;
	}
	
	private function _action_requires_auth_permission(){
		$action_name = Request::current()->action();
		if (is_array($this->permission_actions)){
			foreach ($this->permission_actions as $permission => $actions) {
				if ( (is_array($actions) && in_array($action_name, $actions)) || (is_string($actions) && $actions == $action_name)){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Function to setup controller ACL functionality and check if current user has been authorized to access a certain resource
	 */
	protected function _setup_authorization(){// Check user auth and role
		$action_name = Request::current()->action();
		// cache user object
        $this->_current_user = Auth::instance()->get_user();

        // if remember me was selected by user during a previous login, autologin the user
        Auth::instance()->auto_login();
        /*$username = Cookie::get('user_username', null);
        $user_password = Cookie::get('user_password', null);
        if ($username !== null && $user_password !== null){
            Auth::instance()->login($username, $user_password, true);
            $this->_log('attempted to perform an auto-login', LOG_INFO);
        }*/
		
		//var_dump($this->_user_authorized_to_access_action());exit;
        if (($this->auth_required !== FALSE && Auth::instance()->logged_in($this->auth_required) === FALSE)
                // auth is required AND user role given in auth_required is NOT logged in
                || (is_array($this->secure_actions) && array_key_exists($action_name, $this->secure_actions) && Auth::instance()->logged_in($this->secure_actions[$action_name]) === FALSE)
        		// OR secure_actions is set AND the user role given in secure_actions is NOT logged in
        		|| ($this->_action_requires_auth_permission() && !$this->_user_authorized_to_access_action())
				// OR permission_actions is set AND the user role given in secure_actions is NOT logged in 
        ) {
            if (Auth::instance()->logged_in()) {
                // user is logged in but not on the secure_actions list
                //echo 'logged in with no permission'; exit;
                $this->access_required();
            } else {
            	//echo 'not logged in'; exit;
                $this->login_required();
            }
        }
		//echo 'end of auth routine'; exit;
	}
	
	/**
	 * Maps the keys of $source to a new array using the supplied $map
	 * 
	 * @param
	 * @param
	 * @return
	 */
	protected function map_array_keys(array $source, array $map) {
		$mapping = array();
		foreach ($values as $key => $value) {
			// mapping optional
			if (array_key_exists($key, $map)) // use explicit mapping key if specified
				$mapping[$map[$key]] = $value;
			else // use default key
				$mapping[$key] = $value;
		}
		return $mapping;
	}
	
	/**
	 * Reverse maps the keys of $source to a new array using the supplied $map
	 * 
	 * @param
	 * @param
	 * @return
	 */
	protected function reverse_map_array_keys(array $source, array $map) {
		$mapping = array();
		$map = array_flip($map);
		foreach ($values as $key => $value) {
			// mapping optional
			if (array_key_exists($key, $map)) // use explicit mapping key if specified
				$mapping[$map[$key]] = $value;
			else // use default key
				$mapping[$key] = $value;
		}
		return $mapping;
	}

	/**
	 * Initialize controller
	 *
	 * @param   Request request object
	 * @param   Response response object
	 * @return  void
	 */
	public function __construct(Request $request, Response $response)
	{
		// Assign the request to the controller
		$this->request = $request;
 
		// Assign a response to the controller
		$this->response = $response;
	
		// Create a session object for the controller
		$this->session = Session::instance();
	}
	
	/**
	 * Automatically executed before the controller action. Can be used to set
	 * class properties, do authorization checks, and execute other custom code.
	 * 
	 * @return  void
	 */
	public function before()
	{
		// This codeblock is very useful in development sites:
        // What it does is get rid of invalid sessions which cause exceptions, which may happen
        // 1) when you make errors in your code.
        // 2) when the session expires!
        /*try {
            $this->session = Session::instance();
        } catch (ErrorException $e) {
            session_destroy();
        }*/
        
        $this->_is_json_request = (Arr::get($this->request->accept_type(), 'application/json') !== null);
        
        // perform authorization checks
		$this->_setup_authorization();
		
		// Initialize template
		$this->_initialize_template();
		
		// Register helpers
		$this->_setup_template_helpers();
		
		/**
		 * Begin setting up template variables
		 */
		// Set common template vars
		$this->_setup_page_metadata();
		
		//setup page resources
		$this->_setup_page_assets();
		
		// Set component-specific template vars setup in child controllers
		foreach ($this->_template_component_vars as $key => $value) {
			$this->_template->set($key, $value);
		}
		
		// setup request variables
		$this->_setup_request_params();
		
		// setup search variable
		$this->session->delete($this->request->controller() . '_search_context');
		if ($this->request->post('search_context')){
			$this->session->set($this->request->controller() . '_search_context', $this->request->post('search_context'));			  
		}elseif($this->request->query('clear_search_context')){
			$this->session->delete($this->request->controller() . '_search_context');
		}
	
		// pass session var to controller action
		$this->_search_context = $this->session->get($this->request->controller() . '_search_context');
		$this->_template->set('current_search_context', $this->_search_context);
		
		// setup pagination session variable
        if ($this->request->post('paginate')) {
            $this->session->set('pagination_items_per_page', $this->request->post('paginate'));
        }

        $this->_items_per_page = $this->session->get('pagination_items_per_page', $this->_request_params['limit']);
		$this->_request_params['limit'] = $this->_items_per_page;
		
	}

	/**
     * URL to change the site display language
     *
	 * @param   string $id language code
	 * @return  void
     */
    public function action_change_language()
    {
        $lang = $this->request->param('id');
        if(!array_key_exists('i18n' . DIRECTORY_SEPARATOR . $lang . '.php', Kohana::list_files('i18n', array(APPPATH)))) {
            $lang = 'en-us';
        }

		Cookie::set('lang', $lang);
        I18n::lang($lang);
		$this->_set_msg(I18n::get("system.notifications.change_lang_success"), 'success');
		//$this->_langchanged = true;
        $this->redirect($this->request->referrer());
    }
	
	/**
     * URL to change the site theme
     *
	 * @param   string theme name
	 * @return  void
     */
    public function action_change_theme()
    {
        $theme = $this->request->param('id');
		$this->session->set('theme', $theme);
		$this->_set_msg(I18n::get("system.notifications.change_theme_success"), 'success');
        $this->redirect($this->request->referrer());
    }
	
	/**
	 * Automatically executed after the controller action. Can be used to apply
	 * transformation to the request response, add extra output, and execute
	 * other custom code.
	 * 
	 * @return  void
	 */
	public function after()
	{
		//$this->_set_msg("Hi", "info", true);
		$this->_template->set('user_msg', $this->_get_msg());//array('type' => 'info', 'message_body' => 'Hi'));//
		if ($this->_is_json_request && isset($this->_template->content_data)){
			$this->response->body(json_encode($this->_template->content_data));
		} else {
			if ($this->request->is_ajax()){ // non-json ajax request from jquery-mobile-like apps that returns only the content body
				$this->_template = new View(new Template_Handlebars($this->_template_content), $this->_template->viewmodel());
				$this->_template->set('ajax_load', true);
			} else {
				// normal/traditional browser requests
	        }
			//var_dump($this->_template);exit;
			// Attach template to response
			$this->response->body($this->_template);
		}
		// Clean up messages & cookies
	    $this->_clean_up_msg();
	}
	
	
	/**
     * TODO: Adapt these functions to work with the system
     *
     */
     
    /**
     * Called from before() when the user does not have the correct rights to access a controller/action.
     *
     * Override this in your own Controller / Controller_App if you need to handle
     * responses differently.
     *
     * For example:
     * - handle JSON requests by returning a HTTP error code and a JSON object
     * - redirect to a different failure page from one part of the application
     */
    public function access_required() {
        //$this->request->redirect('user/noaccess');
        throw new HTTP_Exception_403("You do not have permission to view this page.");
    }
	
    /**
     * Called from before() when the user is not logged in but they should. (Useradmin module function)
     *
     */
    public function login_required()
    {
        if (isset($_SERVER['PATH_INFO'])) {
            $url = URL::base(true) . substr($_SERVER['PATH_INFO'],1);
        } else {
            $url = URL::base(true);
        }
        Cookie::set('user_redirect', $url);
        //Request::current()->redirect('user/login');
        throw new HTTP_Exception_401("You do not have permissions to view this page");
    }

    /**
     * Function to manually highlight the current menu item
     */
    protected function _set_current_page($uri)
    {
        $this->_template->set('currentmenuitem', $uri);
    }
	/**
	 * Uploads images to server. It crops the image based on the width and height and also resizes the image
	 * while maintaining the aspect ratio
	 * @param $image - the $FILE['image'] posted
	 * @param $width of the image to resize to
	 * @param $height of the image to resize to
	 * @param $directoyr the directory to upload the image
	 */
    protected function save_image($image,$width=200,$height=200,$directory=false)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        } 
        //$directory = DOCROOT.'uploads/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
 
            Image::factory($file)
                ->resize($width, $height,  Image::INVERSE)
				->crop($width, $height, NULL,0)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
 
            return $filename;
        }
 
        return FALSE;
    }
    
} // End Controller_App