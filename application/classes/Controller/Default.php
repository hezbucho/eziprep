<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Default Page
 *
 */
class Controller_Default extends Controller {

	public function action_index()
	{
		$this->redirect("home");
	}

} // End Default
