<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Friends extends Controller_Site
 {
 	protected $permission_actions = array(
		'STUDENT_LOGIN' => 'index'//array('')
	);
	 
	/**
	 * Function to display index page
	 */
	public function action_index() {
		$users = ORM::factory('User');
		// perform a model function that filters a datset
		$search_field = array('nickname', 'firstname','lastname','school_name');//Arr::get($this->_request_params, 'search_field' );
		$search_value = $this->_search_context;//Arr::get($this->_request_params, 'search');
		$list_columns = $users->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($users, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		foreach ($users->find_all() as $key => $model) {
			$content_array[] = array_merge(
					$model->as_array(),
					array(
						'num' => $this->_numbering,
						'active' => ($model->id == $active_record_id),
						//'parent' => $model->parent_category->short_name
					)
				);// $this->model_to_api($model);
			$this->_numbering++;
		}
		//Get Friends List
		// $friends = Model_Friend::get_friend_list(Auth::instance()->get_user()->id);
		// $friends_array=array();
		// foreach ($friends as $friend) {
			// $friends_array[] = $friend->array();
		// }
		
		$this->_template->set('content_data', $content_array);
				
		$this->_set_search_context(I18n::get("nav.site.search.friends"));
		$this->_set_content('search_friend');
		// Manually set current menu
		//$this->_set_current_page('admin/dashboard');
	}

	
	public function action_invite(){
		if($this->request->param('id') and ($this->request->param('id')!= Auth::instance()->get_user()->id)){
			$friend = $this->request->param('id');
			$user_id = Auth::instance()->get_user()->id;
			$is_friend = ORM::factory('Friend')
			->or_where_open()
				->where('fk_user_one','=',$user_id)
				->and_where('fk_user_two','=',$friend)
			->or_where_close()
			->or_where_open()
				->where('fk_user_one', '=', $friend)
				->and_where('fk_user_two', '=' ,$user_id)
			->or_where_close()
			->find();
			if($is_friend->loaded()){
				//TODO: Dislay message is already a friend
				if($is_friend->status==1)
					$this->_set_msg("Your are already friends", "success");
				else {
					$this->_set_msg("Friend request is already pending", "success");
				}
				$this->redirect('friends');
			}else{
				$newfriend = new Model_Friend();
				$newfriend->fk_user_one = $user_id;
				$newfriend->fk_user_two = $friend;
				$newfriend->save();
				$this->_set_msg("Your invitation was successfully sent", "success");
				$this->redirect('friends');
			}
		}else{
			$this->redirect('friends');
		}
	}
	/**
	 * This sction changes the status of friendship invite to accepted
	 */
	public function action_accept(){
		$id = $this->request->query('user_id');
		if($id and $this->request->query('user_id')!= Auth::instance()->get_user()->id){
			$load_invite = ORM::factory('Friend')->where('fk_user_one','=',$id)
			->where('fk_user_two','=',Auth::instance()->get_user()->id)
			->find();
			//var_dump($friend->status);exit;
			$friend = ORM::factory('friend',$load_invite->friendship_id);
			$friend_user_info = ORM::factory('User',$id);
			try{
				$friend->status = '1';
				$friend->save();
				$this->_set_msg('Friendship Accepted','success',$friend_user_info->as_array());
			}catch(Exception $e){
				$this->_set_msg('An error occured','error',TRUE);
			}
			
			
		}
	}
	/**
	 * This action rejects a friend invite and deletes the record
	 */		
	public function action_deny(){
		$id = $this->request->query('user_id');		
		$load_invite = ORM::factory('Friend')->where('fk_user_one','=',$id)
		->where('fk_user_two','=',Auth::instance()->get_user()->id)
		->find();
		
		$friend = ORM::factory('Friend',$load_invite->friendship_id);
		//var_dump($friend->status);exit;
		try{
			$friend->delete();
			$this->_set_msg('Friendship Rejected','success',TRUE);
		}catch(Exception $e){
			$this->_set_msg('An error occured','error',TRUE);
		}		
	}
	public function action_add_friend(){
		if($this->request->post()){
			$friend = ORM::factory('Friend')->invite_friend(Auth::instance()->get_user()->id,$this->request->post('friend'),1);
			if($friend){
				$this->_set_msg('Add friend to contact list','success',TRUE);
			}else{
				$this->_set_msg(' Something went wron. Try again later','error',TRUE);
			}
		}
	}

 }