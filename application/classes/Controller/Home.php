<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Home extends Controller_Site
 {
 	protected $permission_actions = array(
		'STUDENT_LOGIN' => 'index'//array('')
	);
	 
	/**
	 * Function to display index page
	 */
	public function action_index() {
		$content = array();
		//$this->_set_msg("Notification system works great", "success");
		//$this->redirect('home/x');
		$this->_set_content('subscriptions');
	}

	/**
	 * Function to test messaging
	 */
	public function action_x() {
		/*$health_facilities = ORM::factory('HealthFacility');
		// perform a model function that filters a datset
		$search_field = Arr::get($this->_request_params, 'search_field' );
		$search_value = Arr::get($this->_request_params, 'search');
		$list_columns = $health_facilities->getList($search_field, $search_value);
		//print_r($list_columns);
		// Set up pagination params
		$pagination = $this->_setup_pagination($health_facilities, $list_columns);
		//$this->template->pagination = $paging->render();
        $this->_template->set('pagination_data', $pagination);
		
		// Send back the list
		$content_array = array();
		foreach ($health_facilities->find_all() as $key => $model) {
			$content_array[] = $model->as_array();// $this->model_to_api($model);
		}
		$this->_template->set('content_data', $content_array);*/
		
		$this->_set_content('subscriptions');
		// Manually set current menu
		//$this->_set_current_page('admin/dashboard');
	}
 }