<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Lessons extends Controller_Site
{
	protected $permission_actions = array(
		'STUDENT_LOGIN' => array('index', 'view')
	); 
	/**
	 * Function to list all lessons within a subject
	 */
	public function action_index() {
		$this->_template->set('page_title', 'Lessons');
		$subjects = ORM::factory('Subject')->get_active_subjects();
		$this->_template->set('subject_list', $subjects);
		
		if ($this->request->post('subject')){
			$subject_id = $this->request->post('subject');
			foreach ($subjects as $sub) {
				if ($sub->subject_id == $subject_id)
					$this->_template->set('active_subject_title', $sub->subject_title);
			}
			$this->_template->set('active_subject_id', $subject_id);
			$subject_lessons = ORM::factory('Lesson')->get_subject_lessons($subject_id);
			if (count($subject_lessons))// template checks if subject_lessons exists to toggle an error message so set if not empty
				$this->_template->set('subject_lessons', $subject_lessons);
		}
		$this->_set_content('lessons');
	}
	
	/**
	 * Function to get the lesson introduction
	 */
	public function action_intro() {
		$lesson_id = $this->request->param('id');
		$this->_template->set('page_title', 'Introduction');
		$lesson = ORM::factory('Lesson', $lesson_id);
		$content = array();
		if ($lesson->loaded()){
			$content['introduction'] = $lesson->topic_introduction;
			$content['first_subtopic_id'] = $lesson->get_first_subtopic()->subtopic_id;
			$this->_template->set('page_title', $lesson->topic_title . ' - Introduction');
		}
		$this->_template->set('content_data', $content);
		$this->_set_content('lesson_intro');
		$this->_set_current_page('lessons');
	}
	
	/**
	 * Function to get the lesson subtopic page
	 */
	public function action_subtopic() {
		$subtopic_id = $this->request->param('id');
		$subtopic = ORM::factory('Subtopic', $subtopic_id);
		$content = array();
		if ($subtopic->loaded()){
			$content['past_question_ids'] = implode(',', $subtopic->get_past_questions_ids());
			$content['trial_question_ids'] = implode(',', $subtopic->get_trial_questions_ids());
			$content['subtopic_id'] = $subtopic_id;
			$content['notes'] = $subtopic->subtopic_notes;
			$this->_template->set('page_title', $subtopic->lesson->topic_title . ' - ' . $subtopic->subtopic_title);
		}
		$this->_template->set('content_data', $content);
		$this->_set_content('lesson_subtopic');
		$this->_set_current_page('lessons');
	}
	
	/**
	 * Function to get the lesson subtopic past questions
	 */
	public function action_past_questions() { 
		$this->_question_wizard('past');
	}
	
	/**
	 * Function to get the lesson subtopic trial questions
	 */
	public function action_trial_questions() {
		$this->_question_wizard('trial');
	}
	
	private function _question_wizard($question_mode) {
		$titles = array(
			'past' => 'Past Questions',
			'trial' => 'Trial Questions',
		);
		$js_modes = array(
			'past' => 'pq',
			'trial' => 'tq',
		);
		$this->_set_current_page('lessons');
		$content = array();
		$this->_template->bind('content_data', $content);
		$this->_template->set('question_type', $js_modes[$question_mode]);
		$subtopic_id = $this->request->param('id');
		$subtopic = ORM::factory('Subtopic', $subtopic_id);
		$content['question_mode_id'] = $subtopic_id;
		$q_id = $this->request->query('q_id');
		$user_id = Auth::instance()->get_user()->id;
		$answer = $this->request->query('ans');
		$next_id = intval($this->request->query('nxt'));
		$complete = $this->request->query('complete');
		$restart = $this->request->query('restart');
		$correct = 0;
		if ($subtopic->loaded()){
			$question_ids = $subtopic->{'get_' . $question_mode . '_questions_ids'}();
			$content['question_count'] = count($question_ids);
			$this->_template->set('page_title', $titles[$question_mode] . ' - ' . $subtopic->subtopic_title);
			
			if ($restart){
				// wipe progress table and set current id to first
				$subtopic->wipe_user_questions_progress($user_id, $question_mode);
				$q_id = $question_ids[0];
			}
			
			if (!$q_id || !in_array($q_id, $question_ids))
				$q_id = $question_ids[0];// TODO: Pull latest question id user has tackled from dB and get the next one after that from the question_ids array
			
			$question = ORM::factory('Question', $q_id);
			$content['question_pos'] = intval(array_search($q_id, $question_ids)) + 1;
			$content['question'] = $question;
			
			$moving_forward = ($next_id && intval(array_search($next_id, $question_ids)) > intval(array_search($q_id, $question_ids))) ? true : false;
			$moving_backward = (!$moving_forward && $next_id && array_search($next_id, $question_ids) !== false);
			//var_dump($moving_backward);
			if (($answer && ($next_id || $complete)) || $moving_backward){
				// process whether right/wrong
				$answers = $question->answers->find_all();
				$correct_answer = '';
				foreach ($answers as $ans) {
					if (count($answers) > 1 && $ans->status == 1)
						$correct_answer = $ans->answer_id;
					else if(count($answers) == 1 && $ans->status == 1)
						$correct_answer = $ans->answer;
					if ((count($answers) > 1 && $ans->answer_id == $answer && $ans->status == 1) || (count($answers) == 1 && strtolower($ans->answer) == strtolower($answer)))
						$correct = 1;
				}
				
				if ($answer && !$this->request->query('explanation')){
					// update progress table with answer only if request had an answer and the it wasn't from the explanation dialog
					$progress = ORM::factory('Progress')->where('fk_question_id', '=', $q_id)->where('fk_user_id', '=', $user_id)->find();
					if (!$progress->loaded()){
						$progress->fk_question_id = $q_id;
						$progress->fk_user_id = $user_id;
					}
					$progress->status = $correct;
					$progress->save();
				}else if ($moving_backward){
					// delete progress record(s) if moving backwards
					if ($this->request->query('explanation')){
						// delete failed question if in explanation mode
						$progress = ORM::factory('Progress')->where('fk_question_id', '=', $q_id)->where('fk_user_id', '=', $user_id)->find();
						if ($progress->loaded())
							$progress->delete();
					}
					// delete previous question record
					$q_id_pos = array_search($q_id, $question_ids);
					$prev_question_id = $q_id_pos - 1;
					if (array_key_exists($prev_question_id, $question_ids)){
						$progress = ORM::factory('Progress')->where('fk_question_id', '=', $question_ids[$prev_question_id])->where('fk_user_id', '=', $user_id)->find();
						if ($progress->loaded())
							$progress->delete();
					}
					
				}
				
				if (($correct == 1 && ($moving_forward || $complete)) || ($correct == 0 && $moving_backward)){
					//if correct get next question
					if ($next_id && !$complete){
						$content['question'] = ORM::factory('Question', $next_id);
						$content['question_pos'] = intval(array_search($next_id, $question_ids)) + 1;
					}else if ($complete){
						// show score
						$content['user_score'] = $subtopic->get_user_score($user_id, $question_mode);
						$content['total_score'] = $subtopic->get_total_score($question_mode);
						$this->_set_content('questions_score');
						return;
					}else{
						// something wrong with request???
					}
				}else if ($correct == 0 && ($moving_forward || $complete)){			
					// otherwise get explanation and show that instead
					$content['question_explanation'] = $question->explanation;
					$content['question_answer'] = $correct_answer;
					$content['user_answer'] = $answer;
					$this->_set_content('question_explanation');
					return;
				}
			}else if (!$answer && ($next_id || $complete) && $moving_forward){
				// User hasn't provided an answer but has clicked next
				$this->_set_msg("Please answer the question first!", 'error', true);
			}
		}
		
		$this->_set_content('question_wizard');
	}
	
	/**
	 * Function to display single conversation thread
	 */
	public function action_view() {
		$this->_template->set('page_title', 'Lessons');
		$content_data = array();
		// Get 10 latest user conversations
		$this->_user_conversations = Model_Conversation::get_latest_conversations_list(Auth::instance()->get_user()->id);
		$id = $this->request->param('id');
		if (!$id && count($this->_user_conversations)){ // Get latest conversation id
			$newest = current($this->_user_conversations);
			$id = $newest['conversation_id'];
		}
		if (intval($id)) {
	        // Handle POST
	        if ( array_key_exists('reply', $this->request->post()) && array_key_exists('c_id', $this->request->post()) ) {
	            $message = ORM::factory('ConversationReply');
	            // bind values to table columns
	            $fields_to_update = array('fk_user_id', 'fk_conversation_id', 'time', 'reply','ip');
				$this->request->post('ip' , $_SERVER['REMOTE_ADDR']);
				$this->request->post('fk_user_id' , Auth::instance()->get_user()->id);
				$this->request->post('fk_conversation_id' , $this->request->post('c_id'));
				$this->request->post('time' , time());
	            $message->values($this->request->post(), $fields_to_update);
				
				try{
					$message->save();
	                // Update user message
	                $this->_set_msg('Successfully posted message!', 'success', $message->as_array());
	                	
				} catch (Exception $e) {//ORM_Validation_Exception $e){
					$errors = array();
					if ($e instanceof ORM_Validation_Exception)
						$errors = $e->errors('models');
					//$this->_template->set('errors', $errors);
					$this->_set_msg('There was a problem sending your message, please try again!', 'error', $errors);
				}
	        }
			
			$content_data['conversation_threads'] = Model_ConversationReply::get_conversation($id);
			$conversation = Model::factory('Conversation')->where('conversation_id', '=', $id)->find()->as_array();
			$user_id = Auth::instance()->get_user()->id;
			if ($conversation['user_one'] == $user_id)
				$recipient_id = $conversation['user_two'];
			else
				$recipient_id = $conversation['user_one'];
			$conversation['recepient_name'] = ORM::factory('User', $recipient_id)->firstname;
			$content_data['conversation'] = $conversation;
			$content_data['conversation_list'] = $this->_user_conversations;
			
			$this->_template->set('content_data', $content_data);
	        $this->_set_content('conversation');
			// Manually set current menu
			$this->_set_current_page('messages');
		} else {
			if (count($this->_user_conversations))
				throw new HTTP_Exception_404("Sorry, no matching conversation was found!");
			$this->action_start();
		}
    }
	
	/**
	 * Function to start a conversation thread
	 */
	public function action_start() {
		$this->_template->set('page_title', 'New Message');
		$recepient_id = $this->request->param('id');
		$user_id = Auth::instance()->get_user()->id;
		$conversation_id = $recepient_id ? Model_Conversation::start_conversation($user_id, $recepient_id) : null;
		if ($conversation_id) {
			//$this->_set_msg('Successfully posted message!', 'success');
			$this->redirect('messages/view/' . $conversation_id);
		} else {
			$this->_set_msg('There was a problem starting your conversation, please try again!', 'error', true);
		}
		
		$friends = Model_Friend::get_friend_list($user_id);
		
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		foreach ($friends as $key => $model) {
			$content_array[] = $model->as_array();
		}
		
		$this->_template->set('content_data', $content_array);
		$this->_set_content('start_conversation');
	}
	
	/**
	 * Function to delete a category
	 */
	public function action_delete() {
		$this->_template->set('page_title', 'Delete Message');
        $id = $this->request->param('id');
		$message = ORM::factory('Conversation', $id);
		if ($this->request->post()){
			if (array_key_exists('delete', $this->request->post())) {
				try{
					if ($message->loaded()) {
						$message->delete();
						$this->_set_msg('Successfully deleted the category!', 'success');
					}
				}catch (Exception $e){
					$this->_set_msg('The category could not be deleted!', 'error');
				}
			}elseif(array_key_exists('cancel', $this->request->post())){
	        	$this->_set_msg('Record delete cancelled!', 'info');
	        }
			// redirect back
            $redirect = Cookie::get('ref');
            Cookie::delete('ref');
            Cookie::set('active_record', $id);
            $this->redirect($redirect);
		} else {
            // set referrer page
            if ($this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/conversations');
        }
		$this->_set_breadcrumbs_context($message->title, true);
		$this->_template->set('content_data', $message);
		$this->_set_content('category_delete');
    }
 }