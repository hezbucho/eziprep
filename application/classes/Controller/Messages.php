<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_Messages extends Controller_Site
{
	protected $permission_actions = array(
		'STUDENT_LOGIN' => array('index', 'view')
	); 
	/**
	 * Function to list the latest message thread
	 */
	public function action_index() {
		$this->action_view();
		$this->_template->set('page_title', 'Messages');
	}
	
	/**
	 * Function to display single conversation thread
	 */
	public function action_view() {
		$this->_template->set('page_title', 'Messages');
		$content_data = array();
		// Get 10 latest user conversations
		$this->_user_conversations = Model_Conversation::get_latest_conversations_list(Auth::instance()->get_user()->id);
		$id = $this->request->param('id');
		if (!$id && count($this->_user_conversations)){ // Get latest conversation id
			$newest = current($this->_user_conversations);
			$id = $newest['conversation_id'];
		}
		if (intval($id)) {
	        // Handle POST
	        if ( array_key_exists('reply', $this->request->post()) && array_key_exists('c_id', $this->request->post()) ) {
	            $message = ORM::factory('ConversationReply');
	            // bind values to table columns
	            $fields_to_update = array('fk_user_id', 'fk_conversation_id', 'time', 'reply','ip');
				$this->request->post('ip' , $_SERVER['REMOTE_ADDR']);
				$this->request->post('fk_user_id' , Auth::instance()->get_user()->id);
				$this->request->post('fk_conversation_id' , $this->request->post('c_id'));
				$this->request->post('time' , time());
	            $message->values($this->request->post(), $fields_to_update);
				
				try{
					$message->save();
	                // Update user message
	                $this->_set_msg('Successfully posted message!', 'success', $message->as_array());
	                	
				} catch (Exception $e) {//ORM_Validation_Exception $e){
					$errors = array();
					if ($e instanceof ORM_Validation_Exception)
						$errors = $e->errors('models');
					//$this->_template->set('errors', $errors);
					$this->_set_msg('There was a problem sending your message, please try again!', 'error', $errors);
				}
	        }
			
			$content_data['conversation_threads'] = Model_ConversationReply::get_conversation($id);
			$conversation = Model::factory('Conversation')->where('conversation_id', '=', $id)->find()->as_array();
			$user_id = Auth::instance()->get_user()->id;
			if ($conversation['user_one'] == $user_id)
				$recipient_id = $conversation['user_two'];
			else
				$recipient_id = $conversation['user_one'];
			$conversation['recepient_name'] = ORM::factory('User', $recipient_id)->firstname;
			$content_data['conversation'] = $conversation;
			$content_data['conversation_list'] = $this->_user_conversations;
			
			$this->_template->set('content_data', $content_data);
	        $this->_set_content('conversation');
			// Manually set current menu
			$this->_set_current_page('messages');
		} else {
			if (count($this->_user_conversations))
				throw new HTTP_Exception_404("Sorry, no matching conversation was found!");
			$this->action_start();
		}
    }
	
	/**
	 * Function to start a conversation thread
	 */
	public function action_start() {
		$this->_template->set('page_title', 'New Message');
		$recepient_id = $this->request->param('id');
		$user_id = Auth::instance()->get_user()->id;
		$conversation_id = $recepient_id ? Model_Conversation::start_conversation($user_id, $recepient_id) : null;
		if ($conversation_id) {
			//$this->_set_msg('Successfully posted message!', 'success');
			$this->redirect('messages/view/' . $conversation_id);
		} else {
			$this->_set_msg('There was a problem starting your conversation, please try again!', 'error', true);
		}
		
		$friends = Model_Friend::get_friend_list($user_id);
		
		// Send back the list
		$content_array = array();
		$active_record_id = Cookie::get('active_record', 0);
		foreach ($friends as $key => $model) {
			$content_array[] = $model->as_array();
		}
		
		$this->_template->set('content_data', $content_array);
		$this->_set_content('start_conversation');
	}
	
	/**
	 * Function to start a conversation thread
	 */
	
	public function action_latest_conversations() {
		$this->_template->set('content_data', array());
		$time = time();
		while((time() - $time) < 30) {
		    // query memcache, database, etc. for new data
		    $data = Model_Conversation::get_latest_conversations_list(Auth::instance()->get_user()->id, $time);
		    // if we have new data return it
		    if(!empty($data)) {
		        $this->_set_msg('New message!', 'success', $data);
		        break;
		    }
		    usleep(25000);
		}
		//$this->_is_json_request = true;
	}
	
	/**
	 * Function to delete a category
	 */
	public function action_delete() {
		$this->_template->set('page_title', 'Delete Message');
        $id = $this->request->param('id');
		$message = ORM::factory('Conversation', $id);
		if ($this->request->post()){
			if (array_key_exists('delete', $this->request->post())) {
				try{
					if ($message->loaded()) {
						$message->delete();
						$this->_set_msg('Successfully deleted the category!', 'success');
					}
				}catch (Exception $e){
					$this->_set_msg('The category could not be deleted!', 'error');
				}
			}elseif(array_key_exists('cancel', $this->request->post())){
	        	$this->_set_msg('Record delete cancelled!', 'info');
	        }
			// redirect back
            $redirect = Cookie::get('ref');
            Cookie::delete('ref');
            Cookie::set('active_record', $id);
            $this->redirect($redirect);
		} else {
            // set referrer page
            if ($this->request->referrer() && Cookie::get('ref') != $this->request->referrer())
                Cookie::set('ref', $this->request->referrer());
			else
				Cookie::set('ref', 'admin/conversations');
        }
		$this->_set_breadcrumbs_context($message->title, true);
		$this->_template->set('content_data', $message);
		$this->_set_content('category_delete');
    }
 }