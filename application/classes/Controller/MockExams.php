<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_MockExams extends Controller_Site
{
	protected $permission_actions = array(
		'STUDENT_LOGIN' => array('index', 'view')
	); 
	/**
	 * Function to list the latest message thread
	 */
	public function action_index() {
		$this->_template->set('page_title', 'Mock Exams');
		$subjects = ORM::factory('Subject')->get_active_subjects();
		$this->_template->set('subject_list', $subjects);
		
		if ($this->request->post('subject')){ // filter tests based on subject/topic selection
			$subject_id = $this->request->post('subject');
			foreach ($subjects as $sub) {
				if ($sub->subject_id == $subject_id)
					$this->_template->set('active_subject_title', $sub->subject_title);
			}
			$this->_template->set('active_subject_id', $subject_id);
			$subject_mock_exams = ORM::factory('MockExam')->get_subject_mock_exams($subject_id);
			//print_r($subject_mock_exams);
			if (count($subject_mock_exams))// template checks if subject_tests exists to toggle an error message so set if not empty
				$this->_template->set('subject_mock_exams', $subject_mock_exams);
		}
		$this->_set_content('mock_exams');
	}
	
	public function action_take() {
		$this->_question_wizard('take');
	}
	
	public function action_review() {
		$this->_question_wizard('review');
	}
	
	private function _question_wizard($question_mode) {
		$titles = array(
			'take' => 'Taking',
			'review' => 'Reviewing',
		);
		$this->_set_current_page('mockexams');
		$content = array();
		$this->_template->bind('content_data', $content);
		$mock_exam_id = $this->request->param('id');
		$mock_exam = ORM::factory('MockExam', $mock_exam_id);
		$content['question_mode_id'] = $mock_exam_id;
		$q_id = $this->request->query('q_id');
		$user_id = Auth::instance()->get_user()->id;
		$answer = $this->request->query('ans');
		$next_id = intval($this->request->query('nxt'));
		$complete = $this->request->query('complete');
		$restart = $this->request->query('restart');
		$section = $this->request->query('section');
		$correct = 0;
		if ($mock_exam->loaded()) {
			// get first section id
			$section_ids = $mock_exam->get_sections_ids();
			reset($section_ids);
			$section_id = ($section && array_key_exists($section, $section_ids)) ? $section : key($section_ids);
			$content['current_section_id'] = $section_id;
			$content['current_section_name'] = $section_ids[$section_id];

			$question_ids = $mock_exam->get_questions_ids($question_mode, $section_id);	
			$this->_template->set('mock_exam_question_ids', $question_ids);		
			if (count($question_ids)){
				$content['question_count'] = count($question_ids);
				$this->_template->set('page_title', $titles[$question_mode] . ' ' . $mock_exam->mock_exam_title . ' Mock Exam');
				
				if ($restart){
					if ($this->request->action() != 'review'){
						// wipe progress table and set current id to first
						$mock_exam->wipe_user_questions_progress($user_id, $section_id);
					}
					$q_id = $question_ids[0];
				}
				
				if (!$q_id || !in_array($q_id, $question_ids))
					$q_id = $question_ids[0];// TODO: Pull latest question id user has tackled from dB and get the next one after that from the question_ids array
				
				$question = ORM::factory('Question', $q_id);
				$content['question_pos'] = intval(array_search($q_id, $question_ids)) + 1;
				$content['question'] = $question;
				
				$moving_forward = ($next_id && intval(array_search($next_id, $question_ids)) > intval(array_search($q_id, $question_ids))) ? true : false;
				$moving_backward = (!$moving_forward && $next_id && array_search($next_id, $question_ids) !== false);
				//var_dump($moving_backward);
				
				if ($answer || $question_mode == 'review'){
					$answers = $question->answers->find_all();
					// process whether right/wrong
					$correct_answer = '';
					foreach ($answers as $ans) {
						if (count($answers) > 1 && $ans->status == 1)
							$correct_answer = $ans->answer_id;
						else if(count($answers) == 1 && $ans->status == 1)
							$correct_answer = $ans->answer;
						if ((count($answers) > 1 && $ans->answer_id == $answer && $ans->status == 1) || (count($answers) == 1 && strtolower($ans->answer) == strtolower($answer)))
							$correct = 1;
					}
				}
					
				if ($question_mode == 'take'){
					// initialize time
					$now = time();
					if (!$this->session->get('test_start_time') || $restart)
						$this->session->set('test_start_time', $now);
					$exp_time = $this->session->get('test_start_time') + ($mock_exam->mock_exam_time * 60);
					// pass countdown info to page
					$content['timer'] = true;
					$content['exp_time'] = $exp_time * 1000;
					$content['now'] = $now * 1000;
					
					if ($now >= $exp_time){
						// times up! show score
						$complete = true;
						echo 'time is up';
						$this->session->delete('test_start_time');
					}
				}
					
				if ($question_mode == 'take' && ( ($answer && ($next_id || $complete)) || $moving_backward) ){
					
					if (!$this->request->query('explanation')) {
						
						if ($answer){
							// update progress table with answer only if request had an answer and the it wasn't from the explanation dialog
							$progress = ORM::factory('Progress')->where('fk_question_id', '=', $q_id)->where('fk_user_id', '=', $user_id)->find();
							if (!$progress->loaded()){
								$progress->fk_question_id = $q_id;
								$progress->fk_user_id = $user_id;
							}
							$progress->status = $correct;
							$progress->save();
						}else if ($moving_backward){
							// delete progress record(s) if moving backwards
							$q_id_pos = array_search($q_id, $question_ids);
							$prev_question_id = $q_id_pos - 1;
							if (array_key_exists($prev_question_id, $question_ids)){
								$progress = ORM::factory('Progress')->where('fk_question_id', '=', $question_ids[$prev_question_id])->where('fk_user_id', '=', $user_id)->find();
								if ($progress->loaded())
									$progress->delete();
							}
							
						}
					}
					
					if ($next_id && !$complete){
						$content['question'] = ORM::factory('Question', $next_id);
						$content['question_pos'] = intval(array_search($next_id, $question_ids)) + 1;
					}else if ($complete){
						// show score
						$content['user_score'] = $mock_exam->get_user_score($user_id, $section_id);
						$content['total_score'] = $mock_exam->get_total_score($section_id);
						$content['question_type_id'] = $mock_exam_id;
						// set pointer to last for next check
						end($section_ids);
						if (key($section_ids) != $section_id){
							// reset pointer just in case
							reset($section_ids);
							$keys = array_keys($section_ids);
							$position = array_search($section_id, $keys);
							if (isset($keys[$position + 1])) {
							    $next_section_id = $keys[$position + 1];
								$content['next_section_id'] = $next_section_id;
								$content['next_section_name'] = $section_ids[$next_section_id];
							}
						}
						if ($content['user_score'] < $content['total_score'])
							$content['review'] = true;
						if (!isset($content['next_section_id']))
							$this->session->delete('test_start_time');
						$this->_set_content('questions_score');
						return;
					}
				}else if ($question_mode == 'review'){//$correct == 0 && ($moving_forward || $complete)){			
					// otherwise get explanation and show that instead
					if ($next_id && !$complete){
						$question = ORM::factory('Question', $next_id);
						$content['question'] = $question;
						$content['question_pos'] = intval(array_search($next_id, $question_ids)) + 1;
					}else if ($complete){
						// show score
						$content['user_score'] = $mock_exam->get_user_score($user_id, $section_id);
						$content['total_score'] = $mock_exam->get_total_score($section_id);
						$this->_set_content('questions_score');
						return;
					}
					$content['question_explanation'] = $question->explanation;
					$content['question_answer'] = $correct_answer;//$question->answers->where('status', '=', 1)->find();
					$content['user_answer'] = $answer; // TODO: store in session/ remove user answer from explanation to make things easier
					//print_r($content);
					$this->_set_content('question_explanation');
					return;
				}else if (!$answer && ($next_id || $complete) && $moving_forward){
					// User hasn't provided an answer but has clicked next
					$this->_set_msg("Please answer the question first!", 'error', true);
				}
			}else{
				if ($question_mode = 'review')
					$this->_template->set('error_title', 'Nothing to review!');
				$this->_set_content('question_error');
				return;
			}
		}
		$this->_set_content('question_wizard');
	}
	
	
	public function action_review_question_ids(){
		$mock_exam_id = $this->request->param('id');
		$this->_template->set('content_data', '');
		if ($mock_exam_id){
			$q_ids = ORM::factory('MockExam', $mock_exam_id)->get_questions_ids('review');
			$this->_set_msg('Success', 'success', $q_ids);
		}
	}

	public function action_take_question_ids(){
		$mock_exam_id = $this->request->param('id');
		$this->_template->set('content_data', '');
		if ($mock_exam_id){
			$q_ids = ORM::factory('MockExam', $mock_exam_id)->get_questions_ids('take');
			$this->_set_msg('Success', 'success', $q_ids);
		}
	}

 }