<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller contains all logic for admn controllers
 *
 * @version 02 - Hezron Obuchele 2013-01-03
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Controller_Site
 * @author     Hezron Obuchele
 * @package    EziPrep
 * @category   Controllers
 * @copyright  (c) 2013 BeeBuy Investments Ltd. - http://www.beebuy.biz
 */
class Controller_Site extends Controller_App {
	
	/**
	 * System component name
	 * @var string
	 */
	protected $_component_name = 'site';
	
    // Require login to access system
    //public $auth_required = 'admin';
	
    public function before() {
        parent::before();
		$friends_array = $this->_get_friends_list();
		// Set template blocks TODO: Make this dynamic depending on tpl folder contents
		//$this->_template->template()->partial('header', 'header');
		//$this->_template->template()->partial('header_login', 'header_login');
		//$this->_template->template()->partial('menu', $this->_template_path . 'blocks/menu');
		$this->_template->set('menuname', 'student');
		$this->_template->set('friends_data',$friends_array );
		$this->_template->set('no_invites',$this->_get_friend_notifications());
		$this->_template->set('total_notifications',$this->_get_total_notifications());
		$this->_template->set('no_announcements',Helper_Notifications::get_no_announcements());
		$this->_template->set('friend_invites',$this->_get_friend_invites());		
		///$this->_template->set('no_group_invites',$this->_get_group_invites());
		$this->_template->set('group_invites',$this->_get_group_invite_requests());
		//$this->_template->template()->partial('message', $this->_template_path . 'blocks/message');
		//$this->_template->template()->partial('footer', 'footer');
		//$this->_template->template()->partial('pagination', $this->_template_path . 'blocks/pagination');
		
		// Setup language switcher
		$lang_files = Kohana::list_files('i18n', array(APPPATH));
		$lang_codes = array();
		// remove current language from list to avoid unnecessary requests of setting the same language again TODO: Move to template
		//unset($lang_files['i18n' . DIRECTORY_SEPARATOR . I18n::lang() . '.php']);
		foreach ($lang_files as $file_name => $full_path) {
			$code = str_ireplace('i18n' . DIRECTORY_SEPARATOR, '', $file_name);
			$code = str_ireplace('.php', '', $code);
			//echo $code;
			$lang_codes[] = $code;
		}
		$this->_template->set('languages', $lang_codes);
		
		// setup theme switcher
		if (Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.enable_dynamic_themes")){
			$themes = array();
			foreach (Kohana::$config->load("theme-config.component_configs.{$this->_component_name}.themes") as $name => $file) {
				$themes[] = array('name' => $name, 'label' => ucfirst($name));
			}
			
			$this->_template->set('themes', $themes);
		}
		
		// setup breadcrumbs
        $this->_breadcrumbs('Admin');
    }

    /**
     * Get the widgets from the config/dB
     * 
     */
    protected function _widgets($position = '') {
        // load from dB
        //$widgets = ARM::factory('widget')->getWidgets($position);
        //return $widgets;
        // load from config
        $widgets = Kohana::$config->load('widget');
        if ($widgets) {
            // Remove widgets that have been disabled
            foreach ($widgets as $name => $options) {
                if (!Kohana::$config->load('widget.' . $name . '.enabled')) {
                    unset($widgets[$name]);
                }
            }

            // Remove widgets that require a role above the current one
            foreach ($widgets as $name => $options) {
                // if access_level not defined for menu item, move to next item
                if (!array_key_exists('access_level', Kohana::$config->load('widget.' . $name))) {
                    continue;
                }
                $access_level = Kohana::$config->load('widget.' . $name . '.access_level');
                if (!Auth::instance()->logged_in($access_level)) {
                    unset($widgets[$name]);
                }
                $widgets[$name] = (object) $options;
            }
        }
        return $widgets;
    }
	
	/**
	 * Function to generate a report based on the parameters set in the URL
	 */
	public function action_report() {
		$this->response->headers('Content-Type', 'application/pdf');
        $this->response->body(file_get_contents(DOCROOT . DIRECTORY_SEPARATOR . 'reports' . DIRECTORY_SEPARATOR . 'location_stats.pdf'));
		return;
        $data = $this->request->query();
        if (empty($data) || !isset($data['report_type'])) {
            die('Sorry, invalid request!');// TODO: Add 400 error page here
        }
        $report_type = $data['report_type'];
        // get report data
        unset($data['report_type']);
        $params = $data;
        
        foreach ($params as $k => $p){
            if (is_numeric($p))
                $params[$k] = (int) $p;
        }
        // Display the report on browser
        //die(var_dump($params));
        $jasper = new JasperReport($this->response);
        $jasper->generate_report($report_type , $params, 'I');
    }
	
	/**
     * Run any functionality that needs to be performed after the action
     */
    public function after() {
        /*if (!Request::$current->is_ajax()) {
            // Clean up messages & cookies
            $this->_clean_up_msg();
			Cookie::delete('active_record');
			if (strtolower(Request::$current->action()) !== 'report')
            // Run anything that needs to run after this.
            parent::after();
        }*/ parent::after();
		}
	protected function action_notifications(){
		$notifications = array(
		'invites'=>$this->_get_friend_notifications(),
		'total'=>$this->_get_total_notifications()
		);
		$this->_set_msg('Notifications','success', $notifications);		
	}
	private function _get_friends_list(){
		$friends_array = array();
		if(Auth::instance()->logged_in()){
			$friends = Model_Friend::get_friend_list(Auth::instance()->get_user()->id);			
			foreach ($friends as $friend) {
				$friends_array[] = $friend->as_array();
			}
		}

		return $friends_array;
	}
	private function _get_friend_notifications(){
		$notification = "";
		if(Auth::instance()->logged_in()){
			$notification = Helper_Notifications::get_no_invites(Auth::instance()->get_user()->id);
		}
		$total_requests = $notification + intval($this->_get_no_group_invites());
		return $total_requests;
	}
	private function _get_total_notifications(){
		$notifications = "";
		if(Auth::instance()->logged_in()){
			$notifications = Helper_Notifications::get_total_notifications(Auth::instance()->get_user()->id);
		}
		return $notifications;
	}
	private function _get_friend_invites(){
		$invites = array();
		if(Auth::instance()->logged_in()){
			$invites = Helper_Notifications::get_friend_invites(Auth::instance()->get_user()->id);
		}

		return $invites;
	}
	private function _get_no_group_invites(){
		$invites = array();
		if(Auth::instance()->logged_in()){
			$invites = Helper_Notifications::get_no_group_invites(Auth::instance()->get_user()->id);
		}

		return $invites;
	}
	private function _get_group_invite_requests(){
		$invites = array();
		if(Auth::instance()->logged_in()){
			$invites = ORM::factory('StudyGroup')->get_group_invites(Auth::instance()->get_user()->id);
		}

		return $invites;
	}
}
	