<?php defined('SYSPATH') or die('No direct script access.');

class Controller_StudyGroups extends Controller_Site {
	protected $permission_actions = array('STUDENT_LOGIN' => 'index'//array('')
	);

	/**
	 * Function to display index page
	 */
	public function action_index() {
		$this->_template->set('kill_sidemenu', true);
		$group = ORM::factory('StudyGroup') -> get_user_current_group(Auth::instance() -> get_user() -> id);
		$this -> _template -> set('group_data', $group);
		$group_invites = ORM::factory('StudyGroup') -> get_group_invites(Auth::instance() -> get_user() -> id);
		$this -> _template -> set('group_invites', $group_invites);
		///$this->_set_search_context(I18n::get("nav.site.search.groups"));
		$this -> _set_content('study_group');
	}

	public function action_edit() {
		If ($this -> request -> post()) {
			$post = $this -> request -> post();
			$model = ORM::factory('StudyGroup', $post['study_group_id']);
			$model -> values($post);
			$model -> fk_user_id = Auth::instance() -> get_user() -> id;
			$model -> save();
			if ($post['study_group_id'] == '') {
				$membership = ORM::factory('StudyGroup') -> delete_membership(Auth::instance() -> get_user() -> id);
				if ($membership) {
					$new_membership = ORM::factory('StudyGroupMember');
					$new_membership -> fk_user_id = Auth::instance() -> get_user() -> id;
					$new_membership -> fk_study_group_id = $model -> study_group_id;
					$new_membership -> status = 1;
					$new_membership -> save();
				}
			}
			$this -> _template -> set('group_data', $model);
			$this -> _set_msg("Save Study Group Successfully", "success", TRUE);
		}
		if ($this -> request -> query()) {
			if ($this -> request -> query('group')) {
				$model = ORM::factory('StudyGroup', $this -> request -> query('group'));
				$this -> _template -> set('group', $model);
			}
			$this -> _set_content('group-form');
		}

	}

	public function action_find_friends() {
		$friends = Model_Friend::get_friend_list(Auth::instance() -> get_user() -> id, 1, $this -> request -> query('friend'));
		$this -> _template -> set('content_data', $friends);
		$this -> _template -> set('group', $this -> request -> query('group'));
		$this -> _set_content('friend-list-item');
	}

	public function action_join() {
		if ($this -> request -> query()) {
			//CHECK IF THEY BELONG TO A GROUP ALREADY
			$user = Auth::instance() -> get_user();
			$group_id = $this -> request -> query('group');
			$isMember = ORM::factory('StudyGroupMember') -> where('fk_user_id', '=', $user -> id) -> count_all();
			$message = '';
			$confirm = '';

			if ($isMember > 1) {
				$message = " Joining this group will automatically remove you from your current group.";
				$confirm = "Do you want to continue?";
			} else {
				$message = "Are you sure you want to join this group";
			}
			$this -> _template -> set('message', $message);
			$this -> _template -> set('confirm', $confirm);
			$this -> _template -> set('user', $user);
			$this -> _template -> set('has_capacity', 1);
			$this -> _template -> set('status', 1);
			$this -> _template -> set('group', $group_id);
			$this -> _set_content('invite-form');
		}
	}

	public function action_invite() {
		if ($this -> request -> query()) {
			//CHECK IF THEY BELONG TO A GROUP ALREADY
			$user_id = $this -> request -> query('user');
			$group_id = $this -> request -> query('group');
			if (!isset($group_id)) {
				$group_id = ORM::factory('StudyGroup') -> get_user_current_group(Auth::instance() -> get_user() -> id) -> study_group_id;
			}
			$isMember = ORM::factory('StudyGroupMember') -> where('fk_user_id', '=', $user_id) -> count_all();
			$user = ORM::factory('User', $user_id);
			$confirm = '';
			$message = '';
			$has_capacity = ORM::factory('StudyGroup', $group_id) -> has_capacity();
			if ($has_capacity) {
				if ($isMember > 0) {
					$message = $user -> firstname . " " . $user -> lastname . " already belongs to another study group.";
					$confirm = "Do you want to send the request either way?";
				} else {
					$message = "Are you sure you want to add " . $user -> firstname . " " . $user -> lastname . ' to your group';

				}
			}

			//$confirm = "if you continue a friend request will sent also. " . $confirm;
			$this -> _template -> set('message', $message);
			$this -> _template -> set('confirm', $confirm);
			$this -> _template -> set('has_capacity', $has_capacity);
			$this -> _template -> set('user', $user);
			$this -> _template -> set('status', 0);
			$this -> _template -> set('group', $group_id);
			$this -> _set_content('invite-form');
		}

	}

	public function action_join_group() {
		//STEP1: LEAVE ALL CURRENT GROUPS
		$study_group_member_id='';
		if ($this -> request -> post()) {
			$post = $this -> request -> post();
			$membership = FALSE;if (Auth::instance() -> get_user() -> id == $post['user']) {
				//DELETE USER FROM ANY ACTIVE GROUP
				$membership = ORM::factory('StudyGroup') -> delete_membership($post['user']);
			}
			$new_membership = ORM::factory('StudyGroupMember') -> where('fk_user_id', '=', $post['user']) -> where('fk_study_group_id', '=', $post['group']) -> find();
			//STEP2: JOIN REQUESTED GROUP
			if ($membership) {
				$model = ORM::factory('StudyGroupMember', $new_membership -> study_group_member_id);
				$model -> status = $post['status'];
				$model -> save();
				$study_group_member_id = $model->study_group_member_id;
				$add_Friend = ORM::factory('Friend')->invite_friend($post['user'],$model->invited_by,1);
			} else {
				$post['group'] = ORM::factory('StudyGroup') -> get_user_current_group(Auth::instance() -> get_user() -> id) -> study_group_id;
				$model = ORM::factory('StudyGroupMember');
				$model -> fk_user_id = $post['user'];
				$model -> fk_study_group_id = $post['group'];
				$model -> invited_by = Auth::instance() -> get_user() -> id;
				$model -> status = $post['status'];
				$model -> save();
				$study_group_member_id = $model->study_group_member_id;
			}
			$this -> _set_msg('Successfully joined group', 'success', array('invite_id'=>$study_group_member_id));
		}

	}

	public function action_reject() {
		$post = $this -> request -> post();
		if ($post) {
			$rejected = ORM::factory('StudyGroup') -> reject_invite($post['id']);
			if ($rejected) {
				$is_friend = ORM::factory('Friend') -> is_a_friend(Auth::instance() -> get_user() -> id, $post['user']);
				//var_dump($is_friend);exit;
				if ($is_friend <= 0) {
					$this -> _set_msg('Would you like to add friend to contact list', 'success', array('friend' => $post['user'], 'invite_id' => $post['id']));
				} else {
					$this -> _set_msg('Group request rejected', 'success', array('invite_id' => $post['id']));
				}

			} else {
				$this -> _set_msg('Somthing went wrong. Try again later', 'error', TRUE);
			}
		}

	}

	public function action_remove() {
		if ($this -> request -> param('id') and $this -> request -> query('group')) {
			$user = ORM::factory('User', $this -> request -> param('id'));
			$group_id = $this -> request -> query('group');

			$delete_member = ORM::factory('StudyGroup') -> delete_member($user -> id, $group_id);
			if ($delete_member) {
				if (Auth::instance() -> get_user() -> id == $user -> id)
					$this -> _set_msg('You just left the group', 'success');
				else
					$this -> _set_msg($user -> firstname . ' ' . $user -> lastname . ' was removed from your group', 'success');
			}
			$this -> redirect('studygroups/');
		}
	}

	public function action_delete() {
		$group = ORM::factory('StudyGroup', $this -> request -> param('id'));
		if ($group -> delete_members()) {
			try {
				$group -> delete();
				$this -> _set_msg('Group was deleted', 'success');
			} catch(Exception $e) {
				$this -> _set_msg('something went wrong. Please try again', 'error');
			}
			$this -> redirect('studygroups');
		}

	}

}
