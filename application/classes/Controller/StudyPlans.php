<?php defined('SYSPATH') or die('No direct script access.'); 
	  
class Controller_StudyPlans extends Controller_Site
 {
 	protected $permission_actions = array(
		'STUDENT_LOGIN' => 'index'//array('')
	);
	 
	/**
	 * Function to display index page
	 */
	public function action_index() {
		
		$this->_set_content('study_plan');
	}
	
	public function action_edit(){
		if($this->request->query('study_plan_id')){
			$plan = ORM::factory('StudyPlan',$this->request->query('study_plan_id'));
			$this->_template->set('plan', $plan);
		}
		$subjects = ORM::factory('Subject')->order_by('subject_title','DESC')->find_all();
		$topics = ORM::factory('Lesson')->order_by('topic_title','DESC')->find_all();
		$subtopics = ORM::factory('Subtopic')->order_by('subtopic_title','DESC')->find_all();
		$this->_template->set('subjects', $subjects);
		$this->_template->set('topics', $topics);
		$this->_template->set('subtopics', $subtopics);
		$this->_set_content('event-plan');		
	}
	public function action_get_study_plan(){
		$plans = ORM::factory('StudyPlan')->get_study_plan(Auth::instance()->get_user()->id);
		$plans_array=array();
		foreach ($plans as $plan) {
			$year = date('Y');
			$month = date('m');
			$title = '';
			if($plan->event_description){$title = 'Event: '.$plan->event_description;}
			else{$title = 'Academic: '.$plan->subtopic->subtopic_title;}
			$plans_array[] = array('id' => intval($plan->study_plan_id),
			'title' => $title,
			'start' =>   $plan->start,
			'end' => $plan->end,
			'allDay'=>(($plan->allDay)?TRUE:FALSE)
		);
		}
		$this->_set_msg('Got your study plan','success',$plans_array);		
	}
	public function action_save_plan(){
		if($this->request->post('type')=='edit'){
			$post = $this->request->post();
			$plan = ORM::factory('StudyPlan', $post['study_plan_id']);
			$plan->start = $post['start'];
			$plan->end = $post['end'];
			$plan->allDay = $post['allDay'];
			$plan->fk_subtopic_id = $post['fk_subtopic_id'];
			$plan->event_description = $post['event_description'];
			$plan->fk_user_id = Auth::instance()->get_user()->id;
			$plan->save();
			$this->_set_msg('Saved new plan','success',$plan->as_array());
		}
		if($this->request->post('type')=='update'){
			$post = $this->request->post();
			$dayDelta = $post['dayDelta'];
			$minDelta = $post['minuteDelta'];
			$plan = ORM::factory('StudyPlan', $post['study_plan_id']);
			$plan->allDay = $post['allDay'];
		if($post['action']=='drag'){			
			$plan->start = date('Y-m-d H:i:s',strtotime(''.$dayDelta.' days'.$minDelta.' minutes',strtotime($plan->start)));
			$plan->end = date('Y-m-d H:i:s',strtotime(''.$dayDelta.' days'.$minDelta.' minutes',strtotime($plan->end)));;			
		}else if($post['action']=='resize'){
			$plan->end = date('Y-m-d H:i:s',strtotime(''.$dayDelta.' days'.$minDelta.' minutes',strtotime($plan->end)));;
			$plan->allDay = $post['allDay'];
		}			
			$plan->save();
			$this->_set_msg('Saved new plan','success',$plan->as_array());
		}		
	}
	public function action_get_topics(){
		$subject = $this->request->query('subject_id');
		$topics = ORM::factory('Subject',$subject)->get_topics();
		$topics_array = array();
		foreach ($topics as $topic) {
			$topics_array[]=array('id'=>$topic->lesson_id,'title'=>$topic->topic_title);
		}	
		$this->_set_msg('Topics','success',$topics_array);
	}
	public function action_get_subtopics(){
		$topic =  $this->request->query('lesson_id');
		$subtopics = ORM::factory('Lesson',$topic)->subtopics_list();
		$subtopics_array = array();
		foreach ($subtopics as $subtopic) {
			$subtopics_array[]=array('id'=>$subtopic->subtopic_id,'title'=>$subtopic->subtopic_title);
		}
		$this->_set_msg('Topics','success',$subtopics_array);	
	}
	public function action_delete_event(){
		$plan = ORM::factory('StudyPlan',$this->request->post('plan_id'));
		if($plan->loaded()){
			try{
				$plan->delete();
				$this->_set_msg('Event Deleted','success',TRUE);
			}catch(Exception $e){
				$this->_set_msg(' Something went wrong. Try Again Later','error',TRUE);
			}
		}
	}
	
 }