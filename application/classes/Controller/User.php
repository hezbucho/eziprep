<?php defined('SYSPATH') or die('No direct script access.');
/**
 * This controller contains all logic for admn controllers
 *
 * @version 02 - Hezron Obuchele 2013-01-03
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * Controller_User
 * @author     Hezron Obuchele
 * @package    EziPrep
 * @category   Controllers
 * @copyright  (c) 2013 BeeBuy Investments Ltd. - http://www.beebuy.biz
 */
class Controller_User extends Controller_Site {
	
	public function action_index(){
		$this->action_profile();
	}
	
	public function action_login(){
		if ($this->request->post()){
			// try to login user
			$username =  $this->request->post('username');
			$user_password =  $this->request->post('password');
			$remember =  $this->request->post('remember');
			if ($username !== null && $user_password !== null){
	            Auth::instance()->login($username, $user_password, $remember);
				//var_dump(Auth::instance()->logged_in());
				if (Auth::instance()->get_user()){
					$user = ORM::factory('User',Auth::instance()->get_user()->id);
					$user->online_status = 1;
					$user->save();
					if(Auth::instance()->logged_in('admin'))
						$this->redirect('admin');
					else
						$this->redirect(Cookie::get('user_redirect', 'home'));
				}					
				else{
					$this->_template->set('login_msg','Login Error! Wrong username or password');
				}
					
	        }
		}
		// display login page
		$content = array();
		//$this->_set_msg("Notification system works great", "success");
		//$this->redirect('admin/dashboard/x');
		$this->_set_content('registration');
		$this->_template->set('content_data', $content);
	}
	
	public function action_logout(){
		if(Auth::instance()->logged_in()){
			$user = ORM::factory('User',Auth::instance()->get_user()->id);
			$user->online_status = 0;
			$user->save();
		}
		if (Auth::instance()->logged_in('student')){
			Auth::instance()->logout();
			$this->_set_msg("You are now logged out.", "success");
			$this->redirect('home');
		}else if(Auth::instance()->logged_in('admin')){
			Auth::instance()->logout();
			//$this->_set_msg("You are now logged out.", "success");
			$this->redirect('admin');
		}
		
	}
	
	public function action_password_reset(){
		
	}
	
	public function action_profile(){
		$this->_template->set('kill_sidemenu', true);
		// display profile page
		$content = array();
		if ($this->request->post()){// registration/profile update routine
			//echo $this->request->post('user_id');exit;
			$model = ORM::factory('User', $this->request->post('user_id'));
			$new_user = !$model->loaded();
			//var_dump($this->request->post());exit;
			//$school_id = $this->_validate_school($this->request->post());
			$model->values($this->request->post());
			
			try{
				if ($this->request->post('school_name')) {
					$school_id = $this->_validate_school($this->request->post());
					$model->fk_school_id = $school_id;
				}if(isset($_FILES['avatar'])){					
					$directory =  DOCROOT.'assets/avatars/';
					if($model->avatar){
						unlink($directory.$model->avatar);
					}
					$avatar =  $this->save_image($_FILES['avatar'],60,60,$directory);
					$model->avatar= $avatar;
				}else{
					if(!$model->avatar)
						$model->avatar = "default-avatar.jpg";
				}				
				$model->save();
				
				// bind fields common to both forms
				$model->student_info->fk_user_id = $model->id;
				$model->student_info->dob = $this->request->post('dob');
				$model->student_info->gender = $this->request->post('gender');
				$model->student_info->save();
				// registration routine
				if ($new_user){
					// add a role; add() executes the query immediately
					$model->add('roles', ORM::factory('Role')->where('name', '=', 'student')->find());
					$model->add('roles', ORM::factory('Role')->where('name', '=', 'login')->find());
					$this->_set_msg("You have successfully registered.", "success");
					//$this->redirect('user/login');
					//Autlogin user after registration
					Auth::instance()->login($model->username,$this->request->post('password'));
					if (!Auth::instance()->logged_in())
						$this->redirect('user/login');
					else
						$this->redirect('user/profile/'.$model->id);
				}else{
					
					$model->student_info->values($this->request->post());
					$model->student_info->save();
					$this->_set_msg("Profile successfully updated.", "success");
					$this->redirect('user/profile/'.Auth::instance()->get_user()->id);
				}
				//$content[]
			} catch (ORM_Validation_Exception $e){
				$this->_set_msg('Please correct the errors below and try again!', 'error');
				$this->_template->set('errors', $e->errors('models'));
				if ($new_user){
					
				}
					//$this->redirect('user/login');
					//var_dump($e);exit;
			}
		}elseif($this->request->param('id')){
			//Fetch User Data
			$user = ORM::factory('User',$this->request->param('id'));
			$content = $user->as_array();
			//Gender
			$gender = array('M'=>'M','F'=>'F');
			//Fetch Student Information based on user data
			$student = $user->student_info->as_array();
			//Fetch Country List
			$countries = ORM::factory('Country')->find_all()->as_array();
			//var_dump($student);exit;
			//Fetch List of Schools
			$schools = ORM::factory('School')->select(array('school_name','label'),array('school_id','value'))->find_all();
			$schools_array=array();
			foreach ($schools as $school) {
				$schools_array[]=$school->as_array();
				
			}
			$this->_template->set('schoolsjson', htmlspecialchars_decode(json_encode($schools_array), ENT_QUOTES));
			
			$this->_set_content('profile');
			$this->_template->set('user_data', $content);
			$this->_template->set('country_data', $countries);
			$this->_template->set('student_data', $student);
			$this->_template->set('gender_data', $gender);
			$this->_template->set('school_data', $this->get_schools());
		}else{
			throw new HTTP_Exception_404('Page Not Found');
		}
		
		
		
	}
	public function action_question(){
		$recovery_username = $this->request->query('username');
		$user = ORM::factory('User')->where('username','=',$recovery_username)->find();
		if($user->loaded()){
			$this->_set_msg('Please answer your security question','success',$user->as_array());
		}else{
			$this->_set_msg('The username provided does not exist','error',TRUE);
		}
		
	}
	public function action_change_password(){
		$answer = $this->request->post('answer');
		$username = $this->request->post('username');
		$new_password = $this->request->post('password');
		$user = ORM::factory('User')->where('username','=',$username)->where('answer','=',$answer)->find();
		if($user->loaded()){
			$model = ORM::factory('User',$user->id);
			$model->password = $new_password;
			$model->save();
			$this->_set_msg('Password Successfully changed you can now login','success',TRUE);
		}else{
			$this->_set_msg('You provided the wrong answer','error',TRUE);
		}
		
		
	}
	public function get_schools(){
		$schools = ORM::factory('School')->find_all();
		$schools_array=array();
		foreach ($schools as $school) {
			$schools_array[]=(object)$school->as_array();//array('school_id'=>$school->school_id,'school_name'=>$school->school_name);
			
		}
		
        	return  json_encode($schools_array);
		
		
	}
	private function _validate_school($post){
		$school = ORM::factory('School')
		->where('school_name','=',$post['school_name'])
		->where('principal','=',$post['principal'])
		->where('school_address','=',$post['school_address'])
		->where('school_location','=',$post['school_location'])
		->find();
		if($school->loaded()){
			return $post['fk_school_id'];
		}else{
			$school = ORM::factory('School');
			$school->school_name = $post['school_name'];
			$school->principal = $post['principal'];
			$school->school_address = $post['school_address'];
			$school->school_location = $post['school_location'];
			$school->save();
			return $school->school_id;
		}
	}
	
}

?>