<?php defined('SYSPATH') OR die('No direct script access.');

abstract class Log_Writer extends Kohana_Log_Writer {

	public function format_message(array $message, $format = "time --- level: body in file:line") {
		$additional = isset($message['additional']['exception']) ? $message['additional']['exception'] : null;
		$message['time'] = Date::formatted_time('@' . $message['time'], Log_Writer::$timestamp, Log_Writer::$timezone, TRUE);
		$message['level'] = $this -> _log_levels[$message['level']];
		unset($message['additional']);
		unset($message['trace']);

		$string = strtr($format, $message);

		if ($additional) {
			// Re-use as much as possible, just resetting the body to the trace
			$message['body'] = $additional -> getTraceAsString();
			$message['level'] = $this -> _log_levels[Log_Writer::$strace_level];

			$string .= PHP_EOL . strtr($format, $message);
		}

		return $string;
	}

}
