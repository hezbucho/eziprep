<?php defined('SYSPATH') or die('No direct script access.');
class Model_Announcement extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'announcement_id';
	//Table
	protected $_table_name = 'announcements';
	//Belongs to relationship
	protected $_belongs_to = array('user'=>array('model'=>'User','foreign_key'=>'fk_user_id'),'school'=>array('model'=>'School','foreign_key'=>'fk_school_id'),'level'=>array('model'=>'EducationLevel','foreign_key'=>'fk_education_level_id'),'country'=>array('model'=>'Country','foreign_key'=>'fk_country_id'));
	
	/**
	 * Get a list of Announcements
	 * @param $search_field an array containing columns to search with
	 * @param $search_value the search value inputed
	 * @param bool if True returns only active announcements otherwise FALSE returns all
	 * @return $table_columns array of table columns
	 */
	public function get_announcements($search_field, $search_value,$active=FALSE){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		if($active)
			$this->where('status','=',1);
		// make use of default search filtering accross multiple fields 
		$this->order_by('date','DESC');
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here
		
		return $table_columns;
	}
	/*
	 * Get number of active announcements
	 */
	public function get_no_active_announcements(){
		$this->where('status','=',1);
		return $this->count_all();
	}
}