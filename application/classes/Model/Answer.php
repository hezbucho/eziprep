<?php defined('SYSPATH') or die('No direct script access.');

class Model_Answer extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'answers';
	protected $_load_with = array('question');
	public static $default_answers = array(
				array('answer_id'=>'','answer'=>'','status'=>'1'),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
				array('answer_id'=>'','answer'=>'','status'=>''),
			);
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'answer_id';
	
	protected $_belongs_to = array('question'=>array('model'=>'Question','foreign_key'=>'fk_question_id'));
	//protected $_has_one = array('question'=>array('model'=>'Question','foreign_key'=>'question_id'));
	/**
	 * Saves an answer and the options for a particular question
	 * @param $post (array containing answer and options)
	 * @param $question_id id of the question
	 * @return boolean
	 */
	public function save_answers($post,$question_id){
		if(!empty ($post['answers'])){
			$is_assoc = Arr::is_assoc($post['answers']);			
			foreach ($post['answers'] as $key => $value) {
				//check if its a fresh bash of answers or an edit to existing answers
				if($is_assoc)
					$answer = ORM::factory('Answer',$key);
				else
					$answer = ORM::factory('Answer');
				$answer->answer = $value;
				$answer->fk_question_id = $question_id;
				//if a new batch of answers assume the first indexed value as the correct answer 
				if($key == 0)
					$answer->status = 1;
				$answer->save();
			}
			return true;
		}
	}
	/**
	 * Return the list of answers for a particular question
	 * @param $question_id id of the question
	 * @return array of ORM objects for answers
	 */
	public function get_question_answers($question_id){
		//ORM::factory('Answer')->where('fk_question_id','=',$model->fk_question_id)->find_all()->as_array();
		return $this->where('fk_question_id','=',$question_id)->find_all();
	}
	
}
?>