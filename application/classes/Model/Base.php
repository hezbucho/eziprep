<?php defined('SYSPATH') or die('No direct script access.');
class Model_Base extends ORM {
	
	public function __get($column)
	{
		if (substr($column, 0, 1) == '_'){ // for use with internal property caches
			if (!array_key_exists($column, $this->_object))
				return null;
		}
		return $this->get($column);
	}
	
	public function set($column, $value){
	    if ($column == '@index') 
	       $this->_object['@index'] = null;
		if (substr($column, 0, 1) == '_') // for use with internal property caches
			$this->_object[$column] = null;
		parent::set($column, $value);
	}
	
	protected function _get_table_columns(array $objects){
		$_final_table_columns = array();
		foreach ($objects as $obj) {
			if (array_key_exists($obj, ORM::$_column_cache))
				$_final_table_columns = array_merge($_final_table_columns, ORM::$_column_cache[$obj]);
		}
		return $_final_table_columns;
	}
	
	protected function _search_list($search_field, $search_value, $table_columns){
		if (is_string($search_field) && strlen($search_field) > 0)
			$search_field = array($search_field);
		if ($search_field && $search_value){
			$pass = 1;
			foreach ($search_field as $field) {
				if (strtolower($field) == 'id')
					$field = $this->primary_key();
				$comparator = ($table_columns[$field]['type'] == 'string') ? 'LIKE' : '=';
				$search_value = ($table_columns[$field]['type'] == 'string') ? '%' . $search_value . '%' : $search_value;
				$search_value = (strpos($table_columns[$field]['type'], 'int') !== false) ? intval($search_value) : $search_value;
				if (array_key_exists($field, $table_columns) && $pass > 1)
					$this->or_where($field, $comparator, $search_value);
				elseif (array_key_exists($field, $table_columns) && $pass == 1)
					$this->where($field, $comparator, $search_value);
				$pass++;
			}
		}
	}
}