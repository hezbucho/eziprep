<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Model representing conversations table
 *
 * @version 01 - Hezron Obuchele 2013-05-22
 *
 * PHP version 5
 */
class Model_Conversation extends ORM {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'conversations';
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'conversation_id';
	
	/**
	 * Model's relationships
	 * @array
	 */
	protected $_belongs_to = array(
		'user' => array('model' => 'User', 'foreign_key' => 'user_one'),
	);
	
	protected $_has_many = array(
		'replies' => array('model' => 'ConversationReply', 'foreign_key' => 'fk_conversation_id'),
	);
							
	protected $_has_one = array(
		//'recepient' => array('model' => 'User', 'foreign_key' => 'user_two'),
	);
							
	/**
	 * Setup validation rules
	 *
	 * @return array
	 */
	public function rules() {
		// TODO: See if there are any rules to be added
		return array(
			/*'name' => array(array('not_empty')),
			'contact_person' => array(array('regex', array(':value', '/^[-\pL\pN_.]++$/uD'))),
			'telephone' => array(array('not_empty')),
			'email_address' => array(array('not_empty'), array('email')),
			'postal_address' => array(array('not_empty')),*/
		);
	}
	
	/**
	 * Get full conversation thread with replies
	 */
	public static function get_latest_conversations_list($user_id, $time = 0, $sort = SORT_DESC){
		$query = DB::query(Database::SELECT, 'SELECT C.conversation_id, U.id AS user_id, U.firstname, U.lastname, U.username, U.email FROM conversations C, users U WHERE' . 
		' CASE WHEN C.user_one = :user_id' .
		' THEN C.user_two = U.id' .
		' WHEN C.user_two = :user_id' .
		' THEN C.user_one = U.id' .
		' END AND (C.user_one = :user_id OR C.user_two = :user_id) ORDER BY C.conversation_id DESC LIMIT 10');
		$query->parameters(array(
		    ':user_id' => $user_id,
		));
		//echo $query . '<br />';
		$list = array();
		foreach ($query->execute()->as_array() as $key => $row) {
			$query = DB::query(Database::SELECT, 'SELECT R.conversation_reply_id, R.time, R.reply FROM conversation_replies R WHERE R.fk_conversation_id = :c_id AND R.fk_user_id != :user_id AND time > :time ORDER BY R.conversation_reply_id DESC LIMIT 1');
			$query->parameters(array(
			    ':c_id' => $row['conversation_id'],
			    ':user_id' => $user_id,
			    ':time'	=> $time
			));
			//echo $query . '<br />';
			$result = $query->execute()->as_array();
			if (count($result)){
				$result = array_merge($result[0], $row);
				$list[$result['conversation_reply_id']] = $result;
			}
		}
		asort($list, $sort);
		return $list;
	}
	
	/**
	 * Start a new conversation thread between the two users
	 */
	public static function start_conversation($user_id, $recepient_id){
		if ($user_id != $recepient_id) {
			$query = DB::query(Database::SELECT, 'SELECT conversation_id FROM conversations WHERE (user_one = :user_id AND user_two = :recepient_id) OR (user_one = :recepient_id AND user_two = :user_id) ');
			$query->parameters(array(
			    ':user_id' => $user_id,
			    ':recepient_id' => $recepient_id,
			));
			//echo $query . '<br />';
			$result = $query->execute()->as_array();
			if (!count($result)){
				$query = DB::query(Database::INSERT, 'INSERT INTO conversations (user_one, user_two, ip, time) VALUES (:user_id, :recepient_id, :ip, :time)')
						->parameters(array(
						    ':user_id' => $user_id,
						    ':recepient_id' => $recepient_id,
						    ':ip' => $_SERVER['REMOTE_ADDR'],
						    ':time' => time() // TODO: Create helper to deal with system vs user times
						));
				$r = $query->execute();
				return $r[0];
			}
			return $result[0]['conversation_id'];
		}
		return false;
	}
}
?>