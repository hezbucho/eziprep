<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Model representing geometry_types table
 *
 * @version 01 - Hezron Obuchele 2013-02-11
 *
 * PHP version 5
 */
class Model_ConversationReply extends ORM {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'conversation_replies';
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'conversation_reply_id';
	
	/**
	 * Model's relationships
	 * @array
	 */
	protected $_belongs_to = array(
		'conversation' => array('model' => 'Conversation', 'foreign_key' => 'fk_conversation_id'),
		'user' => array('model' => 'User', 'foreign_key' => 'fk_user_id'),
	);
							
	/**
	 * Setup validation rules
	 *
	 * @return array
	 */
	public function rules() {
		// TODO: See if there are any rules to be added
		return array(
			/*'name' => array(array('not_empty')),
			'contact_person' => array(array('regex', array(':value', '/^[-\pL\pN_.]++$/uD'))),
			'telephone' => array(array('not_empty')),
			'email_address' => array(array('not_empty'), array('email')),
			'postal_address' => array(array('not_empty')),*/
		);
	}
	
	/**
	 * Get full conversation thread with replies
	 */
	public static function get_conversation($conversation_id){
		//$query = "SELECT R.cr_id,R.time,R.reply,U.user_id,U.username,U.email FROM users U, conversation_reply R WHERE R.user_id_fk=U.user_id and R.c_id_fk='$c_id' ORDER BY R.cr_id ASC LIMIT 20";
		$query = DB::query(Database::SELECT, 'SELECT R.conversation_reply_id, R.time, R.reply, U.id AS user_id, U.username, U.email FROM users U, conversation_replies R WHERE R.fk_user_id = U.id AND R.fk_conversation_id = :c_id ORDER BY R.conversation_reply_id ASC LIMIT 20');
		$query->parameters(array(
		    ':c_id' => $conversation_id,
		));
		//echo $query;
		$result = $query->execute();
		return $result->as_array();
	}
}
?>