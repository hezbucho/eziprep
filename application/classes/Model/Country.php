<?php defined('SYSPATH') or die('No direct script access.');
class Model_Country extends Model_Base {	
	//Primary Key
	protected $_primary_key = 'country_id';
	//TODO: Add has many relationship for students and schools
	/**
	 * Get a list of Countries
	 */
	public function get_countries($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;	
	}
	/*
	 * Get List of countries (Deprecated Function user get_countries)
	 */
	public function getList($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;	
	}
	public static function get_country_by_name($string){
		return ORM::factory('Country')->where('country_name','=',$string)->find()->country_id;
	}

}