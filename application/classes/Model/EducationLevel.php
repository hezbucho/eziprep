<?php defined('SYSPATH') or die('No direct script access.');

class Model_EducationLevel extends ORM {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'education_levels';
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'education_level_id';
	
	/**
	 * Get a list of Educational Levels
	 */
	public function getList($search_field, $search_value){
		// TODO: Ensure all columns to be returned in the final resultset are in this list e.g. merge all referenced/joined models columns
		
		$columns=ORM::$_column_cache[$this->object_name()];
		$table_columns=$columns;
		//var_dump($columns);exit;
		$_final_table_columns = $table_columns;
		if (is_string($search_field) && strlen($search_field) > 0)
			$search_field = array($search_field);
		if ($search_field && $search_value){
			$pass = 1;
			foreach ($search_field as $field) {
				if (strtolower($field) == 'id')
					$field = $this->primary_key();
				$comparator = ($table_columns[$field]['type'] == 'string') ? 'LIKE' : '=';
				$search_value = ($table_columns[$field]['type'] == 'string') ? '%' . $search_value . '%' : $search_value;
				$search_value = (strpos($table_columns[$field]['type'], 'int') !== false) ? intval($search_value) : $search_value;
				if (array_key_exists($field, $table_columns) && $pass > 1)
					$this->or_where($field, $comparator, $search_value);
				elseif (array_key_exists($field, $table_columns) && $pass == 1)
					$this->where($field, $comparator, $search_value);
				//elseif (array_key_exists($search_field, $this->_api_to_db_map) && array_key_exists($this->_api_to_db_map[$search_field], $table_columns))
					//$model->where($this->_api_to_db_map[$search_field], $comparator, $search_value);
				$pass++;
			}
		}
		
		return $_final_table_columns;
	}
	public  function levels($country=FALSE){
		if($country){
			$this->where('fk_country_id','=',$country);
		}
		return $this->find_all();
	}
	public static function get_level_by_name($string){
		return ORM::factory('EducationLevel')->where('level_name','=',$string)->find()->education_level_id;
	}
}
?>