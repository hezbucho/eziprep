<?php defined('SYSPATH') or die('No direct script access.');
class Model_ExamSession extends Model_Base {
	
	
	
	//Primary Key
	protected $_primary_key = 'exam_session_id';
	//Table  name
	protected $_load_with = array('questionset');
	protected $_table_name = 'exam_sessions';
	public static $_subject_id;
	public static $_country_id;
	protected $_has_many = array('questionsets'=>array('model'=>'PastQuestion','foreign_key'=>'fk_exam_session_id'));
	
	public function get_exam_sessions($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;
	}
	public function get_past_questions(){
		$question_set = $this->questionsets;
		if(Model_ExamSession::$_subject_id>=1){
			$question_set->where('fk_subject_id','=',Model_ExamSession::$_subject_id);	
		}
		if(Model_ExamSession::$_country_id>=1){
			$question_set->where('fk_country_id','=',Model_ExamSession::$_country_id);	
		}
		return $question_set->find_all();		
	}
}