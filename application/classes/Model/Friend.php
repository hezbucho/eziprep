<?php defined('SYSPATH') or die('No direct script access.');
class Model_Friend extends ORM {

	public $_primary_key = "friendship_id";
	//protected $_has_one = array('recepient' => array('model' => 'User', 'foreign_key' => 'id'),

	/**
	 * Gets a list of your friends with their personal information
	 * @param $user_id the id of the currently logged in user
	 * @return $friend_list orm object array with friends
	 */
	public static function get_friend_list($user_id, $status = 1, $search_value = FALSE) {
		$friends = array();
		$friends = ORM::factory('Friend') -> or_where_open() -> where('fk_user_one', '=', $user_id) -> or_where('fk_user_two', '=', $user_id) -> or_where_close() -> and_where_open() -> where('status', '=', $status) -> and_where_close() -> find_all();

		//var_dump($friends);exit;
		$friends_array = Model_Friend::_friends_array($friends, $user_id);
		$friends_list = array();
		if (count($friends_array)) {
			if ($search_value) {
				$friends_list = ORM::factory('User') -> where('id', 'IN', $friends_array) -> where('id', '!=', $user_id) -> and_where_open() -> where('firstname', 'LIKE', '%' . $search_value . '%') -> or_where('lastname', 'LIKE', '%' . $search_value . '%') -> or_where('username', 'LIKE', '%' . $search_value . '%') -> and_where_close() -> find_all();
			} else {
				$friends_list = ORM::factory('User') -> where('id', 'IN', $friends_array) -> where('id', '!=', $user_id) -> find_all();
			}

		}

		return $friends_list;
	}

	/**
	 * Returns a list of the is of your friends as an array on int
	 * @param $friendsobject an orm object array of friends
	 * @param $user_id id of currently logged in user
	 * @return array containing the id of your friends
	 */
	public static function _friends_array($friendsobject, $user_id) {
		$friends_array = array();
		foreach ($friendsobject as $friend) {
			if ($friend -> fk_user_one == $user_id) {
				$friends_array[] = $friend -> fk_user_two;
			} else {
				$friends_array[] = $friend -> fk_user_one;
			}

		}
		return $friends_array;
	}

	public function is_a_friend($user_id, $friend) {
		return $this -> or_where_open() -> where('fk_user_one', '=', $user_id) -> and_where('fk_user_two', '=', $friend) -> or_where_close() -> or_where_open() -> where('fk_user_one', '=', $friend) -> and_where('fk_user_two', '=', $user_id) -> or_where_close() -> count_all();
	}

	public function invite_friend($user_id, $friend, $status = 0) {
		$check = ORM::factory('Friend') -> is_a_friend($user_id, $friend);
		if ($check<=0) {
			$newfriend = ORM::factory('Friend');
			$newfriend -> fk_user_one = $user_id;
			$newfriend -> fk_user_two = $friend;
			$newfriend -> status = $status;
			try {
				$newfriend -> save();
				return true;
			} catch(Exception $e) {
				return false;
			}
		}

	}

}
