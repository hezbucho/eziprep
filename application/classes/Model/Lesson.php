<?php defined('SYSPATH') or die('No direct script access.');
class Model_Lesson extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'lesson_id';
	
	//Table  name
	protected $_table_name = 'lessons';
	protected $_load_with = array('subject','level','country');
	protected $_belongs_to = array('subject'=>array('model'=>'Subject','foreign_key'=>'fk_subject_id'),
	'level'=>array('model'=>'EducationLevel','foreign_key'=>'fk_education_level_id'),'country'=>array('model'=>'Country','foreign_key'=>'fk_country_id'));
	//TODO: Add has many relationship for students and schools
	protected $_has_many = array(
		'subtopics'=>array('model'=>'Subtopic','foreign_key'=>'fk_lesson_id'),
		'tests'=>array('model'=>'Test','foreign_key'=>'fk_lesson_id'),
	);
	public function get_level(){
		return $this->level;
	}
	
	/**
	 * Get a list of Educational Levels
	 */
	public function get_subject_lessons($subject_id){
		return $this->where('fk_subject_id', '=', $subject_id)->find_all();
	}
	
	public function get_first_subtopic(){
		return $this->subtopics->find();
	}
	
	public function subtopics_list(){
		return $this->subtopics->find_all();
	}
	/**
	 * Get a list of Educational Levels
	 */
	public function getList($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		$search = array_merge(ORM::factory('Country')->list_columns(),ORM::factory('EducationLevel')->list_columns(),ORM::factory('Subject')->list_columns());
		//var_dump(array_merge($table_columns,$search));exit;
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, array_merge($table_columns,$search));
		
		// and/or perform other custom logic here
		
		return $table_columns;
	}
	
	public function topics_list(){
		return $this->find_all();
	}
	public  function topics($subject=FALSE,$level=FALSE,$country=FALSE){
		//$topics = $this;
		if($subject){
			$this->where('fk_subject_id','=',$subject);
		}
		if($country){
			$this->where('level.fk_country_id','=',$country);
		}
		if($level){
			$this->where('fk_education_level_id','=',$level);
		}
		return $this->find_all();
	}

}