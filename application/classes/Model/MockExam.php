<?php defined('SYSPATH') or die('No direct script access.');
class Model_MockExam extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'mock_exam_id';
	//Table  name
	protected $_table_name = 'mock_exams';
	protected $_load_with = array('subject','level','country');
	protected $_belongs_to = array('subject'=>array('model'=>'Subject','foreign_key'=>'fk_subject_id'),'level'=>array('model'=>'EducationLevel','foreign_key'=>'fk_education_level_id'),'country'=>array('model'=>'Country','foreign_key'=>'fk_country_id'));
	protected $_has_many = array('mock_exam_questions'=>array('model'=>'MockExamQuestion','foreign_key'=>'fk_mock_exam_id'),'sections'=>array('model'=>'Section','foreign_key'=>'fk_mock_exam_id'));
	//protected $_has_many = array('questions'=>array('model'=>'Subtopic','foreign-key'=>'fk_lesson_id'));
	
	/**
	 * Get a list of Tests
	 */
	public function getList($search_field, $search_value){
		// TODO: Ensure all columns to be returned in the final resultset are in this list e.g. merge all referenced/joined models columns
		
		$columns=ORM::$_column_cache[$this->object_name()];
		$table_columns=$columns;
		//var_dump($columns);exit;
		$_final_table_columns = $table_columns;
		if (is_string($search_field) && strlen($search_field) > 0)
			$search_field = array($search_field);
		if ($search_field && $search_value){
			$pass = 1;
			foreach ($search_field as $field) {
				if (strtolower($field) == 'id')
					$field = $this->primary_key();
				$comparator = ($table_columns[$field]['type'] == 'string') ? 'LIKE' : '=';
				$search_value = ($table_columns[$field]['type'] == 'string') ? '%' . $search_value . '%' : $search_value;
				$search_value = (strpos($table_columns[$field]['type'], 'int') !== false) ? intval($search_value) : $search_value;
				if (array_key_exists($field, $table_columns) && $pass > 1)
					$this->or_where($field, $comparator, $search_value);
				elseif (array_key_exists($field, $table_columns) && $pass == 1)
					$this->where($field, $comparator, $search_value);
				//elseif (array_key_exists($search_field, $this->_api_to_db_map) && array_key_exists($this->_api_to_db_map[$search_field], $table_columns))
					//$model->where($this->_api_to_db_map[$search_field], $comparator, $search_value);
				$pass++;
			}
		}
		
		return $_final_table_columns;
	}

	public function get_sections_ids() {
		if (!$this->_section_ids)
				$this->_section_ids = $this->sections->find_all()->as_array('section_id', 'section_title');
			return $this->_section_ids;
	}
	
	public function get_questions_count(){
		if (!$this->_questions_count)
			$this->_questions_count = $this->mock_exam_questions->count_all();
		return $this->_questions_count;
	}
	
	public function get_subject_mock_exams($subject_id){
		return $this->where('fk_subject_id', '=', $subject_id)->find_all();
	}
	
	public function get_attempted_questions_count(){
		if (!$this->_attempted_questions_count)
			$this->_attempted_questions_count = $this->mock_exam_questions->join('progress')->on('progress.fk_question_id', '=', 'mockexamquestion.fk_question_id')->where('fk_mock_exam_id', '=', $this->pk())->where('fk_user_id', '=', Auth::instance()->get_user()->id)->count_all();
		return $this->_attempted_questions_count;
	}

	public function wipe_user_questions_progress($user_id, $section_id = false){
		$progress_records = ORM::factory('Progress')->where('fk_question_id', 'IN', $this->get_questions_ids('take', $section_id))->where('fk_user_id', '=', $user_id)->find_all();
		foreach ($progress_records as $p) {
			$p->delete();
		}
	}

	public function get_questions_ids($mode, $section_id = false){
		if (!$section_id){
			$section_ids = $this->get_sections_ids();
			reset($section_ids);
			$section_id = key($section_ids);
		}
		if ($mode == 'review' && $section_id){
			if (!$this->_review_questions_ids)
				$this->_review_questions_ids = $this->mock_exam_questions->join('progress')->on('progress.fk_question_id', '=', 'mockexamquestion.fk_question_id')->where('progress.status', '=', 0)->where('mockexamquestion.fk_section_id', '=', $section_id)->find_all()->as_array(null, 'fk_question_id');
			return $this->_review_questions_ids;
		}elseif($mode == 'take' && $section_id){
			if (!$this->_retake_questions_ids)
				$this->_retake_questions_ids = $this->mock_exam_questions->where('fk_section_id', '=', $section_id)->find_all()->as_array(null, 'fk_question_id');
			return $this->_retake_questions_ids;
		}
		return array();
	}
	
	/*public function get_sections_ids(){
		if ($mode == 'review'){
			if (!$this->_review_questions_ids)
				$this->_review_questions_ids = $this->mock_exam_questions->join('progress')->on('progress.fk_question_id', '=', 'mockexamquestion.fk_question_id')->with('section')->where('progress.status', '=', 0)->find_all()->as_array('section_id', 'section_title');
			return $this->_review_questions_ids;
		}elseif($mode == 'take'){
			if (!$this->_retake_questions_ids)
				$this->_retake_questions_ids = $this->mock_exam_questions->with('section')->find_all()->as_array('section_id', 'section_title');
			return $this->_retake_questions_ids;
		}
		return array();
	}*/

	public function get_user_score($user_id, $section_id = false){
		$earned_marks = ORM::factory('Progress')->select('questions.marks')->join('questions')->on('question_id', '=', 'fk_question_id')->where('fk_question_id', 'IN', $this->get_questions_ids('take', $section_id))->where('fk_user_id', '=', $user_id)->where('status', '=', 1)->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($earned_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	public function get_total_score($section_id = false){
		$total_marks = ORM::factory('Question')->where('question_id', 'IN', $this->get_questions_ids('take', $section_id))->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($total_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	/**
	 * Get a list of Tests
	 */
	public function get_mock_exams($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;
	}

}