<?php defined('SYSPATH') or die('No direct script access.');

class Model_MockExamQuestion extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'mock_exams_questions';
	
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_load_with = array('question');
	protected $_primary_key = 'mock_exam_question_id';
	
	protected $_belongs_to = array('question'=>array('model'=>'Question','foreign_key'=>'fk_question_id'));
	/**
	 * Returns a list of questions for a particular test
	 * @param $test_id
	 * @return $questions (array of ORM objects)
	 */
	public function get_mock_questions($mock_exam_id){
		$questions = $this->where('fk_mock_exam_id','=',$mock_exam_id)->find_all();
		return $questions;		
	}
}
?>