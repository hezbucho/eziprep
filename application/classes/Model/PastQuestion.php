<?php defined('SYSPATH') or die('No direct script access.');

class Model_PastQuestion extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'past_questions';

	/**
	 * Model's primary key name
	 * @string
	 */
	//protected $_load_with = array('question');
	protected $_primary_key = 'past_question_id';
	protected $_load_with = array('subject', 'session', 'country', 'level');
	protected $_has_many = array('sections' => array('model' => 'Section', 'foreign_key' => 'fk_past_question_id'), 'question_sets' => array('model' => 'PastQuestionQuestion', 'foreign_key' => 'fk_past_question_id'));
	protected $_belongs_to = array('subject' => array('model' => 'Subject', 'foreign_key' => 'fk_subject_id'), 'session' => array('model' => 'ExamSession', 'foreign_key' => 'fk_exam_session_id'), 'country' => array('model' => 'Country', 'foreign_key' => 'fk_country_id'), 'level' => array('model' => 'EducationLevel', 'foreign_key' => 'fk_education_level_id'));
	/**
	 * Returns a list of questions for a particular test
	 * @param $test_id
	 * @return $questions (array of ORM objects)
	 */
	public function get_past_questions($search_field, $search_value) {
		$table_columns = $this -> _get_table_columns(array($this -> object_name()));
		// make use of default search filtering accross multiple fields
		$this -> _search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here
		return $table_columns;
	}

	public function get_question_set($subject_id) {
		return $this -> where('pastquestion.fk_subject_id', '=', $subject_id) -> find_all();
	}

	public function get_questions_count() {
		if (!$this -> _questions_count)
			$this -> _questions_count = $this -> question_sets -> count_all();
		return $this -> _questions_count;
	}

	public function get_attempted_questions_count() {
		if (!$this -> _attempted_questions_count)
			$this -> _attempted_questions_count = $this -> question_sets -> join('progress') -> on('progress.fk_question_id', '=', 'pastquestionquestion.fk_question_id') -> where('fk_past_question_id', '=', $this -> pk()) -> where('fk_user_id', '=', Auth::instance() -> get_user() -> id) -> count_all();
		return $this -> _attempted_questions_count;
	}

	public function wipe_user_questions_progress($user_id, $section_id = false) {
		$progress_records = ORM::factory('Progress') -> where('fk_question_id', 'IN', $this -> get_questions_ids('take', $section_id)) -> where('fk_user_id', '=', $user_id) -> find_all();
		foreach ($progress_records as $p) {
			$p -> delete();
		}
	}

	public function get_questions_ids($mode, $section_id = false) {
		if (!$section_id) {
			$section_ids = $this -> get_sections_ids();
			reset($section_ids);
			$section_id = key($section_ids);
		}
		if ($mode == 'review' && $section_id) {
			if (!$this -> _review_questions_ids)
				$this -> _review_questions_ids = $this -> question_sets -> join('progress') -> on('progress.fk_question_id', '=', 'pastquestionquestion.fk_question_id') -> where('progress.status', '=', 0) -> where('pastquestionquestion.fk_section_id', '=', $section_id) -> find_all() -> as_array(null, 'fk_question_id');
			return $this -> _review_questions_ids;
		} elseif ($mode == 'take' && $section_id) {
			if (!$this -> _retake_questions_ids)
				$this -> _retake_questions_ids = $this -> question_sets -> where('fk_section_id', '=', $section_id) -> find_all() -> as_array(null, 'fk_question_id');
			return $this -> _retake_questions_ids;
		}
		return array();
	}

	public function get_user_score($user_id, $section_id = false) {
		$earned_marks = ORM::factory('Progress') -> select('questions.marks') -> join('questions') -> on('question_id', '=', 'fk_question_id') -> where('fk_question_id', 'IN', $this -> get_questions_ids('take', $section_id)) -> where('fk_user_id', '=', $user_id) -> where('status', '=', 1) -> find_all() -> as_array(null, 'marks');
		$score = 0;
		foreach ($earned_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}

	public function get_total_score($section_id = false) {
		$total_marks = ORM::factory('Question') -> where('question_id', 'IN', $this -> get_questions_ids('take', $section_id)) -> find_all() -> as_array(null, 'marks');
		$score = 0;
		foreach ($total_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}

	public function get_sections_ids() {
		if (!$this -> _section_ids)
			$this -> _section_ids = $this -> sections -> find_all() -> as_array('section_id', 'section_title');
		return $this -> _section_ids;
	}

}
?>