<?php defined('SYSPATH') or die('No direct script access.');

class Model_PastQuestionQuestion extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'past_question_questions';
	
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_load_with = array('question');
	protected $_primary_key = 'past_question_questions_id';
	
	protected $_belongs_to = array('question'=>array('model'=>'Question','foreign_key'=>'fk_question_id'));
	
	/**
	 * Returns a list of questions for a particular past question set
	 * @param $past_question_id
	 * @return $questions (array of ORM objects)
	 */
	public function get_past_questions($past_question_id){
		
		$questions = $this->where('fk_past_question_id','=',$past_question_id)->find_all();
		return $questions;		
	}
	public function get_subtopic_past_questions(){
		$this->_past_question_type = 'subtopic';
		return $this->find_all();		
	}
}
?>