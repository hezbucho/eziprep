<?php defined('SYSPATH') or die('No direct script access.');

class Model_Progress extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'progress';
	protected $_load_with = array('question','user');
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'progress_id';
	
	protected $_belongs_to = array(
		'user'=>array('model'=>'User','foreign_key'=>'fk_user_id'),
		'question'=>array('model'=>'Question','foreign_key'=>'fk_question_id')
	);
}
?>