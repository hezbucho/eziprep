<?php defined('SYSPATH') or die('No direct script access.');

class Model_Question extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'questions';
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'question_id';
	
	
	protected $_has_many = array(
		'answers'=>array('model'=>'Answer','foreign_key'=>'fk_question_id'),
		//'users_progress'=>array('model'=>'Answer','foreign_key'=>'fk_question_id'),
	);

	
	
	public function answers_list(){
		if (!$this->_answers_list)
			$this->_answers_list = $this->answers->find_all();
		return $this->_answers_list;
	}
	
	public function answers_list_count(){
		return count($this->answers_list());
	}
	
}
?>