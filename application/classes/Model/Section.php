<?php defined('SYSPATH') or die('No direct script access.');
class Model_Section extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'section_id';
	protected $_table_name = 'sections';
	/**
	 * A school has many students
	 *
	 * @var array Relationhips
	 */

	
	protected $_belongs_to = array('mock_exam' => array('model' => 'MockExam', 'foreign_key' => 'fk_mock_exam_id'));
	
	
	/**
	 * Get a list of Educational Levels
	 */
	public function getList($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;
	}
	public function get_section_ids($mock_exam_id){
		return $this->where('fk_mock_exam_id','=',$mock_exam_id)->find_all()->as_array(null, 'section_id');
	}
}