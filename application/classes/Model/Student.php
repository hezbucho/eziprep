<?php defined('SYSPATH') or die('No direct script access.');
class Model_Student extends ORM {
	
	/**
	 * Relationships that should always be joined
	 * @var array
	 */
	protected $_load_with = array('country_info');
	/**
	 * A Student has one set of user credetials
	 * A student belongs to one country
	 *
	 */
	protected $_has_one = array('user_info' => array('model' => 'User', 'foreign_key' => 'id'),
								
								
							);
	
	//Primary Key
	protected $_primary_key = 'fk_user_id';
	protected $_belongs_to = array('country_info' => array('model' => 'Country', 'foreign_key' => 'fk_country_id'));
}