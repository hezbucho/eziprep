<?php defined('SYSPATH') or die('No direct script access.');
class Model_StudyGroup extends Model_Base {

	//Primary Key
	protected $_primary_key = 'study_group_id';
	//Table  name
	protected $_table_name = 'study_groups';
	//Belongs to Relationships
	protected $_belongs_to = array('founder' => array('model' => 'User', 'foreign_key' => 'fk_user_id'));
	//Has Many Relationships
	protected $_has_many = array('members' => array('model' => 'StudyGroupMember', 'foreign_key' => 'fk_study_group_id'));

	/**
	 * Get a list of StudyGroups
	 */
	public function study_groups($search_field, $search_value, $user_id = FALSE) {
		$table_columns = $this -> _get_table_columns(array($this -> object_name()));
		// make use of default search filtering accross multiple fields
		if ($user_id)
			$this -> members -> where('members.fk_user_id', '=', $user_id);
		$this -> _search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here

		return $table_columns;
	}

	public function group_members() {
		return $this -> members -> find_all();
	}

	public function has_capacity() {
		$_has_capacity = 1;
		if ($this -> members -> count_all() >= 6)
			$_has_capacity = 0;
		return $_has_capacity;
	}

	public function get_user_current_group($user_id) {
		$this -> join('study_group_members') -> on('study_group_id', '=', 'fk_study_group_id');
		$this -> where('study_group_members.fk_user_id', '=', $user_id);
		$this -> where('study_group_members.status', '=', 1);
		return $this -> find();
	}

	public function get_group_invites($user_id) {
		$this->select('study_group_member_id','invited_by',array('requester.firstname','fname'),array('requester.lastname','lname'));
		$this -> join('study_group_members') -> on('study_group_id', '=', 'fk_study_group_id');
		$this -> join(array('users','requester')) -> on('id', '=', 'invited_by');
		$this -> where('study_group_members.fk_user_id', '=', $user_id);
		$this -> where('study_group_members.status', '=', 0);
		return $this -> find_all();
	}

	public function no_group_members() {
		$this -> _no_group_members = $this -> members->where('status','=',1) -> count_all();
		return $this -> _no_group_members;
	}
	public function get_no_invites($user_id){
		$invites = ORM::factory('StudyGroupMember') -> where('fk_user_id', '=', $user_id) -> where('status', '=', 0)->count_all();
		return $invites;
		
	}

	public function delete_membership($user_id) {
		$group = ORM::factory('StudyGroupMember') -> where('status', '=', 1) -> where('fk_user_id', '=', $user_id) -> find();
		$model = ORM::factory('StudyGroupMember', $group -> study_group_member_id);
		if($model->loaded())
			$model -> delete();	
		return true;
	}

	public function delete_member($user_id, $group_id) {
		$groups = ORM::factory('StudyGroupMember') -> where('fk_study_group_id', '=', $group_id) -> where('fk_user_id', '=', $user_id) -> find_all();
		foreach ($groups as $group) {
			$model = ORM::factory('StudyGroupMember', $group -> study_group_member_id);
			$model -> delete();
		}
		return true;
	}
	public function delete_members() {
		$members = ORM::factory('StudyGroupMember') -> where('fk_study_group_id', '=', $this->study_group_id) -> find_all();
		foreach ($members as $member) {
			$model = ORM::factory('StudyGroupMember', $member -> study_group_member_id);
			$model -> delete();
		}
		return true;
	}
	public function reject_invite($invite_id){
		$invite = ORM::factory('StudyGroupMember',$invite_id);
		try{
			$invite->delete();
			return true;
		}catch(Exception $e){
			return false;
		}
		
	}

}
