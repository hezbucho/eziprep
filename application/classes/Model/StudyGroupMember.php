<?php defined('SYSPATH') or die('No direct script access.');
class Model_StudyGroupMember extends Model_Base {
	
	
	
	//Primary Key
	protected $_primary_key = 'study_group_member_id';
	//Table  name
	protected $_table_name = 'study_group_members';
	//Belongs to Relationships
	protected $_belongs_to = array('user_details'=>array('model'=>'User','foreign_key'=>'fk_user_id'));
	
	public function check_membership_status($user_id){
		$this->where('fk_user_id','=',$user_id)->count_all();
	}
}