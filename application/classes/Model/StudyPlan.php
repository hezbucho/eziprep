<?php defined('SYSPATH') or die('No direct script access.');
class Model_StudyPlan extends Model_Base {
	
	
	
	//Primary Key
	protected $_primary_key = 'study_plan_id';
	//Table  name
	protected $_table_name = 'study_plans';
	protected $_load_with = array('subtopic');
	protected $_belongs_to = array('subtopic'=>array('model'=>'Subtopic','foreign_key'=>'fk_subtopic_id'));
	
	public function get_study_plan($user_id){
		$this->where('fk_user_id','=',$user_id);
		return $this->find_all();
	}
}