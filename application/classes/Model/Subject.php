<?php defined('SYSPATH') or die('No direct script access.');
class Model_Subject extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'subject_id';
	/**
	 * A school has many students
	 *
	 * @var array Relationhips
	 */
	protected $_has_many = array(
		'topics' => array('model' => 'Lesson','foreign_key' => 'fk_subject_id') 		
	);
	
	/**
	 * Get a list of active subjects
	 */
	public function get_active_subjects(){
		return $this->where('status', '=', 1)->find_all();
	}
	/**
	 * Get a list of Educational Levels
	 */
	public function getList($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here
		
		return $table_columns;
	}
	public function get_topics(){
		return $this->topics->find_all();
	}
	public static function get_subject_by_name($string){
		return ORM::factory('Subject')->where('subject_title','=',$string)->find()->subject_id;
	}
}