<?php defined('SYSPATH') or die('No direct script access.');
class Model_Subtopic extends Model_Base {
	
	//Primary Key
	protected $_primary_key = 'subtopic_id';
	/**
	 * A school has many students
	 *
	 * @var array Relationhips
	 */
	protected $_has_many = array(
		'trial_questions' => array('model' => 'TrialQuestion','foreign_key' => 'fk_subtopic_id'),
		'past_questions' => array('model' => 'SubtopicPastQuestion','foreign_key' => 'fk_subtopic_id'),
	);
	
	protected $_belongs_to = array('lesson' => array('model' => 'Lesson', 'foreign_key' => 'fk_lesson_id'));
	
	public function get_trial_questions_count(){
		if (!$this->_trial_questions_count)
			$this->_trial_questions_count = $this->trial_questions->count_all();
		return $this->_trial_questions_count;
	}
	
	public function get_trial_questions_user_progress(){
		if (!$this->_trial_questions_user_progress)
			$this->_trial_questions_user_progress = $this->trial_questions->select('')->join('progress')->on('progress.fk_question_id', '=', 'trialquestion.fk_question_id')->where('fk_subtopic_id', '=', $this->pk())->where('fk_user_id', '=', Auth::instance()->get_user()->id)->count_all();
		return $this->_trial_questions_user_progress;
	}
	
	public function get_past_questions_count(){
		return 0;
	}
	
	public function get_past_questions_user_progress(){
		return 0;
	}
	
	public function wipe_user_questions_progress($user_id, $mode){
		$progress_records = ORM::factory('Progress')->where('fk_question_id', 'IN', $this->{'get_' . $mode .'_questions_ids'}())->where('fk_user_id', '=', $user_id)->find_all();
		foreach ($progress_records as $p) {
			$p->delete();
		}
	}
	
	public function get_user_score($user_id, $mode){
		$earned_marks = ORM::factory('Progress')->select('questions.marks')->join('questions')->on('question_id', '=', 'fk_question_id')->where('fk_question_id', 'IN', $this->{'get_' . $mode .'_questions_ids'}())->where('fk_user_id', '=', $user_id)->where('status', '=', 1)->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($earned_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	public function get_total_score($mode){
		$total_marks = ORM::factory('Question')->where('question_id', 'IN', $this->{'get_' . $mode .'_questions_ids'}())->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($total_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	public function get_trial_questions_ids(){
		if (!$this->_trial_questions_ids)
			$this->_trial_questions_ids = $this->trial_questions->find_all()->as_array(null, 'fk_question_id');
		return $this->_trial_questions_ids;
	}
	
	public function get_past_questions_ids(){
		return array(1,2,3,4);
	}
	/**
	 * Get a list of Educational Levels
	 */
	public function getList($search_field, $search_value){
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here
		
		return $table_columns;
	}
	
}