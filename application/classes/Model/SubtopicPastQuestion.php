<?php defined('SYSPATH') or die('No direct script access.');

class Model_SubtopicPastQuestion extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'subtopics_past_questions';
	protected $_load_with = array('question','subtopic');
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'subtopic_past_question_id';
	
	protected $_belongs_to = array(
		'subtopic'=>array('model'=>'Subtopic','foreign_key'=>'fk_subtopic_id'),
		//'question'=>array('model'=>'Question','foreign_key'=>'fk_question_id'),
		'question'=>array('model'=>'Question','foreign_key'=>'fk_question_id'),
	);
	//protected $_has_one = array('question'=>array('model'=>'Question','foreign_key'=>'question_id'));
	public function get_past_questions($subtopic_id){
		if($subtopic_id)
			$this->where('fk_subtopic_id','=',$subtopic_id);
		return $this->find_all();
	}
}
?>