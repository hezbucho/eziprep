<?php defined('SYSPATH') or die('No direct script access.');
class Model_Test extends Model_Base {
		
	//Primary Key
	protected $_primary_key = 'test_id';
	//Table  name
	protected $_table_name = 'tests';
	protected $_load_with = array('lesson');
	protected $_belongs_to = array(		
		'lesson'=>array('model'=>'Lesson','foreign_key'=>'fk_lesson_id'),
	);
	protected $_has_many = array('test_questions'=>array('model'=>'TestQuestion','foreign_key'=>'fk_test_id'));
	
	/**
	 * Setup validation rules
	 *
	 * @return array
	 */
	public function rules() {
		// TODO: See if there are any rules to be added
		return array(
			'test_title' => array(array('not_empty')),
			'time' => array(array('not_empty')),
		);
	}
	public function get_questions_count(){
		if (!$this->_questions_count)
			$this->_questions_count = $this->test_questions->count_all();
		return $this->_questions_count;
	}
	
	public function get_lesson_tests($subject_id){
		return $this->where('lesson.fk_subject_id', '=', $subject_id)->find_all();
	}
	
	public function get_attempted_questions_count(){
		if (!$this->_attempted_questions_count)
			$this->_attempted_questions_count = $this->test_questions->join('progress')->on('progress.fk_question_id', '=', 'testquestion.fk_question_id')->where('fk_test_id', '=', $this->pk())->where('fk_user_id', '=', Auth::instance()->get_user()->id)->count_all();
		return $this->_attempted_questions_count;
	}

	public function wipe_user_questions_progress($user_id){
		$progress_records = ORM::factory('Progress')->where('fk_question_id', 'IN', $this->get_questions_ids('take'))->where('fk_user_id', '=', $user_id)->find_all();
		foreach ($progress_records as $p) {
			$p->delete();
		}
	}

	public function get_questions_ids($mode){
		if ($mode == 'review'){
			if (!$this->_review_questions_ids)
				$this->_review_questions_ids = $this->test_questions->join('progress')->on('progress.fk_question_id', '=', 'testquestion.fk_question_id')->where('progress.status', '=', 0)->find_all()->as_array(null, 'fk_question_id');
			return $this->_review_questions_ids;
		}elseif($mode == 'take'){
			if (!$this->_retake_questions_ids)
				$this->_retake_questions_ids = $this->test_questions->find_all()->as_array(null, 'fk_question_id');
			return $this->_retake_questions_ids;
		}
		return array();
	}
	
	public function get_user_score($user_id){
		$earned_marks = ORM::factory('Progress')->select('questions.marks')->join('questions')->on('question_id', '=', 'fk_question_id')->where('fk_question_id', 'IN', $this->get_questions_ids('take'))->where('fk_user_id', '=', $user_id)->where('status', '=', 1)->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($earned_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	public function get_total_score(){
		$total_marks = ORM::factory('Question')->where('question_id', 'IN', $this->get_questions_ids('take'))->find_all()->as_array(null, 'marks');
		$score = 0;
		foreach ($total_marks as $mark) {
			$score += $mark;
		}
		return $score;
	}
	
	/**
	 * Get a list of Tests
	 */
	public function get_tests($search_field, $search_value){
		//$table_columns = $this->_get_table_columns(array($this->object_name()));
		$table_columns = $this->_get_table_columns(array($this->object_name()));
		// make use of default search filtering accross multiple fields 
		$this->_search_list($search_field, $search_value, $table_columns);
		// and/or perform other custom logic here		
		return $table_columns;
	}
	

}