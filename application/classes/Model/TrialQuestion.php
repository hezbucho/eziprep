<?php defined('SYSPATH') or die('No direct script access.');

class Model_TrialQuestion extends Model_Base {
	/**
	 * Model's table
	 * @string
	 */
	protected $_table_name = 'subtopics_trial_questions';
	protected $_load_with = array('question','subtopic');
	
	/**
	 * Model's primary key name
	 * @string
	 */
	protected $_primary_key = 'subtopic_trial_question_id';
	

	protected $_belongs_to = array(
		'subtopic'=>array('model'=>'Subtopic','foreign_key'=>'fk_subtopic_id'),
		'question'=>array('model'=>'Question','foreign_key'=>'fk_question_id')
	);

}
?>