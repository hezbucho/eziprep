<?php defined('SYSPATH') or die('No direct script access.');
class Model_User extends Model_Auth_User {
	
	/**
	 * Relationships that should always be joined
	 * @var array
	 */
	protected $_load_with = array('student_info','school_info');
	
	/**
	 * A user has many tokens and roles
	 *
	 * @var array Relationhips
	 */
	protected $_has_many = array(
		'user_tokens' => array('model' => 'User_Token'),
		'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
		'followers' => array('model' => 'Friend', 'foreign_key' => 'fk_user_two'),
		// 'requested_friendships' => array('model' => 'Friend', 'foreign_key' => 'fk_user_two'),
	);
	/**
	 * A User may be a student
	 *
	 */
	protected $_has_one = array(
	'student_info' => array('model' => 'Student', 'foreign_key' => 'fk_user_id'),
	
	);
	/**
	 * S user may belong to a school
	 */
	protected $_belongs_to = array('school_info' => array('model' => 'School', 'foreign_key' => 'fk_school_id'));
	
	/**
	 * Check if the user has the specified permission.
	 *
	 * @since 1.0
	 * @param int|ACL_Model_Permission $permission
	 * @return bool
	 */
	/**
	 * Rules for the user model. Because the password is _always_ a hash
	 * when it's set,you need to run an additional not_empty rule in your controller
	 * to make sure you didn't hash an empty string. The password rules
	 * should be enforced outside the model or with a model helper method.
	 *
	 * @return array Rules
	 */
	public function rules()
	{
		return array(
			'username' => array(
				array('not_empty'),
				array('max_length', array(':value', 32)),
				array(array($this, 'unique'), array('username', ':value')),
			),
			'password' => array(
				array('not_empty'),
			),
			'email' => array(
				//array('not_empty'),
				array('email'),
				//array(array($this, 'unique'), array('email', ':value')),
			),
		);
	}
	
	public function can($permission)
	{
		return $this->_check_permission($permission);
	}

	/**
	 * Check if the user has all the specified permissions.
	 *
	 * @since 1.0
	 * @param array $permissions An array of ACL_Model_Permission or Permission PK-s
	 * @return bool True if the user has all the specified permissions
	 */
	public function has_permissions(array $permissions)
	{
		foreach ($permissions as $permission) {
			if (! $this->_check_permission($permission)) {
				return FALSE;
			}
		}
		return TRUE;
	}

	/**
	 * Check if the user has at least one of the specified permissions.
	 * @since 1.0
	 * @param array $permissions An array of ACL_Model_Permission or Permission PK-s
	 * @return bool True if the user had one of the permissions
	 */
	public function has_any_permission(array $permissions)
	{
		foreach ($permissions as $permission) {
			if ($this->_check_permission($permission)) {
				return TRUE;
			}
		}
		return FALSE;
	}

	/**
	 * Check if a user has the specified permission.
	 *
	 * @param int $permission
	 * @throws InvalidArgumentException
	 * @see Model_Permission
	 * @return bool
	 * @since 2.0
	 */
	private function _check_permission($permission)
	{
		if (! is_int($permission) && ! $permission instanceof ACL_Model_Permission) {
			throw new InvalidArgumentException('Expected an instance of ACL_Model_Permission or an integer.');
		}

		// Todo: Do this with one DB::select query
		foreach ($this->roles->find_all() as $role) {
			if ($role->can($permission)) {
				return TRUE;
			}
		}
		return FALSE;
	}
	/**
	 * Get a list of Users
	 */
	public function getList($search_field, $search_value){
		// TODO: Ensure all columns to be returned in the final resultset are in this list e.g. merge all referenced/joined models columns
		$student_columns = ORM::factory('Student')->list_columns();
		$school_columns = ORM::factory('School')->list_columns();
		$user_columns = ORM::$_column_cache[$this->object_name()];//ORM('User')->list_columns();
		$columns=array_merge_recursive($student_columns,$school_columns,$user_columns);
		$table_columns=$columns;
		//var_dump($columns);exit;
		$_final_table_columns = $table_columns;
		if (is_string($search_field) && strlen($search_field) > 0)
			$search_field = array($search_field);
		if ($search_field && $search_value){
			$pass = 1;
			foreach ($search_field as $field) {
				if (strtolower($field) == 'id')
					$field = $this->primary_key();
				$comparator = ($table_columns[$field]['type'] == 'string') ? 'LIKE' : '=';
				$search_value = ($table_columns[$field]['type'] == 'string') ? '%' . $search_value . '%' : $search_value;
				$search_value = (strpos($table_columns[$field]['type'], 'int') !== false) ? intval($search_value) : $search_value;
				if (array_key_exists($field, $table_columns) && $pass > 1)
					$this->or_where($field, $comparator, $search_value);
				elseif (array_key_exists($field, $table_columns) && $pass == 1)
					$this->where($field, $comparator, $search_value);
				//elseif (array_key_exists($search_field, $this->_api_to_db_map) && array_key_exists($this->_api_to_db_map[$search_field], $table_columns))
					//$model->where($this->_api_to_db_map[$search_field], $comparator, $search_value);
				$pass++;
			}
		}
		
		return $_final_table_columns;
	}
	
}