<?php defined('SYSPATH') or die('No direct script access.');
/**
 * Site template viewmodel
 *
 * @version 01 - Hezron Obuchele 2013-01-03
 *
 * PHP version 5
 * LICENSE: Not for reuse or modification without the express 
 * written authorization from BeeBuy Investments Ltd.
 *
 * View_SiteLayout
 * @author     Hezron Obuchele
 * @package    M-awaidha
 * @category   ViewModels
 * @copyright  (c) 2013 BeeBuy Investments Ltd. - http://www.beebuy.biz
 */
 
class View_AdminLayout extends ViewModel {
	
	public function page_title()
	{
		return 'Admin Layout';
	}
	
	public function message()
	{
		return $this->user_msg;
	}
}