<?php defined('SYSPATH' OR die('No direct access allowed.'));
/**
 * Example config for Twitter Bootstrap main navbar menu
 *
 * @see https://github.com/anroots/kohana-menu/wiki/Configuration-files
 * @author Ando Roots <ando@sqroot.eu>
 */
return array(
	'guess_active_item' => true,
	'items'             => array(
		
		array(
			'url'     => 'admin/lessons',
			'title'   => 'nav.site.lessons',
			'icon'    => 'icon-book icon-large',
			'tooltip' => 'nav.site.lessons.tooltip',
			//'section' => 'Materials' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'admin/tests',
			'title'   => 'nav.site.tests',
			'icon'    => 'icon-time icon-large',
			'tooltip' => 'nav.site.tests.tooltip',
			//'visible' => false
		),
		array(
			'url'     => 'admin/pastquestions',
			'title'   => 'nav.site.pastquestions',
			'icon'    => 'icon-chevron-right',
			'tooltip' => 'nav.site.pastquestions.tooltip',
			//'visible' => false
		),
		array(
			'url'     => 'admin/mockexams',
			'title'   => 'nav.site.mockexams',
			'icon'    => 'icon-edit icon-large',
			'tooltip' => 'nav.site.mockexams.tooltip',
		),
		array(
			'url'     => 'admin/announcements',
			'title'   => 'nav.site.announcements',
			'icon'    => 'icon-bullhorn icon-large',
			'tooltip' => 'nav.site.announcements.tooltip',
		),
		array(
			'url'     => '#',
			'title'   => 'nav.admin.settings',
			'icon'    => 'icon-cog icon-large',
			'tooltip' => 'nav.admin.settings.tooltip',
			'items'	  =>array(
							array(
								'url'     => 'admin/schools',
								'title'   => 'nav.admin.schools',
								'icon'    => 'icon-hand-right icon-large',
								'tooltip' => 'nav.admin.schools.tooltip',
							),
							array(
								'url'     => 'admin/examsessions',
								'title'   => 'nav.admin.examsessions',
								'icon'    => 'icon-hand-right icon-large',
								'tooltip' => 'nav.admin.examsessions.tooltip',
							),
							array(
								'url'     => 'admin/countries',
								'title'   => 'nav.admin.countries',
								'icon'    => ' icon-hand-right icon-large',
								'tooltip' => 'nav.admin.countries.tooltip',
							),
							
							array(
								'url'     => 'admin/subjects',
								'title'   => 'nav.admin.subjects',
								'icon'    => 'icon-hand-right icon-large',
								'tooltip' => 'nav.admin.subjects.tooltip',
							),
							array(
								'url'     => 'admin/educationlevels',
								'title'   => 'nav.admin.educationlevels',
								'icon'    => 'icon-hand-right icon-large',
								'tooltip' => 'nav.admin.educationlevels.tooltip',
							),			
						)
		),		
		
		array(
			'url'     => 'resources',
			'title'   => 'nav.site.resources',
			'icon'    => 'icon-bookmark icon-large',
			'tooltip' => 'nav.site.resources.tooltip',
		),
		array(
			'url'     => 'reports',
			'title'   => 'nav.site.reports',
			'icon'    => 'icon-file icon-large',
			'tooltip' => 'nav.site.reports.tooltip',
		)
		
	),
);