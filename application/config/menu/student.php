<?php defined('SYSPATH' OR die('No direct access allowed.'));
/**
 * Example config for Twitter Bootstrap main navbar menu
 *
 * @see https://github.com/anroots/kohana-menu/wiki/Configuration-files
 * @author Ando Roots <ando@sqroot.eu>
 */
return array(
	'guess_active_item' => true,
	'items'             => array(
		array(
			'url'     => 'home',
			'title'   => 'nav.site.studenthome',
			'icon'    => 'icon-home icon-large',
			'tooltip' => 'nav.site.studenthome.tooltip',
			//'section' => 'General' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
			array(
			'url'     => 'lessons',
			'title'   => 'nav.site.lessons',
			'icon'    => 'icon-book icon-large',
			'tooltip' => 'nav.site.lessons.tooltip',
			'section' => 'Academics' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'tests',
			'title'   => 'nav.site.tests',
			'icon'    => 'icon-time icon-large',
			'tooltip' => 'nav.site.tests.tooltip',
		),
		array(
			'url'     => 'pastquestions',
			'title'   => 'nav.site.pastquestions',
			'icon'    => 'icon-chevron-right',
			'tooltip' => 'nav.site.pastquestions.tooltip',
			//'visible' => false
		),
		array(
			'url'     => 'mockexams',
			'title'   => 'nav.site.mockexams',
			'icon'    => 'icon-edit icon-large',
			'tooltip' => 'nav.site.mockexams.tooltip',
		),
		
		array(
			'url'     => 'studyplans',
			'title'   => 'nav.site.studyplans',
			'icon'    => 'icon-calendar icon-large',
			'tooltip' => 'nav.site.studyplans.tooltip',
		),
		array(
			'url'     => 'resources',
			'title'   => 'nav.site.resources',
			'icon'    => 'icon-bookmark icon-large',
			'tooltip' => 'nav.site.resources.tooltip',
		),
		array(
			'url'     => 'messages',
			'title'   => 'nav.site.messages',
			'icon'    => 'icon-inbox icon-large',
			'tooltip' => 'nav.site.messages.tooltip',
			'section' => 'Social' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'studygroups',
			'title'   => 'nav.site.studygroups',
			'icon'    => 'icon-group icon-large',
			'tooltip' => 'nav.site.studygroups.tooltip',
			//'section' => 'Miscellaneous' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'friends',
			'title'   => 'nav.site.friends.list',
			'icon'    => 'icon-user icon-large',
			'tooltip' => 'nav.site.friends.list.tooltip',
		),
		array(
			'url'     => 'announcements',
			'title'   => 'nav.site.announcements',
			'icon'    => 'icon-bullhorn icon-large',
			'tooltip' => 'nav.site.announcements.tooltip',
			'section' => 'Multimedia' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'games',
			'title'   => 'nav.site.games',
			'icon'    => 'icon-user icon-large',
			'tooltip' => 'nav.site.games.tooltip',
		),
	
		array(
			'url'     => 'reports',
			'title'   => 'nav.site.reports',
			'icon'    => 'icon-file icon-large',
			'tooltip' => 'nav.site.reports.tooltip',
			'section' => 'Others' // All subsequent menu items will be under this section until the next item that has this property and so on
		),
		array(
			'url'     => 'questionsearch',
			'title'   => 'nav.site.questionsearch',
			'icon'    => 'icon-search icon-large',
			'tooltip' => 'nav.site.questionsearch.tooltip',
		),
	),
);