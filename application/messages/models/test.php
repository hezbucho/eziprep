<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'test_title' => array(
		'not_empty' => 'You must provide a test title',
	),
	'time' => array(
		'not_empty' => 'The test must be timed',
	),
);

?>