<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'username' => array(
		'not_empty' => 'Not empty',
		'max_length' => 'Max Length',
		'unique' => 'Not Unique'
	),
	'password' => array(
		'not_empty' => 'Not empty',
	),
	'email' => array(
		'email' => 'Not email',
	),
);

?>