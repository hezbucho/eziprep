/* ========================================================
 * admin.js v0.0.1
 * Author: Joseph Bosire
 * Email: kashboss@gmail.com
 * ======================================================== */
/* ENABLE RICH TEXT EDITOR PLUGIN
 * ============================== */
//$('.html-editor').wysihtml5();
 $(document).bind("ajaxComplete", function(){
  	tinymce.init({
    selector: ".html-editor",
    plugins: [
        "jbimages advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    relative_urls: false,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
});
tinymce.init({
    selector: ".answer-editor",
    plugins: [
        "jbimages advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    menubar:false,
    relative_urls: false,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
});
 });
$(document).ready(function(e){
	tinymce.init({
    selector: ".html-editor",
    plugins: [
        "jbimages advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    relative_urls: false,
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | jbimages"
});
});

/* STYLE MODAL WINDOWS
 * =================== */   
$('.modal').css({
  width: '55%',
  left:'43%'  
});
/* SWITCH SAVE FOR DIFFERENT QUESTION TYPES
 * ======================================== */
$('#save-question').click(function (e) {
	e.preventDefault();
	if($('#question-type').val() == 'trial')
		saveTrialQuestion();
	else if($('#question-type').val() == 'test')
		saveTestQuestion();
	else if($('#question-type').val() == 'mock')
		saveMockQuestion();
	else if($('#question-type').val() == 'past')
		savePastQuestion();
});
/* EDIT QUESTION FUNCTION FOR REUSE
 * ================================ */
function editApiQuestion(url,data,callback){
	$('#form-question').hide();
	$('.loader').show();
	$('#edit-question').modal('show');
	$.ajax({
	  type: "GET",
	  url: encodeURI(this.url),
	  data: this.data
	}).done(function( response ) {
		$('#form-question').show();
		$('#edit-question .form-holder').html(response);		
		callback();
		$('.loader').hide();			  
	});	
}
/* GET QUESTION LIST FUNCTION FOR REUSE
 * ==================================== */
function getApiQuestions(url,data,callback){
	$.ajax({
	  type: "GET",
	  url: encodeURI(this.url),
	  data:this.data
	  }).done(function( response ) {
		callback(response);
		$('.loader').hide();
	});
}
/* SAVE QUESTION FUNCTION FOR REUSE
 * ================================ */
function saveApiQuestion(url,callback){
	tinyMCE.triggerSave();
	 $.ajax({
	  type: "POST",
	  url: encodeURI(this.url),
	  dataType:'json',
	  data: $('#form-question').serialize()
	}).done(function( response ) {
		$('#edit-question').modal('hide');
		callback();		
	});
}
/* EDIT AN ANNOUNCEMENT
 * ================================ */
function editAnnouncement(id){
	$('#form-announcement').hide();
	$('.loader').show();
	$('#edit-announcement').modal('show');
	$.ajax({
	  type: "GET",
	  url: encodeURI("admin/announcements/edit"),
	  data: {announcement_id:id}
	}).done(function( response ) {
		$('#form-announcement').show();
		$('#edit-announcement .form-holder').html(response);
		$('.loader').hide();			  
	});	
}
/* SAVE AN ANNOUNCEMENT
 * ==================== */
function saveAnnouncement(){
	 $.ajax({
	  type: "POST",
	  url: encodeURI("admin/announcements/edit"),
	  dataType:'json',
	  data: $('#form-announcement').serialize()
	}).done(function( response ) {
		$('#edit-announcement').modal('hide');
		location.reload();
	});
}
/* LISTEN FOR SAVE ANNOUNCEMEMT CHANGES BUTTON CLICK
 * ====================================*/
$('#save-announcement').click(function (e) {
	e.preventDefault();
	saveAnnouncement();
});
/* APPLY GENERAL FITLERS TO BACKEND LISTS
 * ====================================== */
function applyFilter(url, callback) {
	$.post(url, {
		fk_country_id : $('select#fk_country_id').val(),
		fk_education_level_id : $('select#fk_education_level_id').val(),
		fk_school_id : $('select#fk_school_id').val(),
		fk_section_id : $('select#fk_section_id').val(),
		fk_subject_id : $('select#fk_subject_id').val()
	}, function(response){
		callback(response);		
	});
}
/* EVENT LISTENER FOR GENERAL FILTERS
 * ====================================== */
$(function() {
	$('body').on('change', 'select#fk_country_id,select#fk_education_level_id,select#fk_subject_id,select#fk_school_id', function(e) {
		applyFilter(window.location.href,function(response){
			$('tbody').html(response);
			console.log('Filter applied successfully');
		})
	});
});
/* EVENT HANDLER FOR SECTION FILTER
 * ====================================== */
$(function() {
	$('body').on('change', 'select#fk_section_id', function(e) {
		getMockQuestions($('select#fk_section_id').val());
	});
});
/* DRAG DROP TABLE ODERING
 * ====================================== */

$(function() {
	$("#table-mock-exam-questions tbody").sortable({
			update: function(event, ui) {
				var questionOrder = $(this).sortable('toArray');
				$.post('admin/mockexams/order_questions',{order:questionOrder},function(response){
					getMockQuestions();
					console.log(response);
				},'json');
				console.log(questionOrder);//$.get('update-sort.cfm', {fruitOrder:fruitOrder});
			}
		});
	$("#table-mock-exam-questions tbody").disableSelection();
}); 
/* TOGGLE PUBLISH STATES
 * ====================================== */
function publish(resourceUrl,formId,callback){
	console.log(resourceUrl+'->'+formId+'Object::::'+$('#'+formId).serialize());
	 $.post(resourceUrl+'/publish',$('#'+formId).serialize(),function(response){
		 console.log(response);
		 if(typeof(callback)!=='undefined')
		 	callback;
		 else
		 	location.reload();
	},'json');
}
function unPublish(resourceUrl,formId,callback){
	console.log(resourceUrl+'->'+formId+'Object::::'+$('#'+formId).serialize());
	 $.post(resourceUrl+'/unpublish',$('#'+formId).serialize(),function(response){
		 console.log(response);
		  if(typeof(callback)!=='undefined')
		 	callback;
		 else
		 	location.reload();
	},'json');
}

/* LISTEN QUESTION TYPE SELECT
 * ====================================*/
$('body').on('change', 'select#question_type', function(e) {
	if($('#question_type').val()=="1"){
		$('#option1,#option2,#option3,#option4,#labelOption1,#labelOption2,#labelOption3,#labelOption4').show();
	}
	else if($('#question_type').val()=="2"){
		$('#option2,#option3,#option4,#labelOption2,#labelOption3,#labelOption4').hide();
	}
	else if($('#question_type').val()=="3"){
		$('#option1,#option2,#option3,#option4,#labelOption1,#labelOption2,#labelOption3,#labelOption4').hide();
	}
});