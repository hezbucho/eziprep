/* ========================================================
 * announcements.js v0.0.1
 * Author: Joseph Bosire
 * Email: kashboss@gmail.com
 * ======================================================== */

/* TOGGLE SELECT ALL
 * ======================================== */
$('body').on('change', "#checkAll", function(e) {
	$("tbody").find(':checkbox').prop("checked", this.checked);
});
/* DELETE SELECTED ANNOUNCEMENTS
 * ================================ */''
$('body').on('change', 'select#batch-operation', function(e) {
	var checkboxes = $(' tbody input[type=checkbox]:checked').serializeArray();
	if ($('select#batch-operation').val() == 'delete') {
		console.log($('#announcement-list').serialize())
		batchDelete('admin/announcements/delete', $('#announcement-list'));
	}

});

function batchDelete(url, form) {
	$.post(url, form.serialize(), function(response) {
		if (response.msg.type == 'success'){}
			removeTableRows(form.serializeArray());//location.reload();
		console.log('Delete Successful')
	}, 'json');
}

function singleDelete(resource) {
	$.post(location.href + '/delete', {
		id : resource
	}, function(response) {
		if (response.msg.type == 'success')
			removeTableRows(resource);//location.reload();
		console.log('Delete Successful')
	}, 'json');
}

function removeTableRows(ids) {
	if ($.isArray(ids)) {
		console.log(ids);		
		$.each(ids, function(index, value) {
			$('#row-'+value.value).remove();
		});
	}else{
		$('#row-'+ids).remove();
	}

}
