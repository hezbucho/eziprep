function saveCountry(){
	 event.preventDefault();
	 $.ajax({
	  type: "POST",
	  url: "admin/countries/edit",
	  dataType:'json',
	  data: $('#form-country').serialize()
	}).done(function( response ) {
		console.log('success');
		$('#edit-country').modal('hide');
		location.reload();
	  //alert( "Success: " + msg );
	});//event.preventDefault();
}
function editCountry(id){
	event.preventDefault();
	$('#form-country').hide();
	$('.ajax-progress').show();
	$('#edit-country').modal('show');
	$.ajax({
	  type: "GET",
	  url: "admin/countries/edit",
	  dataType:'json',
	  data: {country_id:id}
	}).done(function( response ) {
		country_process_response(response.data);
		$('#form-country').show();
		$('.ajax-progress').hide();
		console.log('success')
	  //alert( "Success: " + msg );
	});
	
	
}
function country_process_response(response) {
    var frm = document.getElementById("form-country");
    var i;
 
    console.dir(response);      // for debug
 
    for (i in response) {
        if (i in frm.elements) {
            frm.elements[i].value = response[i];
        }
    }
}