/* ========================================================
 * examsessions.js v0.0.1
 * Author: Joseph Bosire
 * Email: kashboss@gmail.com
 * ======================================================== */
 
/* RETIEVE AN EXAM SESSION FOR EDITING
 * =================================== */
function editSession(id){
	$('#form-session').hide();
	$('.loader').show();
	$('#edit-session').modal('show');
	$.ajax({
	  type: "GET",
	  url: encodeURI("admin/examsessions/edit"),
	  data: {exam_session_id:id}
	}).done(function( response ) {
		$('#form-session').show();
		$('#edit-session .form-holder').html(response);
		$('.loader').hide();			  
	});	
}
/* SAVE AN EXAM SESSION
 * ==================== */
function saveSession(){
	 $.ajax({
	  type: "POST",
	  url: encodeURI("admin/examsessions/edit"),
	  dataType:'json',
	  data: $('#form-session').serialize()
	}).done(function( response ) {
		$('#edit-session').modal('hide');
		location.reload();
	});
}
/* LISTEN FOR SAVE CHANGES BUTTON CLICK
 * ====================================*/
$('#save-session').click(function (e) {
	e.preventDefault();
	saveSession();
});
 