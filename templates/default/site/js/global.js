$('div.expander').expander({
	slicePoint : 300
});
var eventsJson;
$(function() {
	$('[rel="popover-friend"]').popover({
		placement : 'left',
		html : true,
		trigger : 'hover',
		delay : {
			show : 100,
			hide : 2500
		}
	});
});
//Registration Scripts
$('#password_confirm').change(function(){
	if($('#password_confirm').val()==$('#password').val()){
		console.log('here');
		$('.match-right').attr('style','display:block');
		$('.match-wrong').attr('style','display:none');
	}else{
		$('.match-right').attr('style','display:none');
		$('.match-wrong').attr('style','display:block');
	}
});

//Group Invite
function groupInvite(userID, groupID) {
	$('#form-group-invite').hide();
	$('.loader').show();
	$('#group-invite').modal('show');
	$.get('studygroups/invite', {
		user : userID,
		group : groupID
	}, function(response) {
		$('#form-group-invite').show();
		$('#group-invite .form-holder').html(response);
		if ($('#hasCapacity').val() == 0) {
			$('.modal-footer').attr('style', 'display:none');
		}
		$('.loader').hide();
	});
}

function joinGroup(groupID) {
	$('#form-group-invite').hide();
	$('.loader').show();
	$('#group-invite').modal('show');
	$.get('studygroups/join', {
		group : groupID
	}, function(response) {
		$('#form-group-invite').show();
		$('#group-invite .form-holder').html(response);
		$('.loader').hide();
	});
}

function editGroup(id) {
	$('#form-group').hide();
	if ( typeof (id) === 'undefined') {
		id = '';
		$.confirm({
			text : "Creating a group will automatically remove you from any group you may have joined. Are you sure you want to continue?",
			confirm : function(button) {
				processGroupForm(id);
			},
			cancel : function(button) {				
			},
			confirmButton : "Yes I am",
			cancelButton : "No"
		});
	}else{
		processGroupForm(id);
	}
}
function evict(user,group) {
	addConfirmDialog("Are you sure you want to evict this user from the group",function(){
		location = "studygroups/remove/"+user+"?group="+group;
	})
}	

function addConfirmDialog(msg,callback){
	 $.confirm({
			text : msg,
			confirm : callback,
			cancel : function(button) {
				// do something
			},
			confirmButton : "Yes",
			cancelButton : "No"
		});
}
function processGroupForm(id){
	$('.loader').show();
	$('#edit-group').modal('show');
	$.get('studygroups/edit', {
		group : id
	}, function(response) {
		$('#form-group').show();
		$('#edit-group .form-holder').html(response);
		$('.loader').hide();
	});
}

$('body').on('click', '#save-group', function(e) {
	$.post('studygroups/edit', $('#form-group').serialize(), function(response) {
		console.log(response);
		$('#edit-group').hide();
		location.reload();
	}, 'json');
});
$('body').on('click', '#save-group-invite', function(e) {
	$.post('studygroups/join_group', $('#form-group-invite').serialize(), function(response) {
		console.log(response);
		$('#group-invite').hide();
		location.reload();
	}, 'json');
});

/* FILTERING
 * ==================== */
$(function() {
	$('body').on('change', 'select#subject', function(e) {
		$.getJSON("studyplans/get_topics", {
			subject_id : $('select#subject').val()
		}, function(j) {
			var options = '';
			for (var i = 0; i < j.data.length; i++) {
				options += '<option value="' + j.data[i].id + '">' + j.data[i].title + '</option>';
			}
			$("select#topic").removeAttr('disabled');
			$("select#topic").html(options);
		});
	});
});
$(function() {
	$('body').on('click', 'select#topic', function(e) {
		$.getJSON("studyplans/get_subtopics", {
			lesson_id : $('select#topic').val()
		}, function(j) {
			var options = '';
			for (var i = 0; i < j.data.length; i++) {
				options += '<option value="' + j.data[i].id + '">' + j.data[i].title + '</option>';
			}
			$("select#subtopic").removeAttr('disabled');
			$("select#subtopic").html(options);
		});
	});
});

$('#find-friend').click(function() {
	$.get('studygroups/find_friends', $('#add-friend').serialize(), function(response) {
		$('#group-members').html(response);
		console.log(response);
	});
});

$(function() {
	$.fn.modalmanager.defaults.resize = true;
});

function uniqueId() {
	return Date.now();
};

var $ajaxModal = $('#ajax-modal');
var defaultModalWidth = $ajaxModal.data('width');

var showAjaxModal = function(url, width, child) {
	width = width || defaultModalWidth;
	$ajaxModal.data('width', width);
	// create the backdrop and wait for next modal to be triggered
	if (!child)
		$('body').modalmanager('loading');
	else
		$ajaxModal.modal('loading');
	if (url.search('\\?') > -1)
		url += '&uid=' + uniqueId();
	else
		url += '?uid=' + uniqueId();
	$ajaxModal.load(url, '', function() {
		$ajaxModal.modal();
	});
}

$('body').on('click', '.ajax-modal', function(e) {
	var width = $(this).data('width');
	console.log(width);
	var url = $(this).data('href');
	showAjaxModal(url, width);
	e.preventDefault();
});

var testtemplate = Handlebars.compile($('#friends-list-item-template').html());
var latestmsgtemplate = Handlebars.compile($('#latest-messages-list-item-template').html());
var appendFriend = function(friend) {
	$('.friends-list-item').last().after(testtemplate(friend));
	$('.friends-list-item').last().popover({
		placement : 'left',
		html : true,
		trigger : 'hover',
		delay : {
			show : 100,
			hide : 2500
		}
	});
}
function accept_invite(id) {
	$.ajax({
		type : "GET",
		url : "friends/accept",
		dataType : 'json',
		data : {
			user_id : id
		}
	}).done(function(response) {
		$('#notification-' + id).fadeOut(300, function() {
			$(this).remove();
		});
		appendFriend(response.data);
		//alert( "Success: " + msg );
	});
}

function deny_invite(id) {
	$.ajax({
		url : 'friends/deny/',
		type : "GET",
		dataType : 'json',
		data : {
			user_id : id
		}
	}).done(function(msg) {
		$('#notification-' + id).fadeOut(300, function() {
			$(this).remove();
		});
		//alert( "Success: " + msg );
	});
}

function get_question() {
	name = $('#recovery_username').val();
	$.ajax({
		url : 'user/question',
		type : "GET",
		dataType : 'json',
		data : {
			username : name
		}
	}).done(function(response) {
		$('#recovery-message').removeClass('alert-error');
		$('#recovery-message').removeClass('alert-success');
		$('#recovery-message').addClass('alert-' + response.msg.type);
		if (response.msg.type == 'error')
			$('#recovery-message').show();
		else
			$('#recovery-message').hide();

		$('#recovery-message').html(response.msg.message_body);
		$('#security-question').html(response.data.security_question);
		$('#security-details').show();

		//alert( "Success: " + msg );
	});
}

function change_password() {
	name = $('#recovery_username').val();
	recovery_answer = $('#recovery_answer').val();
	new_password = $('#new_password').val();
	$.ajax({
		url : 'user/change_password',
		type : "POST",
		dataType : 'json',
		data : {
			username : name,
			answer : recovery_answer,
			password : new_password
		}
	}).done(function(response) {
		$('#recovery-message').removeClass('alert-error');
		$('#recovery-message').removeClass('alert-success');
		$('#recovery-message').addClass('alert-' + response.msg.type);
		$('#recovery-message').show();
		if (response.msg.type == 'success') {
			//hide fields
			$('#security-details').hide();
			$('#security-requirements').hide();

		}
		$('#recovery-message').html(response.msg.message_body);
		//alert( "Success: " + msg );
	});
}

var lpOnComplete = function(response) {
	console.log(response);
	if (response.data) {
		var $newest = $('.latest-messages-list-item').first();
		$.each(response.data, function(i, obj) {
			c_id = obj.conversation_id;
			if ($("#latest-messages-item-" + c_id).length) {
				$("#latest-messages-item-" + c_id + " .latest-chat-contact").html(obj.username);
				$("#latest-messages-item-" + c_id + " .latest-chat-msg").html(obj.reply);
				if ($("#latest-messages-item-" + c_id + ".active")) {
					// update conversation thread
				}
			} else {
				$newest.before(latestmsgtemplate(obj));
			}
		});
	}
	//lpStart();
	setTimeout(lpStart, 15000);
};
var lpStart = function() {
	$.post('messages/latest_conversations', {}, lpOnComplete, 'json');
};
