$('a[data-toggle="tab"]').on('show', function (e) {
	$('#'+e.target.id+'-heading').attr('style','display:block');
	if(typeof(e.relatedTarget) !== "undefined"){
		$('#'+e.relatedTarget.id+'-heading').attr('style','display:none'); 
	}
		
})
$('#save-subtopic').click(function(e) {
	e.preventDefault();
	;
	$.ajax({
		type : "POST",
		url : "admin/lessons/edit_subtopic",
		dataType : 'json',
		data : $('#form-subtopic').serialize()
	}).done(function(response) {
		console.log('success');
		$('#edit-subtopic').modal('hide');
		getSubtopics();
		//$('#lesson-tabs a[href="#tab2"]').tab('show')
		//location.reload();

	});
	//event.preventDefault();
});
function editSubtopic(id) {
	//event.preventDefault();
	lessonId = $('#lesson_id').val();
	$('#form-subtopic').hide();
	$('.ajax-progress').show();
	$('#edit-subtopic').modal('show');
	$.ajax({
		type : "GET",
		url : "admin/lessons/edit_subtopic",
		data : {
			subtopic_id : id,
			lesson_id : lessonId
		}
	}).done(function(response) {
		//subtopic_process_response(response.data);
		$('#edit-subtopic .form-holder').html(response);
		$('.ajax-progress').hide();
		console.log('success');

	});

}


$('#subtopics-tab').click(function(e) {
	e.preventDefault();
	getSubtopics();
});
function getSubtopics() {
	lessonId = $('#lesson_id').val();
	$.ajax({
		type : "GET",
		url : "admin/lessons/get_subtopics?lesson_id=" + lessonId,
	}).done(function(response) {
		$('#table-subtopics tbody').html(response);
		$('#subtopics-tab').unbind('click');
	});
}


$('#trial-questions-tab').click(function(e) {
	e.preventDefault();
	getTrialQuestions();
});
function getTrialQuestions() {
	subtopicId = $('#subtopic').val();
	$.ajax({
		type : "GET",
		url : "admin/lessons/trial_questions?subtopic_id=" + subtopicId,
	}).done(function(response) {
		$('#table-trial-questions tbody').html(response);
		$('#trial-questions-tab').unbind('click');
	});
}

function saveTrialQuestion() {
	$.ajax({
		type : "POST",
		url : "admin/lessons/edit_trial_question",
		dataType : 'json',
		data : $('#form-question').serialize()
	}).done(function(response) {
		console.log('success');
		$('#edit-question').modal('hide');
		getTrialQuestions();
		$('#lesson-tabs a[href="#tab3"]').tab('show')
		//location.reload();

	});
}

function editQuestion(id) {
	subtopicId = $('#subtopic').val();
	$('#form-question').hide();
	$('.ajax-progress').show();
	$('#edit-question').modal('show');
	$.ajax({
		type : "GET",
		url : "admin/lessons/edit_trial_question",
		data : {
			subtopic_trial_question_id : id,
			subtopic_id : subtopicId
		}
	}).done(function(response) {
		//question_process_response(response);
		$('#form-question').show();
		$('#edit-question .form-holder').html(response);
		$('#fk_lesson_id').val(lessonId)
		$('.ajax-progress').hide();
		console.log('success');

	});
}


$('#fk_subtopic_id').on('change', function(e) {
	var option = $('#fk_subtopic_id').val();
	if (option != 0) {
		$('#save-question').attr('disabled', 'false');
	}

});
/* RETRIEVE PAST QUESTIONS
 * ======================= */
function getSubtopicPastQuestions(table, geturl) {
	id = $('#subtopic').val();
	data = {
		fk_subtopic_id : id
	};
	callback = function(response) {
		$(table).html(response);
	};
	url = geturl;
	getApiQuestions(url, data, callback);
	getSubtopicAssignedPastQuestions();
}


$('body').on('click', '#subtopic-past-questions-tab', function(e) {
	getSubtopicPastQuestions('#table-subtopic-past-questions tbody', "admin/subtopics/past_questions");
});
/* RETRIEVE SUBTOPIC ASSIGNED QUESTIONS
 * ======================= */
function getSubtopicAssignedPastQuestions() {
	id = $('#subtopic').val();
	data = {
		fk_subtopic_id : id
	};
	callback = function(response) {
		$('#table-assigned-past-questions tbody').html(response);
	};
	url = "admin/subtopics/subtopic_past_questions";
	getApiQuestions(url, data, callback);
}


$('body').on('click', '#subtopic-past-questions-tab', function(e) {
	getSubtopicPastQuestions('#table-subtopic-past-questions tbody', "admin/subtopics/past_questions");
});
function addSubtopicPastQuestion(id) {
	subtopic = $('#subtopic').val();
	data = {
		fk_question_id : id,
		fk_subtopic_id : subtopic
	};
	$.post('admin/subtopics/add_past_question', data, function(response) {
		console.log(response);
		getSubtopicAssignedPastQuestions();
	}, 'json');
}

function removeSubtopicPastQuestion(id) {
	data = {
		past_question_id : id
	};
	$.post('admin/subtopics/delete_past_question', data, function(response) {
		console.log(response);
		getSubtopicAssignedPastQuestions();
	}, 'json');
}

function deleteSubtopic(id) {
	$.ajax({
		type : "GET",
		url : "admin/lessons/delete_subtopic",
		dataType : 'json',
		data : {
			subtopic_id : id
		}
	}).done(function(response) {
		getSubtopics();
		console.log(response);

	});
}

function deleteTrialQuestion(id) {
	$.ajax({
		type : "GET",
		url : "admin/lessons/delete_trial_question",
		dataType : 'json',
		data : {
			subtopic_trial_question_id : id
		}
	}).done(function(response) {
		getTrialQuestions();
		console.log(response);

	});
}

function editSubtopicContent(id) {
	window.open("admin/subtopics/trial_questions?fk_subtopic_id=" + id, 'Content', "height=480,width=800");	

}
