function saveLevel(){
	event.preventDefault();
	 $.ajax({
	  type: "POST",
	  url: "admin/educationlevels/edit",
	  dataType:'json',
	  data: $('#form-education-level').serialize()
	}).done(function( response ) {
		console.log('success');
		$('#edit-education-level').modal('hide');
		location.reload();
		
	});//event.preventDefault();
}
function editLevel(id){
	event.preventDefault();
	$('#form-education-level').hide();
	$('.ajax-progress').show();
	$('#edit-education-level').modal('show');
	$.ajax({
	  type: "GET",
	  url: "admin/educationlevels/edit",
	  dataType:'json',
	  data: {education_level_id:id}
	}).done(function( response ) {
		level_process_response(response.data);
		$('#form-education-level').show();
		$('.ajax-progress').hide();
		console.log('success');
	  
	});
	
	
}
function level_process_response(response) {
    var frm = document.getElementById("form-education-level");
    var i;
 
    console.dir(response);      // for debug
 
    for (i in response) {
        if (i in frm.elements) {
            frm.elements[i].value = response[i];
        }
    }
}