$(document).ready(function() {
	var question_mode, // determines if in test mode,review mode or lesson mode (combines both)
		ajax_base_url; // holds the base url for mking ajax requests back to the controller page
	if (typeof(test_question_ids) != 'undefined') {
		ajax_base_url = 'tests';
	}
	if (typeof(mock_exams_question_ids) != 'undefined') {
		ajax_base_url = 'mockexams';
	}
	//added by kashboss for past question testing
	if (typeof(past_questions_ids) != 'undefined') {
		ajax_base_url = 'pastquestions';
	}
	$(document.body).bind('hidden', function() {
		$('#notes').show()
	});
	
	
	$('a[href="#past-questions"], a[href="#trial-questions"]').on('click', function(){
		question_mode = 'lesson';
		$('#notes').hide();
	});
	$('#pq-lesson-mode').on('click',null,function(e){
		question_mode = 'lesson';
	});
	//$('a[href="#review"], a[href="#take"]').on('click', function(e){
	$('body').on('click', 'a[href="#review"], a[href="#take"]', function(e) {
		var $this = $(this);
		//added past_questions_ids for testing pqs module front end
		var question_ids;
		if (typeof(mock_exams_question_ids) != 'undefined'){
			question_ids = mock_exams_question_ids;
			console.log('here I am');}
		else if (typeof(test_question_ids) != 'undefined')
			question_ids = test_question_ids;
		//added by kashboss for pq testing
		else if (typeof(past_questions_ids) != 'undefined')
			question_ids = past_questions_ids;
		//var question_ids = past_questions_ids||test_question_ids|| mock_exams_question_ids;
		//var question_ids = past_questions_ids || test_question_ids || mock_exams_question_ids;
		question_mode = $(this).attr('href') == '#take' ? 'take' : 'review';
		if (typeof(question_ids) != 'undefined'){
			var context = typeof(test_question_ids) != 'undefined' ? 'test' : 'mock exam';
			var section = $this.data('section-id');
			var url = ajax_base_url + '/' + question_mode + '_question_ids/' + $this.data('id') + '?uid=' + uniqueId();
			if (section)
				url += '&section=' + section;
			$.ajax({
				type: 'POST',
				url: url,
				dataType: 'json',
				//cache: false
			}).done(function(response){
				console.log(response);
				if (jQuery.isArray(response.data)){
					if (typeof(mock_exams_question_ids) != 'undefined')
						mock_exams_question_ids = response.data;
					if (typeof(test_question_ids) != 'undefined')
						test_question_ids = response.data;
					//added by kashboss for pq testing
					if (typeof(past_questions_ids) != 'undefined')
						past_questions_ids = response.data;
					// fire ajax dialog
					var width = $this.data('width');
					var url = $this.data('href');
					if (section)
						url += '?section=' + section;
					var child = $this.hasClass('child');
					showAjaxModal(url, width, child);
				}else{
					var msg = "You cannot " + question_mode + " this " + context + " at this time. Please try again later.";
					alert(msg);
				}
			});
		}
		e.preventDefault();
	});
	
	$('select[name="subject"]').change(function() {
		var $filter_topics = $('select[name="topic"]'); 
		$filter_topics.load(ajax_base_url + '/topic_list?subject_id=' + $(this).val());
		$filter_topics.removeAttr('disabled');
	}); 
});

// content pagination initialization
jQuery(window).load(function() {
    $('.paginated-content').ContentPagination({height: 400, fadeSpeed: 400});
});

$ajaxModal.on('click', '.step, .next, .previous, .restart, .next-section', function() {
	$ajaxModal.modal('loading');
	var nextUrl = $(this).data('href');
	var nextSection = $ajaxModal.find('#section').val(); // next section of current set of questions
	if (!$(this).hasClass('restart')){
		var q_ids;
		if ($ajaxModal.find('#mode').val() == 'tq')
			q_ids = convertArray(lesson_trial_question_ids);
		if ($ajaxModal.find('#mode').val() == 'pq')
			q_ids = convertArray(lesson_past_question_ids);
		//added by kashboss for past question testing
		if (typeof(past_questions_ids) != 'undefined')
			q_ids = convertArray(past_questions_ids);
		if (typeof(mock_exams_question_ids) != 'undefined')
			q_ids = convertArray(mock_exams_question_ids);
		if (typeof(test_question_ids) != 'undefined')
			q_ids = convertArray(test_question_ids);
		var q_id = parseInt($ajaxModal.find('#q_id').val());
		console.log('section id is ', nextSection);
		if ($(this).hasClass('next-section'))// Start next section button
			nextUrl += '?section=' + $(this).data('section-id');
		else if (q_id){
			nextUrl += '?q_id=' + q_id;
			var ans = $ajaxModal.find('#ans').val() || $ajaxModal.find('input[name=ansOptions]:checked').val();
			if (nextSection)
				nextUrl += '&section=' + nextSection;
			if (ans)
				nextUrl += '&ans=' + ans;
			if (!$(this).hasClass('complete')){
				q_id_pos = jQuery.inArray(q_id.toString(), q_ids);
				console.log('Reached here and failed:'+q_id_pos+' QUESTION ID IS:'+ q_ids);
				if ($(this).hasClass('previous') && q_id_pos != -1)
					nextID = q_ids[q_id_pos - 1];
				else if ($(this).hasClass('next') && q_id_pos != -1)
					nextID = q_ids[q_id_pos + 1];
				if (typeof(nextID) !== 'undefined')
					nextUrl += '&nxt=' + nextID;
				if ($(this).hasClass('close-explantion'))
					nextUrl += '&explanation=1';
			}else{
				nextUrl += '&complete=1';
			}
		}
	}else{
		nextUrl += '?restart=1';
		if (nextSection)
			nextUrl += '&section=' + nextSection;
	}
	$ajaxModal.load(nextUrl, '', function() {
		$ajaxModal.modal();
	});
});
function convertArray(qid){
	for(i=0;i<qid.length;i++){
		qid[i]=qid[i].toString();
	}
	return qid;
}

function pad(number, length){
	var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

var _second = 1000;
var _minute = _second * 60;
var _hour = _minute * 60;
var _day = _hour * 24
var countdownTimer;

function showRemaining(containerID) {
	var now = new Date();
	var distance = end - now;
	var countdown = document.getElementById(containerID);
	if (countdown){
		if (distance < 0) {
			clearInterval(countdownTimer);
			countdown.innerHTML = 'Times Up!';
			return;
		}
		var hours = Math.floor((distance % _day ) / _hour);
		var minutes = Math.floor((distance % _hour) / _minute);
		var seconds = Math.floor((distance % _minute) / _second);		
		
		countdown.innerHTML = '';
		if (hours)
			countdown.innerHTML += hours + ':';
		if (minutes && seconds){
			countdown.innerHTML += (hours) ? pad(minutes, 2) : minutes;
			countdown.innerHTML += ':' + pad(seconds, 2);
		}else if (!minutes && seconds){
			countdown.innerHTML += minutes + ' seconds';
		}else if (minutes && !seconds){
			countdown.innerHTML += pad(minutes, 2) + ':00';
		}
	}else{
		clearInterval(countdownTimer);
	}
}