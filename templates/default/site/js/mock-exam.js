$('#save-section').click(function (e) {
	e.preventDefault();
	mockExamId = $('#fk_mock_exam_id').val();
	pqId = $('#fk_past_question_id').val();
	section_type="";
	var href;
	if(mockExamId != "" && mockExamId != "0"){
		href = "admin/mockexams/edit_section";
		section_type = "mockexams";
	}else{
		href = "admin/pastquestions/edit_section";
		section_type = "pastquestions";
	}
	 $.ajax({
	  type: "POST",
	  url: href,
	  dataType:'json',
	  data: $('#form-section').serialize()
	}).done(function( response ) {
		console.log('success');
		$('#edit-section').modal('hide');
		getSections(section_type);				
	});
});
function editSection(id){
	mockExamId = $('#mock_exam_id').val();
	pqId = $('#past_question_id').val();
	var href,params;
	if(typeof(mockExamId)!= 'undefined'){
		params = {section_id:id,mock_exam_id:mockExamId}
		href = "admin/mockexams/edit_section"
	}else{
		params = {section_id:id,past_question_id:pqId}
		href = "admin/pastquestions/edit_section"
	}
	
	$('#form-section').hide();
	$('.ajax-progress').show();
	$('#edit-section').modal('show');
	$.ajax({
	  type: "GET",
	  url: href,
	  data: params
	}).done(function( response ) {
		$('#edit-section .form-holder').html(response);
		$('.ajax-progress').hide();
		console.log('success');	  
	});	
}

$('#mock-exam-sections-tab').click(function (e) {
  	e.preventDefault();
  	getSections('mockexams');  	
});
$('#past-question-sections-tab').click(function (e) {
  	e.preventDefault();
  	getSections('pastquestions');  	
});

function getSections(section_type){
	mockExamId = $('#mock_exam_id').val();
	pqId = $('#past_question_id').val();
	var params;
	if(section_type == "mockexams")
		params = {mock_exam_id:mockExamId}
	else
		params = {past_question_id:pqId}
	$.ajax({
	  type: "GET",
	  url: "admin/"+section_type+"/get_sections",
	  data:params
	  }).done(function( response ) {
		$('#table-mock-sections tbody').html(response);
	  	$('#mock-exam-sections-tab').unbind('click');
	  	$('#section_type').val(section_type);
	});
}
$('#mock-exam-questions-tab').click(function (e) {
  	e.preventDefault();
  	getMockQuestions();  	
});
function getMockQuestions(section){
	mockExamId = $('#mock_exam_id').val();
	var sectionId;
	if(typeof(section)!== 'undefinded'){
		sectionId= section;
	}
	$.ajax({
	  type: "GET",
	  url: "admin/mockexams/mock_exam_questions",
	  data:{mock_exam_id:mockExamId,section_id:sectionId}
	  }).done(function( response ) {
		$('#table-mock-exam-questions tbody').html(response);
	  	$('#mock-exam-questions-tab').unbind('click');
	});
}
//TODO:: use saveAPiQuestion function
function saveMockQuestion(){
	tinyMCE.triggerSave();
	 $.ajax({
	  type: "POST",
	  url: "admin/mockexams/edit_mock_question",
	  dataType:'json',
	  data: $('#form-question').serialize()
	}).done(function( response ) {
		console.log('success');
		$('#edit-question').modal('hide');
		getMockQuestions()	;	
		$('#mock-exam-tabs a[href="#tab3"]').tab('show')
		//location.reload();
		
	});
}
//TODO:: use editAPiQuestion function
function editMockQuestion(id){
	mockExamId = $('#mock_exam_id').val();
	$('#form-question').hide();
	$('.ajax-progress').show();
	$('#edit-question').modal('show');
	$.ajax({
	  type: "GET",
	  url: "admin/mockexams/edit_mock_question",
	  data: {mock_exam_question_id:id,mock_exam_id:mockExamId}
	}).done(function( response ) {
		//question_process_response(response);
		$('#form-question').show();
		$('#edit-question .form-holder').html(response);
		$('#fk_mock_exam_id').val($('#mock_exam_id').val());
		$('.ajax-progress').hide();
		console.log('success');
	  
	});	
}
$('#fk_subtopic_id').on('change', function(e){
	var option = $('#fk_subtopic_id').val();
	if(option != 0){
		$('#save-question').attr('disabled','false');
	}
	
});
function deleteSubtopic(id){
	$.ajax({
	  type: "GET",
	  url: "admin/lessons/delete_subtopic",
	  dataType:'json',
	  data: {subtopic_id:id}
	}).done(function( response ) {
		getSubtopics();
		console.log(response);
	  
	});	
}
function deleteTrialQuestion(id){
	$.ajax({
	  type: "GET",
	  url: "admin/lessons/delete_trial_question",
	  dataType:'json',
	  data: {subtopic_trial_question_id:id}
	}).done(function( response ) {
		getTrialQuestions();
		console.log(response);
	  
	});	
}