$('.html-editor').wysihtml5();
   
$('.modal').css({
  width: '55%',
  left:'43%'
  
});
$('#save-question').click(function (e) {
	e.preventDefault();
	if($('#question-type').val() == 'trial')
		saveTrialQuestion();
	else if($('#question-type').val() == 'test')
		saveTestQuestion();
	else if($('#question-type').val() == 'mock')
		saveMockQuestion();
	else if($('#question-type').val() == 'past')
		savePastQuestion();
});

