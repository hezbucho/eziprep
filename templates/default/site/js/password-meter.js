$(function() {
	$("#password").complexify({}, function(valid, complexity) {
		if (!valid) {
			$('#progress').css({
				'width' : complexity + '%'
			}).removeClass('progressbarValid').addClass('progressbarInvalid');
		} else {
			$('#progress').css({
				'width' : complexity + '%'
			}).removeClass('progressbarInvalid').addClass('progressbarValid');
		}
	});
}); 