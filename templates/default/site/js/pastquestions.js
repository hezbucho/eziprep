/* ========================================================
 * pastquestions.js v0.0.1
 * Author: Joseph Bosire
 * Email: kashboss@gmail.com
 * ======================================================== */
 /* LOAD QUESTIONS ON TAB SELECT
  * ============================ */
$('#past-questions-tab').click(function (e) {
  	e.preventDefault();
  	getPastQuestions();  	
});
/* RETRIEVE PAST QUESTIONS
 * ======================= */
function getPastQuestions(){
	id = $('#past_question_id').val();
	data = {past_question_id:id};
	callback = function(response){
		$('#table-past-questions tbody').html(response);
	  	$('#past-questions-tab').unbind('click');
		};
	url = "admin/pastquestions/get_past_questions";
	getApiQuestions(url,data,callback);	
}
/* RETIEVE A PAST QUESTION FOR EDITING
 * =================================== */
function editPastQuestion(id){
	pq_id = $('#past_question_id').val()	
	data = {past_question_questions_id:id,past_question_id:pq_id};
	callback = function(){$('#fk_past_question_id').val($('#past_question_id').val())};
	url = "admin/pastquestions/edit_past_question";
	editApiQuestion(url,data,callback);	
}
/* SAVE A PAST QUESTION
 * ==================== */
function savePastQuestion(){	
	callback = function(){
		$('#edit-question').modal('hide');
		getPastQuestions();	
		$('#past-questions-tab a[href="#tab2"]').tab('show');
	};
	url = "admin/pastquestions/edit_past_question";
	saveApiQuestion(url,callback);	
}
/* POPUP WINDOW TO ADD QUESTIONS
 * ============================= */
function editSectionContent(id) {
	param='past_question_id';
	if($('#section_type').val()=='mockexams')
		param = 'mock_exam_id';
		
	window.open("admin/"+$('#section_type').val()+"/get_section_content?"+param+"=" + id, 'Content', "height=480,width=800");	

}
