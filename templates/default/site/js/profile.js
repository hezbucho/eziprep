//var selectedSchool = "";

$('#school_name').typeahead({
    source: function (query, process) {
    	 school_ids = [];
    	 school_names = {};
    	 var schools = all_schools;
		$.each(schools, function (i, school) {
				
		        school_names[school.school_id - 1] = school;
		        school_ids.push(school.school_name);
		    });
 
    process(school_ids);
        // implementation
    },
    updater: function (item) {
    	$.each(school_names,function(index,obj){
    		console.log(index, obj);
    		if(obj.school_name == item){
    			update_school_info(school_names[index]);
    			
    		}
    	}); 
        
        
    	return item;
    },
    matcher: function (item) {
    	if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
	        return true;
	    }else{
	    	 $('#school_id').val('');
	    }
	   
	},
    sorter: function (items) {
         return items.sort();
    },
    highlighter: function (item) {
    var regex = new RegExp( '(' + this.query + ')', 'gi' );
    return item.replace( regex, "<strong>$1</strong>" );
    },
});

function update_school_info(selectedSchool){
	$('#fk_school_id').val(selectedSchool.school_id);

	$('#school_location').val(selectedSchool.school_location);
	$('#school_address').val(selectedSchool.school_address);
	$('#principal').val(selectedSchool.principal);
}

function edit_bio(){
	//hide labels
	$('.bio-data').hide();	
	//show text input
	$('.input-bio').show();	
	//Switch buttons
	document.getElementById('bio_edit').setAttribute('style','display:none');
	document.getElementById('bio_done').setAttribute('style','display:block');

}
function edit_security(){
	//hide labels
	$('.security-data').hide();	
	//show text input
	$('.input-security').show();	
	//Switch buttons
	document.getElementById('security_edit').setAttribute('style','display:none');
	document.getElementById('security_done').setAttribute('style','display:block');

}
function edit_edu(){
	//hide labels
	$('.edu-data').hide();	
	//show text input
	$('.input-edu').show();	
	//Switch buttons
	document.getElementById('edu_edit').setAttribute('style','display:none');
	document.getElementById('edu_done').setAttribute('style','display:block');

}
function edit_contacts(){
	//hide labels
	$('.contact-data').hide();	
	//show text input
	$('.input-contact').show();	
	
	//Switch buttons
	document.getElementById('contact_edit').setAttribute('style','display:none');
	document.getElementById('contact_done').setAttribute('style','display:block');
	
}