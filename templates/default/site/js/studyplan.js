function toggleProgressBar(flag){
	if(flag =='show')
		$('.progress').attr('style','display:block');
	else
		$('.progress').attr('style','display:none');
}

var starts, ends, wholeDay,showModal=false;
//TODO: Move to separate file
$(document).ready(function() {
	getStudyPlan();
});
$(document).ready(function() {
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	var calendar = $('#calendar').fullCalendar({
		header : {
			left : 'prev,next today',
			center : 'title',
			right : 'month,agendaWeek,agendaDay'
		},
		selectable : true,
		unselectAuto : false,
		selectHelper : true,
		eventClick : function(event, jsEvent, view) {
			var view = $('#calendar').fullCalendar('getView');
			if (view.name == 'agendaDay') {
				url = 'studyplans/edit?study_plan_id=' + event.id;
				$('#form-content').html('Loading event plan...')
				$.get(url,null,function(response){$('#form-content').html(response)});
				$('#event-plan').modal("show");
				$('.delete').attr('disabled', false);
				starts = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
				ends = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
				wholeDay = false;
			}
		},
		dayClick : function(date, allDay, jsEvent, view) {
			if (view.name != 'month')
				return;
			$('#calendar').fullCalendar('changeView', 'agendaDay').fullCalendar('gotoDate', date);
			
		},
		select :function( startDate, endDate, allDay, jsEvent, view ) {
			console.log($('#event-plan').attr('data-remote'));
			var view = $('#calendar').fullCalendar('getView');
			if (view.name == 'agendaDay' && showModal==true) {
				url ='studyplans/edit';
				console.log(jsEvent);
				$('#form-content').html('Loading event plan form...')
				$.get(url,null,function(response){$('#form-content').html(response)});
				$('#event-plan').modal("show");
				startDate = $.fullCalendar.formatDate(startDate, "yyyy-MM-dd HH:mm:ss");
				endDate = $.fullCalendar.formatDate(endDate, "yyyy-MM-dd HH:mm:ss");
				starts = startDate;
				ends = endDate;
				wholeDay = false;
				console.log('EVent Select');
				calendar.fullCalendar('unselect');
			}
			showModal=true;
		},
		editable : true,
		eventDrop : function(event, dayDelta, minuteDelta, allDay, revertFunc) {
			updateStudyPlan(event, dayDelta, minuteDelta, false, revertFunc, 'drag');

		},
		eventResize : function(event, dayDelta, minuteDelta, revertFunc) {
			updateStudyPlan(event, dayDelta, minuteDelta, false, revertFunc, 'resize');
		}
	});
});

function saveStudyPlan() {
	//BUILD params to post
	$.post('studyplans/save_plan', {
		study_plan_id : $('#study_plan_id').val(),
		fk_subtopic_id : $('#subtopic').val(),
		event_description:$('#event_description').val(),
		start : starts,
		allDay : wholeDay,
		end : ends,
		type : 'edit'
	}, function(response) {
		getStudyPlan();
		$('#event-plan').modal("hide");
	}, 'json');
}
function deleteEvent(){
	id = $('#study_plan_id').val();
	addConfirmDialog('Are you sure you want to delete this event.',function(){
		$.post('studyplans/delete_event', {plan_id : id}, function(response) {
		getStudyPlan();
		$('#event-plan').modal("hide");
	}, 'json');
	})	
}
function updateStudyPlan(event, days, minutes, wholeDay, revertFunc, actionType) {
	//BUILD params to post
	$.post('studyplans/save_plan', {
		study_plan_id : event.id,
		dayDelta : "+" + days,
		minuteDelta : "+" + minutes,
		allDay : wholeDay,
		type : 'update',
		action : actionType
	}, function(response) {
		getStudyPlan();
	}, 'json');
}

function getStudyPlan() {
	$.getJSON('studyplans/get_study_plan', null, function(response) {
		$('#calendar').fullCalendar('removeEvents');
		$('#calendar').fullCalendar('addEventSource', response.data);
	});
}

