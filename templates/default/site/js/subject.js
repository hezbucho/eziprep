function saveSubject(){
	 event.preventDefault();
	 $.ajax({
	  type: "POST",
	  url: "admin/subjects/edit",
	  dataType:'json',
	  data: $('#form-subject').serialize()
	}).done(function( response ) {
		console.log('success');
		$('#edit-subject').modal('hide');
		location.reload();
	  //alert( "Success: " + msg );
	});//event.preventDefault();
}
function editSubject(id){
	event.preventDefault();
	$('#form-subject').hide();
	$('.ajax-progress').show();
	$('#edit-subject').modal('show');
	$.ajax({
	  type: "GET",
	  url: "admin/subjects/edit",
	  dataType:'json',
	  data: {subject_id:id}
	}).done(function( response ) {
		subject_process_response(response.data);
		$('#form-subject').show();
		$('.ajax-progress').hide();
		console.log('success')
	  //alert( "Success: " + msg );
	});
	
	
}
function subject_process_response(response) {
    var frm = document.getElementById("form-subject");
    var i;
 
    console.dir(response);      // for debug
 
    for (i in response) {
        if (i in frm.elements) {
            frm.elements[i].value = response[i];
        }
    }
}