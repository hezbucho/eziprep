/* ========================================================
 * tests.js v0.0.1
 * Author: Joseph Bosire
 * Email: kashboss@gmail.com
 * ======================================================== */
/* LOAD QUESTIONS ON TAB SELECT
 * ============================ */
$('#test-questions-tab').click(function(e) {
	e.preventDefault();
	getTestQuestions();
});
/* RETRIEVE TEST QUESTIONS
 * ======================= */
function getTestQuestions() {
	id = $('#test_id').val();
	data = {
		test_id : id
	};
	callback = function(response) {
		$('#table-test-questions tbody').html(response);
		$('#test-questions-tab').unbind('click');
	};
	url = "admin/tests/get_test_questions";
	getApiQuestions(url, data, callback);
}

/* RETIEVE A TEST QUESTION FOR EDITING
 * =================================== */
function editTestQuestion(id) {
	data = {
		test_question_id : id
	};
	callback = function() {
		$('#fk_test_id').val($('#test_id').val())
	};
	url = "admin/tests/edit_test_question";
	editApiQuestion(url, data, callback);
}

/* SAVE A TEST QUESTION
 * ==================== */
function saveTestQuestion() {
	callback = function() {
		$('#edit-question').modal('hide');
		getTestQuestions();
		$('#test-questions-tabs a[href="#tab2"]').tab('show');
	};
	url = "admin/tests/edit_test_question";
	saveApiQuestion(url, callback);
}

/* FILTERING
 * ==================== */

$(function() {
	$('body').on('change', 'select#country', function(e) {
		$.getJSON("admin/tests/get_levels", {
			country : $('select#country').val()
		}, function(j) {
			var options = '';
			for (var i = 0; i < j.data.length; i++) {
				options += '<option value="' + j.data[i].id + '">' + j.data[i].title + '</option>';
				console.log(options);
			}
			$("select#level").html(options);
		});
	});
});
$(function() {
	$('body').on('change', 'select#subject,select#level', function(e) {
		$.getJSON("admin/tests/get_topics", {
			country : $('select#country').val(),
			subject : $('select#subject').val(),
			level : $('select#level').val()
		}, function(j) {
			console.log(j);
			var options = '';
			for (var i = 0; i < j.data.length; i++) {
				options += '<option value="' + j.data[i].id + '">' + j.data[i].title + '</option>';
				console.log(options);
			}
			$("select#topics").html(options);
		});
	});
}); 
//ADD CONTENT
function editTestContent(id) {
	window.open("admin/tests/get_test_content?test_id=" + id, 'Content', "height=480,width=800");	

}
