$(function() {
	// listen to click event
	$('.btn-group[data-toggle="buttons-checkbox"] .btn').click(function(e) {// checkbox behavior
		var $this = $(this);
		var $parent = $($this.parent().get(0));
		var inputName = $parent.data('name');
		var section = $parent.data('section');
		// post data as array
		// $('[name="' + inputName + '"]').val($this.data('value'));
		// only add behavior to our custom groups
		if ($this.parent('.horizontal-group').length || $this.parent('.vertical-group').length) {
			// console.log($this.parent('[data-toggle="buttons-checkbox"]').length);
			var $icon = $($this.children('i')[0]);
			//console.log($this.children('i')[0]);
			if (!$this.hasClass('active')) {// inverse logic because this event handler executes before the plugin attaches the class
				$this.addClass('btn-primary');
				$icon.addClass('icon-white')
			} else {
				$this.removeClass('btn-primary');
				$icon.removeClass('icon-white')
			}
			calculateSubtotal(null, section, $this.data('value'));
		}
	});
	$('.btn-group[data-toggle="buttons-radio"] .btn').click(function(e) {// radio behavior
		var $this = $(this);
		var $parent = $($this.parent().get(0));
		var inputName = $parent.data('name');
		var section = $parent.data('section');
		// simply update hidden field with this name with the crrent element's value
		// $('[name="' + inputName + '"]').val($this.data('value'));
		console.log(inputName);
		$parent.children().each(function(i, obj) {
			var $obj = $(this);
			var $icon = $($obj.children('i')[0]);
			if ($obj.data('value') == $this.data('value') && $($obj.parent().get(0)).data('name') == inputName) {
				$obj.addClass('btn-primary');
				$icon.addClass('icon-white');
				//$('.row-price[data-name="' + inputName + '"] .badge').html('$' + $obj.data('value'));
				//console.log($obj.data('value'), section);
				calculateSubtotal($obj.data('value'), section);
			} else {
				$obj.removeClass('btn-primary');
				$icon.removeClass('icon-white')
			}
		});
	});
	
	window.tmpPrice = 0;
	window.calculateSubtotal = function(duration, dataSectionName, inverseLogic){
		duration = duration ||  $('.btn-group[data-section="' + dataSectionName + '"][data-toggle="buttons-radio"] .btn.active').data('value');
		tmpPrice = 0;
		$('.btn-group[data-section="' + dataSectionName + '"][data-toggle="buttons-checkbox"] .btn').each(function(i, obj) {
			var $this = $(obj);
			cost = parseFloat($this.data('cost'));
			if (cost && ((!$this.hasClass('active') && inverseLogic == $this.data('value')) || ($this.hasClass('active') && inverseLogic != $this.data('value'))) )
					tmpPrice += cost;
		});
		
		//console.log(duration, tmpPrice);
		tmpPrice *= duration;
		//console.log(tmpPrice);
		$('.row-price[data-section="' + dataSectionName + '"] .badge').html('$' + tmpPrice);
	}
}); 