function updateNotifications(){
	$.getJSON('site/notifications',null,function(response){
		if(response.msg.type == 'success'){
			$('#total_notifications').html(response.data.total);
			$('#no_invites').html(response.data.invites);
		}
	});
}
$(document).ready(function() {
	// validate signup form on keyup and submit
	var validator = $("#registration-form").validate({
		rules : {
			username : {
				required : true,
				minlength : 2
			},
			password : {
				password : "#username"
			},
			password_confirm : {
				required : true,
				equalTo : "#password"
			}
		},
		messages : {
			username : {
				required : "Enter a username",
				minlength : jQuery.format("Enter at least {0} characters")
			},
			password_confirm : {
				required : "Repeat your password",
				minlength : jQuery.format("Enter at least {0} characters"),
				equalTo : "Enter the same password as above"
			}
		},
		// the errorPlacement has to take the table layout into account
		errorPlacement : function(error, element) {
			error.prependTo(element.parent().next());
		},
		// specifying a submitHandler prevents the default submit, good for the demo
		submitHandler : function() {
			alert("submitted!");
		},
		// set this class to error-labels to indicate valid fields
		success : function(label) {
			// set &nbsp; as text for IE
			label.html("&nbsp;").addClass("checked");
		}
	});

	// propose username by combining first- and lastname
	$("#username").focus(function() {
		var firstname = $("#firstname").val();
		var lastname = $("#lastname").val();
		if (firstname && lastname && !this.value) {
			this.value = firstname + "." + lastname;
		}
	});

});
function acceptGroupInvite(id,group_id) {
	$.post('studygroups/join_group', {
		user : id,
		group:group_id,
		status:1
	}, function(e) {
		if (e.msg.type == "success") {
			$('#group-notification-' + e.data.invite_id).remove();
			console.log('Added friend...');
			updateNotifications();
			location = "studygroups";
		}
	}, 'json');
}

function addFriend(id) {
	$.post('friends/add_friend', {
		friend : id
	}, function(e) {
		if (e.msg.type == "success") {
			console.log('Added friend...');
		}
	}, 'json');
}
function leaveGroup(user,group) {
	addConfirmDialog("Are you sure you want to leave this group",function(){
		location = "studygroups/remove/"+user+"?group="+group;
	})
}	
function rejectGroupInvite(invite_id, invited_by) {
	$.post('studygroups/reject', {
		id : invite_id,
		user : invited_by
	}, function(response) {
		if (response.data.friend) {
			addConfirmDialog(response.msg.message_body, function() {
			});
		} else {
			console.log('Invite deleted');
		}
		$('#group-notification-' + response.data.invite_id).remove();
		updateNotifications();
	}, 'json');
}
