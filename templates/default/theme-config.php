<?php defined('SYSPATH') or die('No direct script access.');
$session = Session::instance();
$template_name = Kohana::$config->load('settings.template') ? Kohana::$config->load('settings.template') : 'default';//$session->get('sys_template_name');
$component_name = $session->get('sys_template_component');
$uri_segments = Helper_Core::process_uri_segments();
if ( !$component_name && is_dir(APPPATH . 'classes' . DIRECTORY_SEPARATOR . 'Controller' . DIRECTORY_SEPARATOR . $uri_segments[0]) )
	$template_component = strtolower($uri_segments[0]);
else
	$template_component = $component_name ? $component_name : 'site';
$template_path = 'templates/' . $template_name  . '/' . $template_component;
$template_path = file_exists(DOCROOT. $template_path) ? $template_path . '/': '';
return array(
	// add component-specific configs here e.g. theming etc
	'component_configs' => array(
		'site' => array(
			'content_blocks_folder' => 'contentblocks',
			// default theme name; if key is missing theme functionality is disabled entirely
			'theme' => 'default',
			// flag indicating whether theme file is in the main_css or included in child override css files 
			'theme_in_main_css' => true, // if true, theme css will be prepended to globalcss instead of after global css (if key entirely missing/ is false) 
			// name->file mappings for themes
			'themes' => array(
				'default' => 'css/bootstrap.css',
				'bootstrap' => 'css/bootstrap_original.css',
			),
			// whether or not to allow dynamic themes, if false or key is entirely missing dynamic themes are disabled
			'enable_dynamic_themes' => true,
			// class to attach to each widget's container div
			'widget_container_class' => '',
		),
		'admin' => array(
			// component from which this should load template; if key is missing, template files loaded from component folder

			'share_template' => 'site',
			'content_blocks_folder' => 'admincontentblocks',
			/*// default theme name; if key is missing theme functionality is disabled entirely

			//'share_template' => 'site',
			'content_blocks_folder' => 'contentblocks',
			// default theme name; if key is missing theme functionality is disabled entirely
	Add materials ui concept for admin and barebaones controllers
			'theme' => 'default',
			// flag indicating whether theme file is in the main_css or included in child override css files 
			'theme_in_main_css' => true, // if true, theme css will be prepended to globalcss instead of after global css (if key entirely missing/ is false) 
			// name->file mappings for themes
			'themes' => array(
				'default' => 'css/bootstrap.css',
				'bootstrap' => 'css/bootstrap_original.css',
			),
			// whether or not to allow dynamic themes, if false or key is entirely missing dynamic themes are disabled
			'enable_dynamic_themes' => true,
			// class to attach to each widget's container div
			'widget_container_class' => '',*/
		),
		'mobile' => array()
	),
	// Begin css & js asset mapping
	'assets' => array(
		'site' => array(// component name
			'site' => array(//controller name
				'groups' => array(// signifies the start of asset group definition
					'globalcss' => array( // the globalcss key MUST be in the main level2/component controller
						array('CSS', $template_path . 'css/font-awesome.min.css', array(
							'media' => 'screen',
						)),
						// array('CSS', $template_path . 'css/font-awesome-ie7.min.css', array(
							// 'media' => 'screen',
						// )),
						array('CSS', $template_path . 'css/global.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/bootstrap-responsive.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/bootstrap-modal.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/fullcalendar.css', array(
							'media' => 'screen',
						)),
						
					),
					'globaljs' => array(
						array('JS', $template_path . 'js/jquery-1.9.1.min.js'),
						array('JS', $template_path . 'js/bootstrap-dropdown.js'),
						array('JS', $template_path . 'js/bootstrap-button.js'),
						array('JS', $template_path . 'js/toggle-button.js'),
						array('JS', $template_path . 'js/bootstrap-modalmanager.js'),
						array('JS', $template_path . 'js/bootstrap-modal.js'),
						array('JS', $template_path . 'js/bootstrap-tooltip.js'),
						array('JS', $template_path . 'js/bootstrap-popover.js'),
						array('JS', $template_path . 'js/bootstrap-tab.js'),						
						array('JS', $template_path . 'js/handlebars.js'),
						array('JS', $template_path . 'js/jquery.expander.min.js'),
						array('JS', $template_path . 'js/jquery.confirm.js'),
						array('JS', $template_path . 'js/fullcalendar.min.js'),	
						array('JS', $template_path . 'js/studyplan.js'),
						array('JS', $template_path . 'js/global.js'),
						array('JS', $template_path . 'js/user.js'),
						array('JS', $template_path . 'js/content-pagination.js'),
						array('JS', $template_path . 'js/materials.js'),						
						
						array('JS', $template_path . 'js/calendar-custom.js'),						
						//array('JS', $template_path . 'js/html5shiv.js'),jquery-ui-1.10.2.custom.min
					),
				)
			),
			'announcements' => array(
				'groups' => array(
					'indexcss' => array( //index action-specific css resoucres						
					),
					'indexjs' => array( //index action-specific js resoucres
						array('JS', $template_path . 'js/jquery.expander.min.js'),
					),
				)
			),
			'studygroups' => array(
				'groups' => array(
					'indexcss' => array( //index action-specific css resoucres
						
					),
					'indexjs' => array( //index action-specific js resoucres
						array('JS', $template_path . 'js/bootstrap-tab.js'),
						
					)
				)
			),
			'studyplans' => array(
				'groups' => array(
					'indexcss' => array( //index action-specific css resoucres
						
					),
					'indexjs' => array( //index action-specific js resoucres
						
						
					)
				)
			),
			'lessons' => array(
				'groups' => array(
					'indexcss' => array( //index action-specific css resoucres
						
					),
					'indexjs' => array( //index action-specific js resoucres
						
					),
					'profilejs' => array( //profile action-specific js resoucres

						array('JS', $template_path . 'js/jquery.complexify.js'),
						array('JS', $template_path . 'js/password-meter.js'),
						array('JS', $template_path . 'js/bootstrap-datetimepicker.min.js'),
						array('JS', $template_path . 'js/date-picker-init.js'),
						
						array('JS', $template_path . 'js/bootstrap-typeahead.js'),
						array('JS', $template_path . 'js/profile.js'),
					)
				)
			),
			'user' => array(
				'groups' => array(
					'logincss' => array( //index action-specific css resoucres
						array('CSS', $template_path . 'css/bootstrap-datetimepicker.min.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/jquery.validate.password.css', array(
							'media' => 'screen',
						)),
					),
					'loginjs' => array( //index action-specific js resoucres
						array('JS', $template_path . 'js/jquery.complexify.js'),
						array('JS', $template_path . 'js/password-meter.js'),
						array('JS', $template_path . 'js/bootstrap-datetimepicker.min.js'),
						array('JS', $template_path . 'js/date-picker-init.js'),
						array('JS', $template_path . 'js/profile.js'),
						array('JS', $template_path . 'js/jquery.validate.min.js'),
						array('JS', $template_path . 'js/jquery.validate.password.js'),
						array('JS', $template_path . 'js/user.js'),
					),
					'profilejs' => array( //profile action-specific js resoucres

						array('JS', $template_path . 'js/jquery.complexify.js'),
						array('JS', $template_path . 'js/password-meter.js'),
						array('JS', $template_path . 'js/bootstrap-datetimepicker.min.js'),
						array('JS', $template_path . 'js/date-picker-init.js'),
						
						array('JS', $template_path . 'js/bootstrap-typeahead.js'),
						array('JS', $template_path . 'js/profile.js'),
					)
				)
			)
		),
		'admin' => array(// component name
			'admin' => array(//controller name
				'groups' => array(// signifies the start of asset group definition
					'globalcss' => array( // the globalcss key MUST be in the main level2/component controller
						array('CSS', $template_path . 'css/bootstrap-wysihtml5.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/global.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/bootstrap-responsive.css', array(
							'media' => 'screen',
						)),
						
					),
					'globaljs' => array(
						array('JS', $template_path . 'js/jquery-1.9.1.min.js'),
						array('JS', $template_path . 'js/jquery-ui-1.10.3.custom.min.js'),
						array('JS', $template_path . 'js/bootstrap-dropdown.js'),
						array('JS', $template_path . 'js/bootstrap-tab.js'),
						array('JS', $template_path . 'js/admin/bootstrap-modal.js'),
						array('JS', $template_path . 'js/admin/tinymce/tinymce.min.js'),//tinymce.cachefly.net/4.0/tinymce.min.js
						//array('JS', $template_path . 'js/wysihtml5-0.3.0.min.js'),
						//array('JS', $template_path . 'js/bootstrap-wysihtml5.js'),
						array('JS', $template_path . 'js/bootstrap-collapse.js'),
						//array('JS', $template_path . 'js/admin.js'),
						array('JS', $template_path . 'js/admin/admin.js'),
						array('JS', $template_path . 'js/admin/announcements.js'),
						array('JS', $template_path . 'js/lesson.js'),
						array('JS', $template_path . 'js/subject.js'),
						array('JS', $template_path . 'js/level.js'),
						array('JS', $template_path . 'js/country.js'),
						array('JS', $template_path . 'js/tests.js'),
						array('JS', $template_path . 'js/mock-exam.js'),
						array('JS', $template_path . 'js/pastquestions.js'),
						array('JS', $template_path . 'js/examsessions.js'),
						
					),
					'subjectjs' => array(
						array('JS', $template_path . 'js/subject.js'),
					),
					'indexcss' => array( //index action-specific css resoucres
						/*array('CSS', $template_path . 'css/jquerytour.css', array(
							'media' => 'screen',
						)),*/
					),
					'indexjs' => array( //index action-specific js resoucres
						
					)
				)
			),
			'dashboard' => array(
				'groups' => array(
					'indexcss' => array( //index action-specific css resoucres
						array('CSS', $template_path . 'css/widgets.css', array(
							'media' => 'screen',
						)),
					),
					'indexjs' => array( //index action-specific js resoucres
						array('JS', $template_path . 'js/bootstrap-collapse.js'),
					)
				)
			)
		),
		'mobile' => array(// component name
			'mobile' => array(//controller name
				'groups' => array(// signifies the start of asset group definition
					'globalcss' => array( // the globalcss key MUST be in the main level2/component controller
						array('CSS', $template_path . 'css/jquery.mobile-1.3.0.min.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/leaflet.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/global.css', array(
							'media' => 'screen',
						)),
						array('CSS', $template_path . 'css/detailsview.css', array(
							'media' => 'screen',
						)),
					),
					'globaljs' => array(
						array('JS', $template_path . 'js/jquery-1.9.1.min.js'),
						array('JS', $template_path . 'js/global.js'),
						array('JS', $template_path . 'js/jquery.mobile-1.3.0.min.js'),
						array('JS', $template_path . 'js/leaflet.js'),
					),
					'indexjs' => array( //action-specific js resoucres
						array('JS', $template_path . 'js/set_center.js'),
					),
					'health_facilityjs' => array( //action-specific js resoucres
						array('JS', $template_path . 'js/health_facility.js'),
					),
					'directionsjs' => array(
						//array('JS', $template_path . 'js/leaflet.js'),
						//array('JS', $template_path . 'js/leaflet.zoomfs.js'),
						array('JS', $template_path . 'js/directions.js'),
					)
				)
			)	
		)
	)
);